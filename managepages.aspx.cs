﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class managepages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRoles();
        }
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.MANAGEPAGES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() 
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    void BindRoles()
    {
        ddlRoles.DataSource = new RolesDAL().GetAllDataSet();
        ddlRoles.DataTextField = "Title";
        ddlRoles.DataValueField = "RoleId";
        ddlRoles.DataBind();

    }

    [WebMethod]
    public static string Insert(int id, string title, bool isActive, string arrRoles)
    {

       

        MembershipPages objMembershipPages = new MembershipPages()
        {
            MembershipPageId = id,
            Title = title.Trim().ToUpper(),
            IsActive = isActive,
            PagePrefix = "",
            Roles = arrRoles

        };

        string[] pidData = arrRoles.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("Id");

        for (int i = 0; i < pidData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["Id"] = Convert.ToInt32(pidData[i]);
            dt.Rows.Add(dr);

        }

        JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();

        int status = new MembershipPagesBLL().InsertUpdate(objMembershipPages, dt);
        var JsonData = new
        {
            MembershipPage = objMembershipPages,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}