﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class stewardwelcome : System.Web.UI.Page
{


    public Int32 BranchId { get { return Request.QueryString["bid"] != null ? Convert.ToInt32(Request.QueryString["bid"]) : 0; } }

    public string BranchName { get { return Request.QueryString["bname"] != null ? Convert.ToString(Request.QueryString["bname"]) : ""; } }
    public Int32 UserId { get { return Request.QueryString["UserId"] != null ? Convert.ToInt32(Request.QueryString["UserId"]) : 0; } }
    public string UserName { get { return Request.QueryString["UserName"] != null ? Convert.ToString(Request.QueryString["UserName"]) : ""; } }

    protected void Page_Load(object sender, EventArgs e)
    {
      
     

        Response.Cookies[Constants.EmployeeId].Value = UserId.ToString();
        Response.Cookies[Constants.DesignationId].Value = "2";
        Response.Cookies[Constants.BranchId].Value = BranchId.ToString();
        Response.Cookies[Constants.BranchName].Value = BranchName;
        Response.Cookies[Constants.EmployeeName].Value = UserName;

        Response.Redirect("CustomerKotScreen.aspx");
    }
}