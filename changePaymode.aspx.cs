﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

public partial class changePaymode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCreditCustomers();
            BindOtherPaymodeDrop();
        }
        CheckRole();
    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.CHANGEPAYMODE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }


    }

    void BindCreditCustomers()
    {

        ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit.DataValueField = "CCODE";
        ddlChosseCredit.DataTextField = "CNAME";
        ddlChosseCredit.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Customer--";
        li1.Value = "0";
        ddlChosseCredit.Items.Insert(0, li1);

    }
    void BindOtherPaymodeDrop()
    {
        string SqlQuery = string.Empty;

        SqlQuery = "select OtherPayment_ID,OtherPayment_Name from prop_otherpaymentmode";
        DataTable dt = new DataTable();
        Connection con = new Connection();
        SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
        dad.Fill(dt);
        ddlOtherPayment.DataSource = dt;
        ddlOtherPayment.DataValueField = "OtherPayment_ID";
        ddlOtherPayment.DataTextField = "OtherPayment_Name";
        ddlOtherPayment.DataBind();
        ddlOtherPayment.Items.Insert(0, "--Choose Other--");

    }
    [WebMethod]
    public static string GetBillByBNF(string BNF)
    {
        Bill objBill = new Bill();
        string OtherPaymentmode = string.Empty;
        string Mode = string.Empty;
        string OTPVal = string.Empty;
        decimal Amount = 0;
        new PayModeBLL().GetBillByBillNowPrefix(BNF, objBill);
        string SqlQuery = "select otherpayment_id,Mode,Cashamt,OnlineAmt,CoupanNo from OnlineOtherpayments where bill_no='" + BNF.ToString() + "'";
        DataTable dt = new DataTable();
        Connection con = new Connection();
        SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
        dad.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            OtherPaymentmode = dt.Rows[0].ItemArray[0].ToString();
            Mode = dt.Rows[0].ItemArray[1].ToString();
            OTPVal = dt.Rows[0].ItemArray[4].ToString();
            if (Mode == "COD")
            {
                Amount = Convert.ToDecimal(dt.Rows[0].ItemArray[2]);
            }
            else
            {
                Amount = Convert.ToDecimal(dt.Rows[0].ItemArray[3]);
            }
        }
        var JsonData = new
        {
            BillData = objBill,
            OtherPaymode = OtherPaymentmode,
            Mode = Mode,
            Amount = Amount,
            OTPVal= OTPVal
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string ChangePaymode(string BillNowPrefix, string BillType, decimal CashAmount, decimal CrCardAmount, string Bank, string BankName, decimal CreditAmount, string CreditCustId, string CreditCustName, decimal OnlinePayment, string OtherPaymentID, string Mode, decimal FinalAmount,string OTPVal)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int retval = 0;
        Connection con = new Connection();
        retval = new PayModeBLL().ChangePaymode(BillNowPrefix, BillType, CashAmount, CrCardAmount, Bank, BankName, CreditAmount, CreditCustId, CreditCustName, OnlinePayment, Branch);
        if (Mode != "" && BillType == "OnlinePayment")
        {
            int OtherPayment = Convert.ToInt32(OtherPaymentID);

            string SqlQuery = "select PaymentModeID from prop_otherpaymentmode where  PaymentModeID<> 0 and OtherPayment_ID=" + OtherPayment;
            DataTable dttable = new DataTable();
            SqlDataAdapter sqldad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
            sqldad.Fill(dttable);
            if (dttable.Rows.Count > 0)
            {
                int CustID = Convert.ToInt32(dttable.Rows[0].ItemArray[0]);
                SqlParameter[] Param = new SqlParameter[7];
                Param[0] = new SqlParameter("@BillNowPrefix", BillNowPrefix);
                Param[1] = new SqlParameter("@CashAmount", ((BillType == "Cash"|| Mode=="COD" && BillType== "OnlinePayment") ? FinalAmount : 0));
                Param[2] = new SqlParameter("@CreditAmount", BillType == "Credit" ? FinalAmount : 0);
                Param[3] = new SqlParameter("@CrCreditAmount", BillType == "CreditCard" ? FinalAmount : 0);
                Param[4] = new SqlParameter("@OnlineAmount", (BillType == "OnlinePayment" && Mode!="COD" ) ? FinalAmount : 0);
                Param[5] = new SqlParameter("@Cust_ID", CustID);
               
                Param[6] = new SqlParameter("@req", "UpdateBillMode");
                SqlHelper.ExecuteNonQuery(con.sqlDataString, CommandType.StoredProcedure, "Proc_sp_OnlineOtherPayments", Param);
            }
            ChangeOnlineOtherPaymentMode(BillNowPrefix, OtherPaymentID, Mode, CashAmount, OTPVal);
        }
        else if (BillType != "OnlinePayment")
        {
            string[] SplitBillNo = BillNowPrefix.Split('-');

            string SqlQuery = "select * from bill_master where bill_no=" + Convert.ToInt32(SplitBillNo[1]);
            DataTable dts = new DataTable();
            SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
            dad.Fill(dts);
            if (dts.Rows.Count > 0)
            {
                SqlParameter[] Param = new SqlParameter[6];
                Param[0] = new SqlParameter("@BillNowPrefix", BillNowPrefix);
                Param[1] = new SqlParameter("@CashAmount", BillType == "Cash" ? FinalAmount : 0);
                Param[2] = new SqlParameter("@CreditAmount", BillType == "Credit" ? FinalAmount : 0);
                Param[3] = new SqlParameter("@CrCreditAmount", BillType == "CreditCard" ? FinalAmount : 0);
                Param[4] = new SqlParameter("@OnlineAmount", BillType == "OnlinePayment" ? FinalAmount : 0);
                Param[5] = new SqlParameter("@req", "BillMode");
                SqlHelper.ExecuteNonQuery(con.sqlDataString, CommandType.StoredProcedure, "Proc_sp_OnlineOtherPayments", Param);
            }
        }
      
        var JsonData = new
        {
            BillData = retval,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    public static void ChangeOnlineOtherPaymentMode(string BillNowPrefix, string OtherPaymentID, string Mode, decimal Amount,string OTPVal)
    {
        decimal ZeroVal = 0;
        Connection con = new Connection();
        SqlParameter[] Param = new SqlParameter[7];
        Param[0] = new SqlParameter("@BillNowPrefix", BillNowPrefix);
        Param[1] = new SqlParameter("@OtherPaymentID", Convert.ToInt32(OtherPaymentID));
        Param[2] = new SqlParameter("@Mode", Mode);
        if (Mode == "COD")
        {
            Param[3] = new SqlParameter("@CashAmount", Amount);
            Param[4] = new SqlParameter("@OnlineAmt", ZeroVal);
        }
        else if (Mode == "OnlinePayment")
        {
            Param[3] = new SqlParameter("CashAmount", ZeroVal);
            Param[4] = new SqlParameter("@OnlineAmt", Amount);
        }
        Param[5] = new SqlParameter("@OTPVal", OTPVal);
        Param[6] = new SqlParameter("@req", "OnilnePaymet");
        SqlHelper.ExecuteNonQuery(con.sqlDataString, CommandType.StoredProcedure, "Proc_sp_OnlineOtherPayments", Param);
    }
    [WebMethod]
    public static List<PaymentMode> BindDropOnlinePayemntmode(string PaymentModeID)
    {
        string SqlQuery = string.Empty;
        List<PaymentMode> OtherPaymentModeList = new List<PaymentMode>();
        if (PaymentModeID == "OnlinePayment")
        {
            SqlQuery = "select OtherPayment_ID,OtherPayment_Name from prop_otherpaymentmode";

            //else
            //    SqlQuery = "select OtherPayment_ID,OtherPayment_Name from prop_otherpaymentmode where PaymentModeID=" + PaymentModeID;
            DataTable dt = new DataTable();
            Connection con = new Connection();
            SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
            dad.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0 && PaymentModeID == "0")
                {
                    OtherPaymentModeList.Add(new PaymentMode() { OtherPaymentModeID = "0", OtherPaymentName = "--Choose Other--" });
                }
                OtherPaymentModeList.Add(new PaymentMode() { OtherPaymentModeID = dt.Rows[i].ItemArray[0].ToString(), OtherPaymentName = dt.Rows[i].ItemArray[1].ToString() });
            }
        }
        return OtherPaymentModeList.ToList();
    }


}