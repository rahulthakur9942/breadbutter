﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class SettingExPrining : System.Web.UI.Page
{
    printer_settings printersetting = new printer_settings();
    protected void Page_Load(object sender, EventArgs e)
    {
        tbdos.Attributes["type"] = "number";
        if (IsPostBack == false)
        {
          
            bindgride();

        }
    }
    protected void bindgride()
    {
        printersetting.req = "bindgrid";

        DataTable dt = printersetting.bindgride();
        gv_display.DataSource = dt;
        gv_display.DataBind();
    }
    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {
        gv_display.PageIndex = e.NewPageIndex;
        gv_display.DataBind();
        bindgride();
    }


    protected void gv_display_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

        string dep_name = e.CommandArgument.ToString();
        ViewState["glbl_dep_name"] = dep_name;
        if (e.CommandName == "select")
        {
            printersetting.req = "get_printersetting";
            printersetting.Dep_Name = dep_name;
            printersetting.get_printersetting();

            tbNet_Del.Value= printersetting.Net_Del;
            tbPrinterName.Value = printersetting.Print_Name;
            tbdeptname.Value = printersetting.Dep_Name;
            tbdos.Value = printersetting.Dos;
            tbnetuser.Value = printersetting.Net_Use;

            btnsave.Text = "Update";
            tbdeptname.Disabled = true;

        }
        else if (e.CommandName == "del")
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('POS will not remove if it is in use!');", true);
            //pos.req = "deletepos";
            //pos.posid = posid;
            //pos.del_pos();
            bindgride();


        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Submit")
        {
            printersetting.req = "insert";
            printersetting.Dep_Name = tbdeptname.Value.Trim();
            printersetting.Print_Name = tbPrinterName.Value.Trim();
            printersetting.Dos = tbdos.Value.Trim();
            printersetting.Net_Del = tbNet_Del.Value.Trim();
            printersetting.Net_Use = tbnetuser.Value.Trim();
            printersetting.InsertUpdatePrinterSettings();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Added Successfully!');", true);
        }
        else
        {
            printersetting.req = "update";
            printersetting.Dep_Name = tbdeptname.Value.Trim();
            printersetting.Print_Name = tbPrinterName.Value.Trim();
            printersetting.Dos = tbdos.Value.Trim();
            printersetting.Net_Del = tbNet_Del.Value.Trim();
            printersetting.Net_Use = tbnetuser.Value.Trim();
            printersetting.InsertUpdatePrinterSettings();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Updated Successfully!');", true);

        }
        tbnetuser.Value = "";
        tbNet_Del.Value = "";
        tbPrinterName.Value = "";
        tbdeptname.Value = "";
        tbdos.Value = "";
        bindgride();

    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        tbnetuser.Value = "";
        tbNet_Del.Value = "";
        tbPrinterName.Value = "";
        tbdeptname.Value = "";
        tbdeptname.Disabled = false;
        tbdos.Value = "";
        btnsave.Text = "Submit";
    }
    }