﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.Runtime.InteropServices;
using Microsoft.ApplicationBlocks.Data;

public partial class manageKotscreen : System.Web.UI.Page
{

  public class chkdelcahrges
  {
    public int delcharges { get; set; }
    public int minamt { get; set; }
    public string searchkey { get; set; }
    public string contactno { get; set; }
    public string address { get; set; }
    public string name { get; set; }
   
  }
  mst_customer_rate msr = new mst_customer_rate();
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    protected void Page_Load(object sender, EventArgs e)
  {
        //Session["postype"] = 3;

        Response.Cookies[Constants.posid].Value = "3";
 
        if (!IsPostBack)
    {
        
      //string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);
            dayopencloseprm();

            //if (strDate == "")
            //{

            //    Response.Redirect("index.aspx?DayOpen=Close");
            //}

            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
      gvTax.DataBind();
      BindSteward();
      CheckRole();
      ddcustomertype();
    }
  }



    public void dayopencloseprm()
    {

        string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);
        string PendingKot = new DayOpenCloseDAL().ChkPendingkot(Branch);

        if (strDate == "" && PendingKot != "")
        {

            hdnsavebtn.Value = "1";
            
            
          
        }
        else if (strDate == "")
        {
            Response.Redirect("index.aspx?DayOpen=Close");

        }

    }
    [WebMethod]
    public static List<PaymentMode> BindDropOnlinePayemntmode(string PaymentModeID)
    {
        string SqlQuery = "select OtherPayment_ID,OtherPayment_Name from prop_otherpaymentmode where PaymentModeID=" + PaymentModeID;
        DataTable dt = new DataTable();
        Connection con = new Connection();
        SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
        dad.Fill(dt);
        List<PaymentMode> OtherPaymentModeList = new List<PaymentMode>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            OtherPaymentModeList.Add(new PaymentMode() { OtherPaymentModeID = dt.Rows[i].ItemArray[0].ToString(), OtherPaymentName = dt.Rows[i].ItemArray[1].ToString() });
        }

        return OtherPaymentModeList.ToList();
    }
    public void ddcustomertype()
  {
    msr.req = "bind_ddcustomer";
    DataTable dt = msr.bind_item_dd();
    dd_customername.DataSource = dt;
    dd_customername.DataTextField = "customer_name";
    dd_customername.DataValueField = "cst_id";
    dd_customername.DataBind();
    System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Shop", "0");
    dd_customername.Items.Insert(0, listItem1);

  }
  public void BindSteward()
  {
    ddlsteward.DataSource = new EmployeeBLL().GetAll();
    ddlsteward.DataTextField = "Name";
    ddlsteward.DataValueField = "Code";
    ddlsteward.DataBind();

    ListItem li1 = new ListItem();
    li1.Text = "Steward";
    li1.Value = "0";
    ddlsteward.Items.Insert(0, li1);



    ddlTableOpt.DataSource = new TablesBLL().GetAll();
    ddlTableOpt.DataTextField = "TableName";
    ddlTableOpt.DataValueField = "TableID";
    ddlTableOpt.DataBind();

        ListItem li2 = new ListItem();
        li2.Text = "Table";
        li2.Value = "0";

        ddlTableOpt.Items.Insert(0, li2);


    //ddlSourceTable.DataSource = new TablesBLL().GetAllKotTables();
    //ddlSourceTable.DataTextField = "TableName";
    //ddlSourceTable.DataValueField = "TableID";
    //ddlSourceTable.DataBind();
    //ddlSourceTable.Items.Insert(0, li1);


    ddlDestTable.DataSource = new TablesBLL().GetAll();
    ddlDestTable.DataTextField = "TableName";
    ddlDestTable.DataValueField = "TableID";
    ddlDestTable.DataBind();

    ddlDestTable.Items.Insert(0, li1);


  }
  public void CheckRole()
  {
    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.KOTSCREEN));
         
    string[] arrRoles = sesRoles.Split(',');

    var roles = from m in arrRoles
                where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                select m;

    int len = roles.Count();
    if (len == 0)
    {
      Response.Redirect("index.aspx");

    }
    else
    {
      Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
    }

  }


  [WebMethod]
  public static string BindTables()
  {

    string TablesData = new TablesBLL().GetOptions();


    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;

    var JsonData = new
    {
      TableOptions = TablesData

    };
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string BindSourceTables()
  {

    string TablesData = new TablesBLL().GetSourceTables();


    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;

    var JsonData = new
    {
      TableOptions = TablesData

    };
    return ser.Serialize(JsonData);
  }



  [WebMethod]
  public static string GetKotDetail(int KotNo)
  {

    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    List<KotDetail> lstKot = new kotBLL().GetByKotNoDetail(KotNo, Branch);

    var JsonData = new
    {
      productLists = lstKot
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }



  [WebMethod]
  public static string BindCompById(int Emp)
  {

    Employees objEmp = new Employees();
    objEmp.Code = Emp;
    new EmployeeBLL().GetCompByEmpId(objEmp);
    var JsonData = new
    {

      EmpData = objEmp,

    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    return ser.Serialize(JsonData);
  }



  [WebMethod]
  public static string BindCategories()
  {
    int CategoryId = 0;
    Settings objSettings = new Settings();
    string catData = new CategoriesBLL().GetCategoriesHTML(objSettings, out CategoryId);
    var JsonData = new
    {
      categoryData = catData,
      setttingData = objSettings,
      CategoryId = CategoryId
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string AdvancedSearch(int CategoryId, string Keyword)
  {
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    string prodData = new ProductBLL().AdvancedSearch(CategoryId, Keyword.Trim(), Branch);
    var JsonData = new
    {
      productData = prodData
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string GetByItemCode(string ItemCode, int billtype)
  {
    Product objProduct = new Product() { Item_Code = ItemCode };
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    new ProductBLL().GetByItemCode(objProduct, Branch, billtype);
    var JsonData = new
    {
      productData = objProduct
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }

  [WebMethod]
  public static string DeleteKot(string ProductCode, decimal KotNo)
  {
    Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
    int Status = new kotBLL().DeleteKot(ProductCode, KotNo, UserNo);
    var JsonData = new
    {
      status = Status
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string InsertUpdate(int MKOTNo, int TableID, int PaxNo, int R_Code, int M_Code, decimal Value, decimal DisPercentage, decimal DisAmount, decimal ServiceCharges, decimal TaxAmt, decimal TotalAmount, bool Complementary
      , bool Happy, int EmpCode, string itemcodeArr, string priceArr, string qtyArr, string AmountArr, string taxArr, string TaxAmountArr, string AddOnArr, string EditValArr, bool TakeAway,string SendMessageCustomerName,string SendMessagePhoneNumber,int cst_id)
  {
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
    KOT objKot = new KOT()
    {
      MKOTNo = MKOTNo,
      TableID = TableID,
      PaxNo = PaxNo,
      R_Code = R_Code,
      M_Code = M_Code,
      Value = Value,
      DisPercentage = DisPercentage,
      DisAmount = DisAmount,
      ServiceCharges = ServiceCharges,
      TaxAmount = TaxAmt,
      TotalAmount = TotalAmount,
      Complementary = Complementary,
      Happy = Happy,
      EmpCode = EmpCode,
      KOTNo = 1,
      BranchId = Branch,
      TakeAway = TakeAway,
      cst_id=cst_id,
    };

    string[] ItemCode = itemcodeArr.Split(',');
    string[] Price = priceArr.Split(',');
    string[] Qty = qtyArr.Split(',');
    string[] Amount = AmountArr.Split(',');
    string[] Tax = taxArr.Split(',');
    string[] TaxAmount = TaxAmountArr.Split(',');
    string[] AddOn = AddOnArr.Split(',');
    string[] EditVal = EditValArr.Split(',');

    DataTable dt = new DataTable();
    dt.Columns.Add("Item_Code");
    dt.Columns.Add("Rate");
    dt.Columns.Add("Qty");
    dt.Columns.Add("Amount");
    dt.Columns.Add("Tax");
    dt.Columns.Add("Tax_Amount");
    dt.Columns.Add("AddOn");


    for (int i = 0; i < ItemCode.Length; i++)
    {
      if (Convert.ToInt16(EditVal[i]) == 0)
      {
        DataRow dr = dt.NewRow();
        dr["Item_Code"] = ItemCode[i];
        dr["Rate"] = Convert.ToDecimal(Price[i]);
        dr["Qty"] = Convert.ToDecimal(Qty[i]);
        dr["Amount"] = Convert.ToDecimal(Amount[i]);
        dr["Tax"] = Convert.ToDecimal(Tax[i]);
        dr["Tax_Amount"] = Convert.ToDecimal(TaxAmount[i]);
        dr["AddOn"] = Convert.ToString(AddOn[i]);
        dt.Rows.Add(dr);
      }
    }




    JavaScriptSerializer ser = new JavaScriptSerializer();
    // Send Message to Customer//
    int status = new kotBLL().Insert(objKot, dt, UserNo);
    Connection con = new Connection();
    SqlParameter[] Param = new SqlParameter[1];
    Param[0] = new SqlParameter("@MessageText", "Order_Msg");
    var Message = SqlHelper.ExecuteScalar(con.sqlDataString, CommandType.StoredProcedure, "Proc_getbillingsms", Param);
    if (Message != null)
    {
      var CustomerName = SendMessageCustomerName;
      var PhoneNo = SendMessagePhoneNumber;
      Message = Message.ToString().Replace("$CN$", CustomerName);
      //SendSms SendMessage = new SendSms();
      //SendMessage.SMSSend(PhoneNo, Message.ToString());
    }

    var JsonData = new
    {
      Bill = objKot,
      BNF = objKot.KOTNo,
      Status = status
    };
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static chkdelcahrges chkdelcharges()
  {
    chkdelcahrges chk = new chkdelcahrges();
        Connection conn = new Connection();
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        SqlConnection con = new SqlConnection(conn.sqlDataString);
    con.Open();
    SqlCommand cmd = new SqlCommand("select * from mastersetting_Basic where BranchId=@BranchId", con);
    cmd.CommandType = CommandType.Text;
    cmd.Parameters.AddWithValue("@BranchId", Branch);
    DataTable dt = new DataTable();
    SqlDataAdapter adp = new SqlDataAdapter(cmd);
    SqlDataReader rd = cmd.ExecuteReader();
    if (rd.Read())
    {

      chk.delcharges = Convert.ToInt32(rd["del_charges"]);
      chk.minamt = Convert.ToInt32(rd["min_bill_value"]);
    }
    rd.Close();
    adp.Fill(dt);
    con.Close();

    return chk;


  }

  [WebMethod]
  public static chkdelcahrges searchcustomer(string searchkey)
  {
    chkdelcahrges chk = new chkdelcahrges();
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("select Customer_name,Contact_No,Address_1+' '+Address_2 as address from customer_master where Contact_No like @searchkey + '%'", con);
    cmd.CommandType = CommandType.Text;
    cmd.Parameters.AddWithValue("@searchkey", searchkey);
    DataTable dt = new DataTable();
    SqlDataAdapter adp = new SqlDataAdapter(cmd);
    SqlDataReader rd = cmd.ExecuteReader();
    if (rd.Read())
    {


      chk.name = rd["Customer_name"].ToString();
      chk.address = rd["address"].ToString();
      chk.contactno = rd["Contact_No"].ToString();
      
    }
    rd.Close();
    adp.Fill(dt);
    con.Close();

    return chk;


  }



  [WebMethod]
  public static string GetKotByTableNo(int TableNo)
  {

    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    List<KOT> lstKot = new kotBLL().GetAllKotByTableNo(TableNo, Branch);

    var JsonData = new
    {
      KOT = lstKot
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }



  [WebMethod]
  public static string TransfrTable(int FromTable, int ToTable, string qry)
  {
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    int SourceTable = FromTable;
    int DestTable = ToTable;
    string fqry = qry;
    JavaScriptSerializer ser = new JavaScriptSerializer();

    int status = new kotBLL().TransferTable(SourceTable, DestTable, Branch, fqry);
    var JsonData = new
    {

      Status = status
    };
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string KOTprint()
  {
    Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
    List<KotPrint> objkot = new kotBLL().GetDataByUserNo(UserNo);

    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;

    var JsonData = new
    {
      kotprint = objkot

    };
    return ser.Serialize(JsonData);

  }

}