﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="manageKotscreen.aspx.cs" EnableEventValidation="false" Inherits="manageKotscreen" %>
   <%@ Register src="~/Templates/CashCustomers.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">  
	<meta name="viewport" content="width=device-width, initial-scale=1">  
	<link rel="stylesheet" href="Touchcss/responsive.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Touchcss/bootstrap.min.css" rel="stylesheet" />
    <link href="Touchcss/css.css" rel="stylesheet" />
    <link href="css/Checkbox.css" rel="stylesheet" type="text/css" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
    <link rel="stylesheet" href="Touchcss/jquery-ui.css">
    <script src="Touchjs/jquery-1.10.2.js"></script>
    
    <script src="Touchjs/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="Touchjs/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Touchcss/bootstrap-glyphicons.css" />
    <link href="Touchjs/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Touchjs/jquery.uilock.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" />
 
<link href="css/bootstrap-select.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/billscreen.css" rel="stylesheet" type="text/css" />
   <link href="css/Calculator/creative.css" rel="stylesheet" />
       <script type="text/javascript" src="js/jquery.uilock.js"></script>
     <link href="semantic.css" rel="stylesheet" type="text/css" />
   <script src="js/Calculator/calculate.js"></script>
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
        <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
  <script src="js/Jquery.numpad.js" type="text/javascript"></script>
    <link href="css/Jquery.numpad.css" rel="stylesheet" />
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="Touchjs/jquery-ui.js"></script>
     <script src="js/customValidation.js" type="text/javascript"></script>
        <img src="images/favicon-16x16.png" />
      <link href="css/css.css" rel="stylesheet" />
   
    <%--<script type="text/javascript" src="Scripts/jquery-1.8.3.min.js"></script>--%>
    <style type="text/css">

        .nav ul li {
            float: left;
            background: #333;
            width: 110px;
            height: 48px;
        }

        .checkbox {
            margin: 0 0 1em 2em;
        }

        .ui-keyboard-button ui-keyboard-61 ui-state-default ui-corner-all {
            display: none;
        }

        .checkbox .tag {
            color: green;
            display: block;
            float: left;
            font-weight: bold;
            position: relative;
            width: 120px;
        }

        .checkbox label {
            display: inline;
            color: White;
        }

        .checkbox .input-assumpte {
            display: none;
        }

        .input-assumpte + label {
            -webkit-appearance: none;
            background-color: white;
            border: 1px solid white;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
            padding: 9px;
            display: inline-block;
            position: relative;
        }

        .input-assumpte:checked + label:after {
    color: #32CD32;
    content: '\2714';
    left: 0px;
    position: absolute;
    top: 0px;
    font-size: 15px;
}

        .box h3 {
            background: #887d73;
        }

        #numericInput {
            position: relative;
        }

        #numBox {
            width: 150px;
            height: 20px;
            text-align: right;
            border: solid 1px black;
            cursor: text;
            overflow: hidden;
            display: none;
            background-color: white;
        }

        #keypad {
            width: 150px;
            height: 200px;
            border: solid 1px black;
            display: none;
            position: absolute;
            top: 22px;
            left: 1px;
        }

        .key {
            border: solid 2px white;
            background-color: #333;
            text-align: center;
            font-weight: bold;
            color: white;
            cursor: pointer;
        }

        .btn {
            border: solid 2px white;
        }

        .button1 {
            background: #f06671 none repeat scroll 0 0;
            border-radius: 3px;
            box-shadow: inset -2px -5px 5px #8d3c42;
            color: #ffffff;
            font-size: 14px;
            height: 50px;
            margin: 0 17px 0px 0;
            padding: 10px 5px 5px;
            text-align: center;
            vertical-align: middle;
            width: 95%;
            cursor: pointer;
        }

        #borders {
            position: relative;
            z-index: 1;
            background-color: #e1e1e1; 
            margin: 0px; 
        }

        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }

        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }

        #tbkotinfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbkotinfo tr td {
                padding: 3px;
            }

        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }

        .Search1 {
            background-color: #333;
            border-radius: 2px;
            float: left;
            margin: 5px 0 14px -8px;
            padding-top: 0px;
            width: 100%;
            padding-bottom: 0px;
            padding-left: 8px;
        }


        /*.Search2 {
        background-color: #333;
        border-radius: 2px;
        float: left;
        margin: 5px 0 14px -8px;
        padding-top: 0px;
        width: 100%;
        padding-bottom: 0px;
        padding-left: 131px;
      }*/


        a:hover, a:focus {
            color: white;
            text-decoration: none;
        }

        .ancBasic {
            background: -webkit-linear-gradient(#04c99d, #6fc9b5);
        }

        .ancSelected {
            background: rgba(0, 0, 0, 0) linear-gradient(#001f02, #3ac6a6) repeat scroll 0 0;
        }

        #tbProductInfo tr {
            background-color: white !important;
        }

        .box {
            width: 97px;
        }

        #dvOuter_ddlMobSearchBox {
            z-index: 0 !important;
        }

        .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable > #getCalculator {
            z-index: 1000 !important;
        }

        .MangeBill {
            background: #f06671 none repeat scroll 0 0;
            font-size: 14px;
            font-weight: bolder;
            cursor: pointer;
            background-color: green;
            width: 98%;
            border-radius: 3px;
            box-shadow: inset -2px -5px 5px #8d3c42;
            color: #ffffff;
            height: 50px;
            text-align: center;
            vertical-align: middle;
            cursor: pointer;
            padding-top: 12px;
        }
    </style>

  <script type="text/javascript">
      $(document).on('click', '#btnCal', function () {
          $("#getCalculator").dialog({
              autoOpen: true,
              width: 393,
              left: 756,
              resizable: false,
              modal: true
          });
      })
  </script>
   <script type="text/javascript">
        $('#<%=dd_customername.ClientID %>').change(function () {
            PaymentModeID = $('#<%=dd_customername.ClientID %> option:selected').val();
            $.ajax({
                type: "POST",
                data: "{'PaymentModeID':" + PaymentModeID + "}",
                url: "ManageKotScreen.aspx/BindDropOnlinePayemntmode",
                contentType: "application/json",
                async: false,
                dataType: "json",
                success: function (msg) {
                    var Target = $("#ddlOtherPayment");
                    Target.empty();
                    $.each(msg.d, function (i, item) {
                        Target.append("<option value=" + item.OtherPaymentModeID + ">" + item.OtherPaymentName + "</option>")
                    });
                },
            });
        });
    </script>
    <script language="javscript" type="text/javascript">

        $('input[type="text"]').click(function () {
            $.ajax({
                type: "POST",
                url: "billscreen.aspx/Keyboard",
                contentType: "application/json",
                dataType: "json",

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {
                }

            });
            $(this).focus();
        });
    
        var cashcustmob = 0;
        var QtyCounter = 1;
        var QtyIndex = 0;
        var Sertax = 0;
        var Takeaway = 0;
        var Takeawaydefault = 0;
        var mode = "";
        var count = 0;
        var modeRet = "";
        var indexR = 0;
        var KotAmt = 0;
        var subtotal = 0;
        var dlcharges = 0;
        var netamt = 0;
        var Tableno = 0;
        var cstname = "";
        var address = "";
        var contactno = "";
        var cashcustcode = "";
        var cashcustName = "";
        var ddlcustid = "";
        var BillNowPrefix = "";
        //.....................................


        $(document).ready(function () {


            if ($("#hdnsavebtn").val() == "1") {

                $("#btnsaveKot").hide();

            }
            else {

                $("#btnsaveKot").show();

            }

        });
        $(function () {
            $("#num").change(function () {
                //                $("#CopyValue").val($(this).val()); 593ed5a41883f 

                var Countprod = 0;

                for (var i = 0; i < ProductCollection.length; i++) {
                    if (ProductCollection[i]["EditVal"] == "0") {
                        Countprod = Countprod + 1;
                    }
                }


                if (Countprod == "0") {
                    
                    alert("No New Item Is Added To KOT");
                    $.uiUnlock();
                    return false;


                }
                else {

                    var RowIndex = QtyIndex;
                    var fQty = $(this).val();
                    ProductCollection[RowIndex]["Qty"] = fQty;
                    var Price = ProductCollection[RowIndex]["Price"];
                    ProductCollection[RowIndex]["ProductAmt"] = Number(fQty) * Number(Price);
                    Bindtr();
                }






            });
        })





        function ResetCashCustmr() {


            $("#txtAddon").val("");
            $("#dvAdd").hide();

            $("#lblCashCustomerName").text("");
            CshCustSelId = 0;
            CshCustSelName = "";
            $("#CashCustomer").css("display", "none");
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");
            $("#txtddlMobSearchBox").val("CASH");
            $("#txtcustId").val("CASH");

        }



        function TakeMeTop() {
            $("html, body").animate({ scrollTop: $(document).height() }, 500);
        }

        function BindTables() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindTables",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    // $("#ddlTableOpt").html(obj.TableOptions);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }


            });


        }


        function BindCompValue(EmpID) {

            $.ajax({
                type: "POST",
                data: '{"Emp":"' + EmpID + '"}',
                url: "manageKotScreen.aspx/BindCompById",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#lblCompValue").html(obj.EmpData.ComplimentryValue);
                    $("#lblcombal").html(obj.EmpData.Balance);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }


            });


        }



        function BindSourceTables() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "managekotscreen.aspx/BindSourceTables",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#ddlSourceTable").html(obj.TableOptions);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }


            });


        }



        //...................................

        var DiscountAmt = 0;
        var Total = 0;
        var DisPer = 0;
        var VatAmt = 0;
        var TaxAmt = 0;




        function RestControls() {

            $("#lblCreditCustomerName").text("");

            $("ddlCreditCustomers").hide();
            $("ddlChosseCredit").hide();
            $("ddlChosseCredit").val(0);
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");
            BillNowPrefix1 = "";
            Type = "";
            $("#CashCustomer").css("display", "none");
            ProductCollection = [];

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
            //              $("#dvdisper").html("0");
            //              $("#dvdiscount").html("0");
            //              $("#dvnetAmount").html("0");
            //              $("#dvTax").html("0");
            //              $("#dvVat").html("0");
            //              $("#dvsertaxper").html("0");
            $("#dvsbtotal").html("0");
            m_ItemId = 0;
            m_ItemCode = "";
            m_ItemName = "";
            m_Qty = 0;
            m_Price = 0;
            m_Vat = 0;
            m_AddOn = "";
            m_KkotNo = 0;

            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
            $("#ddlTableOpt option").removeAttr("selected");
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
            $("#dvBillWindow").hide();
            $("#dd_customername").val("0");
            $("#ddlTableOpt").removeAttr("disabled"); 
            $("#ddlpax").removeAttr("disabled");
            $("#ddlsteward").removeAttr("disabled");
        }



        function kotPrint(BillNowPrefix) {



            $.ajax({
                type: "POST",
                data: '{}',
                url: "manageKotscreen.aspx/KOTprint",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i <= obj.kotprint.length; i++) {



                        Printtkot(BillNowPrefix, obj.kotprint[i]["DepartmentName"], 'Kot', obj.kotprint.length, i);

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });
        }




        var minamt = 0;
        // $("#chkhomedelivery").click(function () { alert('hello') })


        $(document).on('change', '#chkhomedelivery', function () {

            if ($('#chkhomedelivery').is(":checked")) {
                $("#ddlTableOpt").val(0);
                $("#ddlsteward").val(0);
                $("#tdsave2").show();
                $("#tdnsaveKot").hide();
                $("#tdraisbill").hide();
                $("#ddlTableOpt").attr("disabled", "disabled");
                $("#ddlpax").attr("disabled", "disabled");
                $("#ddlsteward").attr("disabled", "disabled");
                $('#chktakeway').prop('checked', false);
                $.ajax({
                    type: 'post',
                    url: "manageKotscreen.aspx/chkdelcharges",
                    contentType: "application/json",
                    dataType: 'json',
                    success: function (data) {
                        if ($('#<%=dd_customername.ClientID %> option:selected').val() == "0") {
                    dlcharges = data.d.delcharges;
                    $("#spn_delcharge").text(dlcharges);
                }

                minamt = data.d.minamt;
                CommonCalculation();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },


          });


        }
        else {
            $("#tdraisbill").show();
            $("#tdsave2").hide();
            $("#tdnsaveKot").show();
            $("#ddlTableOpt").removeAttr("disabled");
            $("#ddlpax").removeAttr("disabled");
            $("#ddlsteward").removeAttr("disabled");
            $("#spn_delcharge").text(0);
        }

          subtotal = parseFloat($("#dvsbtotal").text());
          $("#dvnettotal").html(Total.toFixed(2));
          CommonCalculation();
          //alert('hello');
      })


    $(document).on('change', '#chktakeway', function () {

        if ($('#chktakeway').is(":checked")) {
            $("#tdsave2").show();
            $("#tdnsaveKot").hide();
            $("#tdraisbill").hide();
            $("#ddlsteward").attr("disabled", "disabled");;
            $("#ddlTableOpt").attr("disabled", "disabled");
            $("#ddlpax").attr("disabled", "disabled");
            $('#chkhomedelivery').prop('checked', false);
            $("#spn_delcharge").text(0);
            $("#ddlTableOpt").val(0);
            $("#ddlsteward").val(0);



        }
        else {
            $("#tdraisbill").show();
            $("#tdsave2").hide();
            $("#tdnsaveKot").show();
            $("#ddlTableOpt").removeAttr("disabled");

            $("#ddlpax").removeAttr("disabled");
            $("#ddlsteward").removeAttr("disabled");
            $("#spn_delcharge").text(0);
        }
    });
    function InsertUpdate() {


        if ($('#ChkComplementary').is(":checked")) {
            var Total = $("#dvsbtotal").html();

            var Balance = $("#lblcombal").html();

            if (Number(Total) > Number(Balance)) {
                alert("Your Balance is too low for this billing");
                return;
            }


        }
        var Countprod = 0;

        for (var i = 0; i < ProductCollection.length; i++) {
            if (ProductCollection[i]["EditVal"] == "0") {
                Countprod = Countprod + 1;
            }
        }


        if (Countprod == "0") {

            alert("No New Item Is Added To KOT");
            $.uiUnlock();
            return;

        }
        else {


            var MKotNo = "1";
            Tableno = $("#ddlTableOpt").val();
            var TakeAway = false;
            if ($('#chktakeway').is(":checked")) {
                TakeAway = true;
            }

            //                           if (Tableno == "0") {
            //                               Tableno = "0";
            //                           }

            //                           if (option == "TakeAway") {
            //                               Tableno = "0";
            //                           }

            var pax = $("#ddlpax").val();
            if (pax == "") {
                pax = "1";
            }
            var R_Code = "1";
            var M_Code = "1";
            var Value = KotAmt;
            var DisPer = 0;
            var DisAmt = 0;
            var ServiceCharges = 0;
            var TaxAmt = 0;
            var TotalAmt = KotAmt;
            var cst_id = 0;


            //           var DisPer = $("#dvdisper").html();   
            //           var DisAmt = $("#dvdiscount").html();  
            //           var ServiceCharges =0;     
            //           var TaxAmt = $("#dvTax").html();
            //           var TotalAmt = $("#dvnetAmount").html(); 



            var Complementary = false;
            if ($('#ChkComplementary').is(":checked")) {
                Complementary = true;
            }
            var Happy = false;
            if ($("#chkhappyhours").prop('checked') == true) {
                Happy = true;
            }

            var Empcode = $("#ddlsteward").val();

            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var Tax = [];
            var PAmt = [];
            var Ptax = [];
            var AddOn = [];
            var EditVal = [];


            if (ProductCollection.length == 0) {
                alert("Please first Select ProductsFor Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {


                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Qty[i] = ProductCollection[i]["Qty"];
                Price[i] = ProductCollection[i]["Price"];
                Tax[i] = ProductCollection[i]["TaxCode"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                AddOn[i] = ProductCollection[i]["Addon"];
                EditVal[i] = ProductCollection[i]["EditVal"];

            }

            if ($('#chkhomedelivery').is(":checked")) {

                subtotal = parseFloat($("#dvsbtotal").text());
                Tableno = -1;
                if (subtotal > minamt) {

                    $("#spn_delcharge").text(0);
                    TotalAmt = TotalAmt + 0;


                }
                else {

                    TotalAmt = TotalAmt + parseFloat($("#spn_delcharge").text());

                }

                //TotalAmt = TotalAmt + parseFloat(minamt);

            }
            else { Tableno = $("#ddlTableOpt").val(); }
            var SendMessageCustomerName = $("#SendMessageCustomerName").val();
            var SendMessagePhoneNumber = $("#SendMessagePhoneNumber").val();
         
            cst_id = $('#<%=dd_customername.ClientID %> option:selected').val();
            //if (SendMessageCustomerName == "")
            //{
            //  alert("Please enter customer name/ phone number");
            //  return false;
            //}

          $.ajax({
              type: "POST",
              data: '{ "MKOTNo": "' + MKotNo + '","TableID": "' + Tableno + '","PaxNo": "' + pax + '","R_Code": "' + R_Code + '","M_Code": "' + M_Code + '","Value": "' + Value + '","DisPercentage": "' + DisPer + '","DisAmount": "' + DisAmt + '","ServiceCharges": "' + ServiceCharges + '","TaxAmt": "' + TaxAmt + '","TotalAmount": "' + TotalAmt + '","Complementary": "' + Complementary + '","Happy": "' + Happy + '","EmpCode": "' + Empcode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '","AddOnArr": "' + AddOn + '","EditValArr": "' + EditVal + '","TakeAway": "' + TakeAway + '","SendMessageCustomerName": "' + SendMessageCustomerName + '","SendMessagePhoneNumber": "' + SendMessagePhoneNumber + '","cst_id": "' + cst_id + '"}',
              url: "manageKotscreen.aspx/InsertUpdate",
              contentType: "application/json",
              dataType: "json",
              async:false,
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);
                  BillNowPrefix = obj.BNF;

                  if (obj.Status == 0) {
                      alert("An Error Occured. Please try again Later");
                      return;

                  }

                  else {
                      //alert("KOT Saved Successfully");
                      BindSourceTables();
                      $("#ddlsteward").val(0);
                      $("#ddlpax").val(0);
                      $('#chktakeway').prop('checked', false);
                      if ($("#ddlTableOpt").val() <= 0) {
                          //window.location.reload();
                      }
                  }


              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {


                  //kotPrint(BillNowPrefix);


                  RestControls();
                  $.uiUnlock();
              }

          });





      }

  }


  var option = "";
  $(document).ready(function () {
      $('#<%=dd_customername.ClientID %>').change(function () {
                var checkval = $('#<%=dd_customername.ClientID %>').val();
          if (checkval > 0) {
              $("#ddlTableOpt").attr("disabled", "disabled");
              $("select#ddlTableOpt option").filter(function () {
                  return $(this).val() == "0";
              }).prop('selected', true);

              $("#ddlpax").attr("disabled", "disabled");
              $("select#ddlpax option").filter(function () {
                  return $(this).val() == "0";
              }).prop('selected', true);

              $("#ddlsteward").attr("disabled", "disabled");
              $("select#ddlsteward option").filter(function () {
                  return $(this).val() == "0";
              }).prop('selected', true);
          }
          else {
              $("#ddlTableOpt").removeAttr('disabled');
              $("#ddlpax").removeAttr('disabled');
              $("#ddlsteward").removeAttr('disabled')
          }
      })
        })
  function GetByItemCode(div, ItemCode) {
      var rows = $('#tbProductInfo tr').length;
      var stwr = $("#ddlsteward").attr('disabled')
      var tbl = $("#ddlTableOpt").attr('disabled')
      var billtype = $('#<%=dd_customername.ClientID %> option:selected').val();

          //if ($("#ChkComplementary").prop("checked") == false) {

          //    alert("Choose Table No");
          //    return;
          //}

          //if ($("#ddlsteward").val() == "0") {
          //    alert("Choose Steward First");
          //    return;
          //}

          // if ($("#chktakeway").prop("checked") == false) {
          //     alert("Choose Steward First");
          //     return;
          // }

          //if ($("#chkhomedelivery").prop("checked") == false) {
          //     alert("Choose Table No");
          //     return;
          //}

        if (stwr != 'disabled' && $("#ddlsteward").val() == 0) {

            alert("Choose Steward First");
            return;
        }

        if (tbl != 'disabled' && $("#ddlTableOpt").val() == 0) {

            alert("Choose Table No");
            return;
        }





        $.ajax({
            type: "POST",
            data: '{ "ItemCode": "' + ItemCode + '","billtype": "' + billtype + '"}',
            url: "screen.aspx/GetByItemCode",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                if (obj.productData.ItemID == 0) {
                    alert("No Item Found Corresponding to Code " + obj.productData.Item_Code);
                    $("#txtItemCode").val("").focus();


                }
                else {
                    addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, 0, "", 0, 0)
                    // addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, " ")

                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {




                $.uiUnlock();
            }

        });
        if (rows > -1) {


            $('#<%=dd_customername.ClientID %>').attr("disabled", "disabled");


        }
        Scrolldown();

    }


    function GetKotDetail(KotNo) {


        $.uiLock('');

        $.ajax({
            type: "POST",
            data: '{ "KotNo": "' + KotNo + '"}',
            url: "manageKotscreen.aspx/GetKotDetail",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                ProductCollection = [];

                if (obj.productLists.length != "0") {
                    for (var i = 0; i < obj.productLists.length; i++) {


                        var takeway = obj.productLists[i].TakeAway;
                        if (takeway == true) {

                            $('#chktakeway').prop('checked', true);

                        }
                        else {
                            $('#chktakeway').prop('checked', false);

                        }


                        $("#ddlpax option[value='" + obj.productLists[i]["PaxNo"] + "']").prop("selected", true);
                        $("#ddlsteward option[value='" + obj.productLists[i]["EmpCode"] + "']").prop("selected", true);
                        //addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, 0, obj.productData.AddOn)
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["ItemName"], obj.productLists[i]["SaleRate"], obj.productLists[i]["TaxCode"], 0, obj.productLists[i]["ProductCode"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Qty"], obj.productLists[i]["AddOn"], 1, obj.productLists[i]["KOTNo"]);


                    }
                }
                else {
                    ProductCollection = [];
                    $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                }




            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


                $.uiUnlock();
            }

        });





    }


    function GetKotByTableNo(TableNo) {

        $.uiLock('');

        $.ajax({
            type: "POST",
            data: '{ "TableNo": "' + TableNo + '"}',
            url: "manageKotscreen.aspx/GetKotByTableNo",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                KotCollection = [];
                $('#tbkotinfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                if (obj.KOT.length != "0") {
                    for (var i = 0; i < obj.KOT.length; i++) {

                        //addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, 0, obj.productData.AddOn)
                        addToKotList(obj.KOT[i]["KOTNo"], obj.KOT[i]["KotDate2"], obj.KOT[i]["KotTime"], obj.KOT[i]["TotalAmount"]);
                    }
                }
                else {
                    KotCollection = [];
                    $('#tbkotinfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                    $("#ddlDestTable").val("0");
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                $.uiUnlock();
            }

        });





    }


    function Printtkot(celValue, DeptValue, PType, max, i) {



        var iframe = document.createElement("iframe");
        iframe.setAttribute("id", "reportkot" + i);
        iframe.style.width = 0 + "px";
        iframe.style.height = 0 + "px";
        document.body.appendChild(iframe);
        document.getElementById('reportkot' + i).contentWindow.location = "Reports/PrintKOT.aspx?BillNowPrefix=" + celValue + "&DepartmentName=" + DeptValue;

        if (max == i + 1) {
            DeletePrinter(celValue);
        }

    }


    function DeletePrinter(m_BillNowPrefix) {

        $.ajax({
            type: "POST",
            data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
            url: "BillScreen.aspx/DeletePrinter",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);



            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }



    $(document).ready(
    function () {



        $("#num").val("");


        $("#btnDown").click(function () {


            Countprod = 0;

            for (var i = 0; i < ProductCollection.length; i++) {
                if (ProductCollection[i]["EditVal"] == "0") {
                    Countprod = Countprod + 1;

                }
            }


            if (Countprod == "0") {

                alert("No New Item Is Added To KOT");
                $.uiUnlock();
                return;

            }
            else {
                var TotLen = ProductCollection.length;
                if (TotLen != QtyCounter) {
                    QtyCounter = Number(QtyCounter) + 1;
                    QtyIndex = Number(QtyIndex) + 1;
                    $("#tr" + QtyCounter + "").css("background-color", "grey");
                    var ClrCounter = Number(QtyCounter) - 1
                    $("#tr" + ClrCounter + "").css("background-color", "white");

                }




            }



        });



        $("#btnUp").click(function () {


            var Countprod = 0;

            for (var i = 0; i < ProductCollection.length; i++) {
                if (ProductCollection[i]["EditVal"] == "0") {
                    Countprod = Countprod + 1;
                }
            }


            if (Countprod == "0") {

                alert("No New Item Is Added To KOT");
                $.uiUnlock();
                return;

            }
            else {

                if (QtyCounter != 1) {
                    QtyCounter = Number(QtyCounter) - 1;
                    QtyIndex = Number(QtyIndex) - 1;
                    $("#tr" + QtyCounter + "").css("background-color", "grey");
                    var ClrCounter = Number(QtyCounter) + 1
                    $("#tr" + ClrCounter + "").css("background-color", "white");
                }



            }



        })






        $("[data-labelfor]").click(function () {
            $('#' + $(this).attr("data-labelfor")).prop('checked',
   function (i, oldVal) { return !oldVal; });
        });



        $("#ddlsteward").change(
        function () {
            if ($('#ChkComplementary').is(":checked")) {

                BindCompValue($(this).val());
            }

        }
        );




        $("#ChkComplementary").click(
        function () {
            if ($(this).prop("checked")) {
                $("#ddlTableOpt").attr("disabled", "disabled");
                $("#ddlTableOpt").val("0");
                $("#ddlpax").attr("disabled", "disabled");
                $("#ddlpax").val("0");

            }
            else {
                $("#ddlTableOpt").removeAttr("disabled");
                $("#ddlpax").removeAttr("disabled");
            }
        }
        );


        $("#LblComplementary").click(
        function () {
            if ($(this).prop("checked")) {
                $("#ddlTableOpt").attr("disabled", "disabled");
                $("#ddlTableOpt").val("0");
                $("#ddlpax").attr("disabled", "disabled");
                $("#ddlpax").val("0");

            }
            else {
                $("#ddlTableOpt").removeAttr("disabled");
                $("#ddlpax").removeAttr("disabled");
            }
        }
        );
        BindSourceTables();
        BindTables();
        $("#dvKotList").show();

        $("#btnGetByItemCode").click(
    function () {

        var ItemCode = $("#txtItemCode");

        if (ItemCode.val().trim() == "") {
            ItemCode.focus();

            return;
        }


        GetByItemCode(this, ItemCode.val());
        $("#tr" + QtyCounter + "").css("background-color", "yellow");

    }

    );


        $("#ddlMobSearchBox").supersearch({
            Type: "CashCustomer",
            Caption: "CustName/Mobile ",
            AccountType: "D",
            Width: 50,
            DefaultValue: 0,
            Godown: 0
        });




        $("#ddlSourceTable").change(
     function () {

         var Tableval = $("#ddlSourceTable").val();

         GetKotByTableNo(Tableval);

     });




        $("#ddlTableOpt").change(
     function () {

         var Tableval = $("#ddlTableOpt").val();

         GetKotDetail(Tableval);

     });


        $("#dvSave").click(
        function () {
            var sourcetable = 0;
            var DestTable = 0;
            sourcetable = $("#ddlSourceTable").val();
            if (sourcetable == "0") {
                alert("Choose Source Table First");
                $("#ddlSourceTable").focus();

                return;
            }


            DestTable = $("#ddlDestTable").val();
            if (DestTable == "0") {
                alert("Choose Destination Table");
                $("#ddlDestTable").focus();

                return;
            }

            var KotNos = [];


            if (KotCollection.length == 0) {
                alert("No Kot Against This Table");

                return;
            }

            for (var i = 0; i < KotCollection.length; i++) {


                KotNos[i] = KotCollection[i]["KotNo"];


            }

            $.ajax({
                type: "POST",
                data: '{ "FromTable": "' + sourcetable + '","ToTable": "' + DestTable + '","qry": "' + KotNos + '"}',
                url: "manageKotscreen.aspx/TransfrTable",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    if (obj.Status != 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {
                        alert("Table Transfered Successfully");

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    TransferReset();
                    $.uiUnlock();
                }

            });



        }
     );
        $("#btnsave2").click(function () {
            $.uiLock();
            var Countprod = 0;
            cashcustcode = $("#ddlMobSearchBox").val();
            cashcustmob = $("#ddlMobSearchBox option:selected").attr("phone");

            //cashcustName = $("#txtddlMobSearchBox").val();

            cashcustName = $("#ddlMobSearchBox option:selected").attr("name");

            for (var i = 0; i < ProductCollection.length; i++) {
                if (ProductCollection[i]["EditVal"] == "0") {
                    Countprod = Countprod + 1;
                }
            }
            localStorage.setItem("Tableno", 0);

            if (Countprod == "0") {

                alert("No New Item Is Added To KOT");
                $.uiUnlock();
                return;

            }
            //else if ($("#SendMessageCustomerName").val() == "") {
            //var SendMessageCustomerName = $("#SendMessageCustomerName").val();
            //var SendMessagePhoneNumber = $("#SendMessagePhoneNumber").val();
            //if (SendMessageCustomerName == "") {
            // alert("Please enter customer name/ mobile number");
            // return false;
            // }
            ddlcustid = $('#<%=dd_customername.ClientID %> option:selected').val();
            InsertUpdate();

            
          if ($("#SendMessageCustomerName").val() == "") {
              window.location = ("KotRaiseBill.aspx?Id=A&tblno=" + BillNowPrefix + "&cashcustcode=0&cashcustName=Cash&cashcustmob=0&compnycustid=" + ddlcustid);
          }
          else {
              window.location = ("KotRaiseBill.aspx?Id=A&tblno=" + BillNowPrefix + "&cashcustcode=" + cashcustcode + "&cashcustName=" + cashcustName + "&cashcustmob=" + cashcustmob + "&compnycustid=" + ddlcustid);
          }

        });

          $("#btnExit").click(
          function () {

              window.location = "index.aspx";


          }
          );

          $("#btnTransfer").click(
      function () {
          $("#TransferDialog").dialog({
              autoOpen: true,
              left: 367,
              width: 350,
              resizable: false,
              modal: true
          });



      });
        

          $("#btnSearchcust").click(function () {
              var searchkey = $("#txtgetcustomer").val();
              $.ajax({
                  type: "POST",
                  data: '{ "searchkey": "' + searchkey + '"}',
                  url: "managekotscreen.aspx/searchcustomer",
                  contentType: "application/json",
                  dataType: "json",
                  success: function (msg) {

                      cstname = msg.d.name;
                      address = msg.d.address;
                      contactno = msg.d.contactno;

                      alert(cstname + address + contactno);

                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      var obj = jQuery.parseJSON(xhr.responseText);
                      alert(obj.Message);
                  },
                  complete: function () {

                  }

              });

          });

          $("#btnsaveKot").click(
  function () {
      var Validation = true;
      if ($("#dd_customername").val()>0 && $("#chkhomedelivery").prop('checked') == true)
      {
          Validation = true;
      }
      else if ($("#dd_customername").val() > 0 && $("#chktakeway").prop('checked') == true)
      {
          Validation = true;
      }
      else
      {
          Validation = false;
      }
      if ($("#dd_customername").val() > 0 && Validation == false)
      {
          alert("Please select Home Delivery or Take Away");
          return;
      }
      var Tableval = $("#ddlTableOpt").val();
      $.uiLock();
      if (Tableval != 0) {
          localStorage.setItem("Tableno", Tableval);
      }
      else {
          localStorage.setItem("Tableno", 0);
      }
      InsertUpdate();
      $('#<%=dd_customername.ClientID %>').removeAttr("disabled");

}
);




          //         $("div[id='dvdisper']").html("0");
          //           $("div[id='dvdiscount']").html( "0.00");

          $("#btnSearch").click(
      function () {

          var Keyword = $("#txtSearch");
          if (Keyword.val().trim() != "") {

              Search(0, Keyword.val());
          }
          else {
              Keyword.focus();
          }

      });



          $("#txtSearch").keyup(
          function (event) {

              var keycode = (event.keyCode ? event.keyCode : event.which);

              if (keycode == '13') {


                  var Keyword = $("#txtSearch");
                  if (Keyword.val().trim() != "") {

                      Search(0, Keyword.val());
                  }
                  else {
                      Keyword.focus();
                  }


              }

          }

          );





          $("#txtItemCode").keyup(
          function (event) {






              var keycode = (event.keyCode ? event.keyCode : event.which);

              if (keycode == '13') {



                  if ($(this).val().trim() != "") {

                      GetByItemCode(this, $(this).val());
                      $(this).val("").focus();
                  }
                  else {
                      $(this).focus().val("");
                  }


              }

          }

          );





          //            $("#chktakeway").change(function () {
          //                if ($("#chktakeway").prop('checked') == true) {
          //                    option = "TakeAway";
          //                    $("#ddlTableOpt").attr("disabled", "disabled");
          //                    $("#ddlTableOpt").val(0);

          //                }
          //                else {
          //                    option = "Dine";
          //                    $("#ddlTableOpt").removeAttr("disabled");

          //                }


          //            });




          $.ajax({
              type: "POST",
              data: '{}',
              url: "screen.aspx/BindCategories",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);


                  $("#categories").html(obj.categoryData);
                  AdjustMinues();
                  Sertax = obj.setttingData.SerTax;
                  Takeaway = obj.setttingData.TakeAway;
                  Takeawaydefault = obj.setttingData.TakeAwayDefault;

                  var CatId = obj.CategoryId;

                  Search(CatId, "");
              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {

              }

          }
      );


             function TransferReset() {
              BindSourceTables();
              BindTables();
              $("#ddlSourceTable").val("0");

              $("#ddlDestTable").val("0");
              KotCollection = [];
              $('#tbkotinfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
              $("#TransferDialog").dialog('close');
          }


          $("#dvExit").click(
      function () {

          TransferReset();
      });



          $("#btnRaiseBill").click(
      function () {
          cashcustcode = $("#ddlMobSearchBox").val();
          cashcustmob = $("#ddlMobSearchBox option:selected").attr("phone")
          cashcustName = $("#ddlMobSearchBox option:selected").attr("name");

          if (cashcustcode != 'null' && cashcustName != " " && cashcustmob != 'undefined') {
              var Table = localStorage.getItem("Tableno");
              window.location = ("KotRaiseBill.aspx?Id=A&tblno=0&cashcustcode=0&cashcustName=Cash&cashcustmob=0&compnycustid=0");
          }
          else {
              window.location = ("KotRaiseBill.aspx?Id=A&tblno=0&cashcustcode=" + cashcustcode + "&cashcustName=" + cashcustName + "&cashcustmob=" + cashcustmob + "&compnycustid=" + ddlcustid + "");
          }
      });

          $("#btnSettlement").click(
      function () {
          window.location = "Settlement.aspx"
      }
          );



          $(document).on("click", "#dvClose", function (event) {
              var rows = $('#tbProductInfo tr').length;
              var RowIndex = Number($(this).closest('tr').index());
              var tr = $(this).closest("tr");
              tr.remove();
              if (rows == 1) {

                  $('#<%=dd_customername.ClientID %>').removeAttr("disabled");


    }
            ProductCollection.splice(RowIndex, 1);

            if (ProductCollection.length == 0) {

                //                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");


                RestControls();



            }
            Bindtr();
            CommonCalculation();

        });



          $(document).on("click", "#dvAddon", function (event) {
              indexR = 0;
              indexR = Number($(this).closest('tr').index());
              $("#dvAdd").show();

          });


          $(document).on("click", "a[name='categories']", function (event) {

              $("a[name='categories']").removeClass("ancSelected").addClass("ancBasic");
              $(this).addClass("ancSelected").removeClass("ancBasic");



          });



          $("#btnAddonsave").click(
      function () {
          var fAddon = $("#txtAddon").val();

          ProductCollection[indexR]["Addon"] = fAddon;
          $("#txtAddon").val("");
          $("#dvAdd").hide();
          Bindtr();
      });





      });

var m_ItemId = 0;
var m_ItemCode = "";
var m_ItemName = "";
var m_Qty = 0;
var m_Price = 0;
var m_TaxRate = 0;
var m_Surval = 0;
var m_AddOn = "";
var m_EditVal = 0;

var ProductCollection = [];
function clsproduct() {
    this.ItemId = 0;
    this.ItemCode = "";
    this.ItemName = "";
    this.Qty = 0;
    this.Price = 0;
    this.TaxCode = 0;
    this.SurVal = 0;
    this.ProductAmt = 0;
    this.Producttax = 0;
    this.ProductSurchrg = 0;
    this.TaxId = 0;
    this.Addon = "";
    this.EditVal = 0;
    this.KotNo = 0;
}

var m_KotNo = 0;
var m_KotDate = "";
var m_KotTime = "";
var m_Value = 0;

var KotCollection = [];
function clsKot() {
    this.KotNo = 0;
    this.KotDate = "";
    this.KotTime = "";
    this.Value = 0;

}

$(document).on("keyup", "input[name='txtBillQty']", function (event) {

    var keycode = (event.keyCode ? event.keyCode : event.which);

    if (keycode == '13') {
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];



        var fQty = $(this).val();
        if (isNaN(fQty)) {
            fQty = 1;

        }
        ProductCollection[RowIndex]["Qty"] = fQty;

        Bindtr();

        $("#txtSearch").focus();
    }

});


function Bindtr() {


    DiscountAmt = 0;
    VatAmt = 0;
    Total = 0;
    KotAmt = 0;
    var fPrice = 0;
    $("div[name='vat']").html("0.00");
    $("div[name='amt']").html("0.00");
    $("div[name='sur']").html("0.00");
    var CounterId = 1;
    $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
    for (var i = 0; i < ProductCollection.length; i++) {
        if ((ProductCollection[i]["EditVal"]) == "1") {

            var tr = "<tr id = 'tr" + CounterId + "'><td id = 'td" + CounterId + "' style='width:80px;text-align:center;font-size:10px;font-weight:normal;display:none;'>" + ProductCollection[i]["ItemId"] + "</td><td style='width:180px;text-align:center;font-size:12px;font-weight:normal;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:30px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:35px;width:35px;text-align: center' disabled = 'disabled'>-</div></td><td style='width:40px;text-align:center;font-size:15px;font-weight:normal;'><input type = 'text' style='width:25px' id ='txtBillQty" + CounterId + "' disabled = 'disabled' name = 'txtBillQty' value = '" + ProductCollection[i]["Qty"] + "'/></td><td style='width:30px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:35px;width:35px;text-align: center' disabled = 'disabled'>+</div></td><td style='width:50px;text-align:center;font-size:15px;font-weight:normal;'>" + ProductCollection[i]["Price"] + "</td><td style='width:80px;text-align:center;font-size:15px;font-weight:normal;'>" + ProductCollection[i]["ProductAmt"] + "</td><td style='width:80px;text-align:center'>" + ProductCollection[i]["Addon"] + "</td><td style='width:50px;text-align:center'><i id='dvClose1" + CounterId + "' onclick='javascript:DeleteRow(" + ProductCollection[i]["ItemCode"] + "," + ProductCollection[i]["KotNo"] + "," + CounterId + ");'  style='cursor:pointer'><img src='images/trash.png' margin-left: 18px;padding-right: 20px;/></i></td><td style='width:50px;text-align:center'></td></tr>";
            CounterId++;
        }
        else {

            var tr = "<tr id = 'tr" + CounterId + "'><td id = 'td" + CounterId + "' style='width:80px;text-align:center;font-size:15px;font-weight:normal;display:none;'>" + ProductCollection[i]["ItemId"] + "</td><td style='width:180px;text-align:center;font-size:12px;font-weight:normal;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:30px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:35px;width:35px;text-align: center'>-</div></td><td style='width:40px;text-align:center;font-size:15px;font-weight:normal;'><input type = 'text' style='width:25px' id ='txtBillQty" + CounterId + "'  name = 'txtBillQty' value = '" + ProductCollection[i]["Qty"] + "'/></td><td style='width:30px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:35px;width:35px;text-align: center'>+</div></td><td style='width:50px;text-align:center;font-size:15px;font-weight:normal;'>" + ProductCollection[i]["Price"] + "</td><td style='width:80px;text-align:center;font-size:15px;font-weight:normal;'>" + ProductCollection[i]["ProductAmt"] + "</td><td style='width:50px;text-align:center'><i id='dvAddon' style='cursor:pointer'><img src='images/addon.png'/></i></td><td style='width:80px;text-align:center'>" + ProductCollection[i]["Addon"] + "</td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png' padding-right: 20px;/></i></td></tr>";
            var amt = 0;
            amt = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);

            KotAmt = KotAmt + amt;
            CounterId++;
        }


        $("#tbProductInfo").append(tr);
        fPrice = 0;
        fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
        var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / 100);
        var surchrg = (Number(TAx) * Number(ProductCollection[i]["SurVal"])) / 100;
        var tottax = TAx + surchrg;
        VatAmt = VatAmt + tottax;
        Total = Total + fPrice;
        ProductCollection[i]["ProductAmt"] = fPrice;
        ProductCollection[i]["Producttax"] = TAx;
        ProductCollection[i]["ProductSurchrg"] = surchrg;

        var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
        var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
        var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
        $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
        $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
        $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


        $(function () {
            var wtf = $('#dvProductInfo');
            var height = wtf[0].scrollHeight;
            wtf.scrollTop(height);
        });

        CommonCalculation();

    }

    if (ProductCollection.length == 0) {
        var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
        $("#tbProductInfo").append(tr);
    }
    $("#tr" + QtyCounter + "").css("background-color", "#db3030");
    var ClrCounter = Number(QtyCounter) - 1
    $("#tr" + ClrCounter + "").css("background-color", "white");
    $("#num").val("");


}


function DeleteRow(ItemId, KotNo, counterId) {

    if (confirm("Are You Sure you want To Delete the record?")) {
        $.ajax({
            type: "POST",
            data: '{"ProductCode": "' + ItemId + '","KotNo": "' + KotNo + '"}',
            url: "manageKotscreen.aspx/DeleteKot",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                var tr = $("#dvClose1" + counterId).closest("tr");
                tr.remove();


                GetKotDetail(Tableval);
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

            }

        }
  );
    }

}

function CommonCalculation() {

    $("div[id='dvsbtotal']").html(Total.toFixed(2));
    $("div[id='dvVat']").html(VatAmt.toFixed(2));
    TaxAmt = GetServiceTax();

    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
    $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
    subtotal = parseFloat($("#dvsbtotal").text());
    if ($('#chkhomedelivery').is(":checked")) {
        if ($('#<%=dd_customername.ClientID %> option:selected').val() == "0") {
        if (subtotal > minamt) {

            $("#spn_delcharge").text(0);
            $("#dvnettotal").html(Total.toFixed(2));

        }
        else {

            $("#spn_delcharge").text(dlcharges);
            var netamt = parseFloat(dlcharges) + parseFloat(Total);
            $("#dvnettotal").text(netamt.toFixed(2));

        }
    }
    else {
        $("#dvnettotal").html(Total.toFixed(2));
    }
}
}


function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, B_Qty, AddOn, EditVal, KotNo) {

    m_AddOn = AddOn;
    m_ItemId = ProductId;
    m_ItemCode = Code;
    m_Price = Price;
    m_ItemName = Name;
    m_EditVal = EditVal;
    m_KkotNo = KotNo;
    if (B_Qty == 0) {
        if (modeRet == "Return") {
            m_Qty = -1;
        }
        else {
            m_Qty = 1;
        }
    }
    else {
        m_Qty = B_Qty;
    }
    m_TaxRate = TaxCode;
    m_Surval = SurVal;
    m_Tax_Id = Tax_Id;

    //         var item = $.grep(ProductCollection, function (item) {
    //             return item.ItemId == m_ItemId;
    //         });

    //         if (item.length) {

    //               for (var i = 0; i < ProductCollection.length; i++)
    //               {
    //                   if(ProductCollection[i]["ItemId"] == m_ItemId)
    //                   {
    //                       var qty = ProductCollection[i]["Qty"];
    //                       qty = qty+1;
    //                       ProductCollection[i]["Qty"] = qty ;
    //                       Bindtr();
    //                       return;
    //                   }
    //               }

    //         }

    TO = new clsproduct();
    var ExistItem = "";
    for (var i = 0; i < ProductCollection.length; i++) {
        if (m_ItemName === ProductCollection[i].ItemName) {
            ExistItem = "Exist";
        }
    }
    var Amount = 0;
    var Qty = 0;
    if (ProductCollection.length == 0) {
        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ProductAmt = m_Price;
        TO.Addon = m_AddOn;
        TO.EditVal = m_EditVal;
        TO.KotNo = m_KkotNo;
        ProductCollection.push(TO);
    }
    else if (ExistItem != "") {
        for (var i = 0; i < ProductCollection.length; i++) {
            if (m_ItemName === ProductCollection[i].ItemName) {
                Amount = (parseFloat(Amount) + parseFloat(ProductCollection[i].Price));
                Qty = parseFloat(ProductCollection[i].Qty);
                var TotalQty = parseFloat(Qty) + parseFloat(m_Qty)
                var TotalPrice = parseFloat(TotalQty) * parseFloat(m_Price);
                ProductCollection[i].Qty = parseFloat(Qty) + parseFloat(m_Qty);
                ProductCollection[i].ProductAmt = TotalPrice;
            }
        }

    }
    else {
        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ProductAmt = m_Price;
        TO.Addon = m_AddOn;
        TO.EditVal = m_EditVal;
        TO.KotNo = m_KkotNo;
        ProductCollection.push(TO);
    }

    Bindtr();
    $("#tbProductInfo tbody tr").last().children().find('input').eq(0).select();
    var textControlID = $("#tbProductInfo tbody tr").last().children().find('input').eq(0).attr('id')


}


function addToKotList(KotNo, KotDate, KotTime, Value) {

    m_KotNo = KotNo;
    m_KotDate = KotDate;
    m_KotTime = KotTime;
    m_Value = Value;

    TO = new clsKot();

    TO.KotNo = m_KotNo;
    TO.KotDate = m_KotDate
    TO.KotTime = m_KotTime;
    TO.Value = m_Value;


    KotCollection.push(TO);

    BindKottr();

}



function BindKottr() {

    $('#tbkotinfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
    for (var i = 0; i < KotCollection.length; i++) {

        var tr = "<tr><td style='width:80px;text-align:center'>" + KotCollection[i]["KotNo"] + "</td><td style='width:80px;text-align:center'>" + KotCollection[i]["KotDate"] + "</td><td style='width:80px;text-align:center'>" + KotCollection[i]["KotTime"] + "</td><td style='width:80px;text-align:center'>" + KotCollection[i]["Value"] + "</td></tr>";

        //  var tr = "<tr> <td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td>";
        //             tr=tr+"<td style='text-align:center'><table ><tr style='border:0px'><td><div id='btnMinus' class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>-</div></td><td>" + ProductCollection[i]["Qty"] + " </td><td><div id='btnPlus'  class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>+</div></td></tr></table> </td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
        //            

        $("#tbkotinfo").append(tr);


    }

}






function GetServiceTax() {


    var TaxAmt = 0;

    $("#dvsertaxper").html(Sertax);


    if (option == "TakeAway") {
        if (Takeaway == "1") {
            TaxAmt = (Number(Total) * Number(Sertax)) / 100;
            $("#dvTax").html(TaxAmt.toFixed(2));


        }
        else {
            $("#dvsertaxper").html("0");
            TaxAmt == "0";
            $("#dvTax").html(TaxAmt.toFixed(2));
        }
    }
    else if (option == "Dine") {
        TaxAmt = (Number(Total) * Number(Sertax)) / 100;
        $("#dvTax").html(TaxAmt.toFixed(2));


    }


    return TaxAmt;

}


function Search(CatId, Keyword) {



    $.ajax({
        type: "POST",
        data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
        url: "screen.aspx/AdvancedSearch",
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {

            var obj = jQuery.parseJSON(msg.d);

            $("#products").html(obj.productData);


        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {

        }

    }
);


}


//........................................


function GetPluginData(Type) {

    if (Type == "CashCustomer") {


        var Discount = $("#ddlMobSearchBox option:selected").attr("discount");
        $("#lblcustmobno").text($("#ddlMobSearchBox option:selected").attr("phone"));
        $("#txtcustId").val($("#ddlMobSearchBox option:selected").val());
        CshCustSelId = $("#ddlMobSearchBox option:selected").val();
        CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");
        $("#SendMessageCustomerName").val(CshCustSelName);
        $("#SendMessagePhoneNumber").val($("#ddlMobSearchBox option:selected").attr("phone"));
        $("#CashCustomer").css("display", "block");

        if (EnableCustomerDiscount == 1) {
            $("#dvdisper").val(Discount.toFixed(2));

        }

        $("#CustomerSearchWindow").hide();
        $("#dvProductList").show();
        $("#dvBillWindow").hide();
        $("#dvHoldList").hide();
        $("#dvOrderList").hide();
        CommonCalculation("0");





    }
    else if (Type == "Accounts") {
        var customerId = $("#ddlCreditCustomers").val();
        CrdCustSelId = customerId;
        $("#hdnCreditCustomerId").val(customerId);


        $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
        CrdCustSelName = $("#ddlCreditCustomers option:selected").text();



        $("#creditCustomer").css("display", "block");

    }
}

$(document).on("click", "#btnPlus", function (event) {


    var RowIndex = Number($(this).closest('tr').index());
    var PId = ProductCollection[RowIndex]["ItemId"];

    var Mode = "Plus";

    var Qty = ProductCollection[RowIndex]["Qty"];
    var Price = ProductCollection[RowIndex]["Price"];
    var fQty = 0;
    if (Qty < 0) {
        fQty = Number(Qty) + (-1);
    }
    else {
        fQty = Number(Qty) + 1;
    }




    ProductCollection[RowIndex]["Qty"] = fQty;

    Bindtr();

});


$(document).on("click", "#btnMinus", function (event) {

    var RowIndex = Number($(this).closest('tr').index());
    var PId = ProductCollection[RowIndex]["ItemId"];
    var Mode = "Minus";

    var Qty = ProductCollection[RowIndex]["Qty"];
    var Price = ProductCollection[RowIndex]["Price"];

    var fQty = 0;
    if (Qty < 0) {
        fQty = Number(Qty) - (-1);
    }
    else {
        fQty = Number(Qty) - 1;
    }

    ProductCollection[RowIndex]["Qty"] = fQty;
    if (fQty == "0") {
        ProductCollection.splice(RowIndex, 1);
    }

    Bindtr();


});



//............................................







    </script>

    


    <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script src="js/Jquery.numpad.js" type="text/javascript"></script>
    <link href="css/jquery.numpad.css" rel="stylesheet" type="text/css" />
        <script>




            $(function () {
                $('#num')
    .keyboard({
        layout: 'num',
        restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
        preventPaste: true,  // prevent ctrl-v and right click
        autoAccept: true



    })
    .addTyping();


            });

	</script>



</head>


<body  id="borders" class="double-border";>

    <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
    <iframe id="reportkot" width="0" height="0" onload="processingComplete()"></iframe>


	
    <form id="Form1" runat="server">
    <input type="hidden" id="hdnnetamt" value="0" />
    <input type="hidden" id=" " value="0" />
 
        <div class="row" style="max-width: 1300px;overflow-y:auto;width:103%;overflow-x:hidden" id="MenuScroll"">
        
           <div class="nav menu_tabs">
                <ul id="categories" style="float: left;margin-left: 0px;width: 10000px;">
                 
                </ul>
            </div>
          
        </div>

			<div class="row" >
               <div class="search_main">
               <div class="col-xs-7">
                 <div class="Search1 search_section kotscreen_search_section">
                     <table>
		                 <tr>
                         <td>
                             <asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList>
                        </td>
<%--		   <td >
            <label style="color:White;font-size:15px;background-color:black;border-style:solid;border-width:1px;width: 68px;height:40px;text-align: center;padding-top: 8px;margin-left: -2px;"> Table</label></td>--%>
                        <td>
                          <asp:DropDownList id="ddlTableOpt" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
                        </td>
           <%--<td class="checkbox" style="color:White;font-weight: bold;font-size:15px;background-color:black;border-style:solid;border-width:1px;padding-left:5px;height:40px;margin-left:6px;">
  <label for="ChkComplementary" font-color="white"style="color:White;font-weight: bold;font-size:15px;background-color:black;border-style:none;border-width:1px;padding-left:5px;height:40px;margin-left:-2px;">Complementary</label>
  <input type="checkbox" class="input-assumpte" id="ChkComplementary"style="font-size:25px;height:25px;width:55px;margin-top:6px;" data-index="27" />
  <label for="ChkComplementary"  font-color="white"style="color:White;font-size:15px;background-color:white;border-style:solid;border-width:1px;padding-left:5px;height:27px;width: 28px;margin-left:10px;margin-top:6px;"></label>

         </td> --%>
                        <td class="checkbox" >
                          <label for="ChkComplementary" font-color="white">Complementary</label>
                          <input class="input-assumpte" id="ChkComplementary" data-index="27" type="checkbox">
                          <label for="ChkComplementary" class="check" font-color="white"></label>
                        </td>

                        <td class="checkbox">
                         <label for="chkhappyhours" font-color="white" >Happy Hour's</label>
                            <input class="input-assumpte" id="chkhappyhours" data-index="27" type="checkbox">
                         <label for="chkhappyhours" class="check" font-color="white" ></label>
                        </td>
                       </tr>

                        <tr>
       
         <%--    <td >
            <label style="color:White;font-size:15px;background-color:black;border-style:solid;border-width:1px;padding-left:5px;height:40px;margin-left: 3px;padding-top: 9px;margin-top:-15px;width: 76px;">Steward</label>
           </td>--%>
            <td>
           <%--<asp:DropDownList  CssClass="selectpicker"  ID ="ddlsteward" runat="server"></asp:DropDownList>--%>
            <asp:DropDownList id="ddlsteward" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
           </td>
     <%--      <td >
            <label style="color:White;font-size:15px;background-color:black;border-style:solid;border-width:1px;height:40px;width: 68px;height: 39px;text-align: center;padding-top: 6px;margin-left: -12px;margin-top:-15px;"> Pax</label>
           </td>--%>
        
            <td>
         <%--  <asp:DropDownList  CssClass="selectpicker btn3"  ID ="ddlpax" runat="server">--%>

          <asp:DropDownList id="ddlpax" class="form-control" ClientIDMode="Static" runat="server">
           <asp:ListItem Value="0">Pax</asp:ListItem>
           <asp:ListItem Value="1">1</asp:ListItem>
           <asp:ListItem Value="2">2</asp:ListItem>
           <asp:ListItem Value="3">3</asp:ListItem>
           <asp:ListItem Value="4">4</asp:ListItem>
           <asp:ListItem Value="5">5</asp:ListItem>
           <asp:ListItem Value="6">6</asp:ListItem>
           <asp:ListItem Value="7">7</asp:ListItem>
           <asp:ListItem Value="8">8</asp:ListItem>
           <asp:ListItem Value="9">9</asp:ListItem>
           <asp:ListItem Value="10">10</asp:ListItem></asp:DropDownList>
           </td>

           <td class="checkbox">
              <label for="chkhomedelivery" font-color="white" >Home Delivery</label>
                <input class="input-assumpte" id="chkhomedelivery" type="checkbox"/>
             <label for="chkhomedelivery" class="check" id="delchk" font-color="white"></label>
           </td>  

           <td class="checkbox" class="checkbox">
              <label for="chktakeway" font-color="white">Take Away</label>
                 <input class="input-assumpte" id="chktakeway" data-index="27" type="checkbox">
              <label for="chktakeway" class="check" font-color="white"></label>
           </td> 
         </tr>
     </table>

           </div>
             </div>

                	<asp:HiddenField ID = "hdnsavebtn" runat="server" ClientIDMode = "Static" />
     <div  class="col-xs-5">
         <%--  <h3 style="color:white;font-weight:bold;margin-left: 185px;margin-top: -29px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  KOT SCREEN</h3> --%>
          
           <div class="Search2 search_section kotscreen_search_section2">
                    <div class="input-group" style="display:none;">
                        <table width="100%">
                         <tr>
                             <td>Comp Value:</td>
                             <td><asp:Label ID ="lblCompValue" runat="server">0.00</asp:Label></td>
                             <td>Balance:</td>
                             <td><asp:Label ID ="lblcombal" runat="server">0.00</asp:Label></td>
                         </tr>
                        </table>
                    </div>
           
					<div class="input-group">
					   <table>
                          <tr>
                             <td class="txt"> 
                                <select id="ddlMobSearchBox" class="txt"> </select>
                                   <%-- <input class="txt form-control ui-keyboard-input ui-widget-content ui-corner-all" placeholder="Mobile no" aria-describedby="basic-addon2" id="txtcustmobno" style="padding: 0px 5px;width: 138px;border: 0px;height: 45px;margin-top: -46px;margin-left: -142px;float:  right;position:  relative;" aria-haspopup="true" role="textbox" type="text">--%>
                                <label  id="lblcustmobno"></label>
                                <span><img id="btnAddCustomer" src="images/adduser-white.png"/></span>
                             </td>
                          </tr>
                                
                          <tr>
                            <td>
                              <input class="txt form-control ui-keyboard-input ui-widget-content ui-corner-all" placeholder="Enter Code" aria-describedby="basic-addon2" id="txtItemCode" aria-haspopup="true" role="textbox" type="text">
                                 <span  id="btnGetByItemCode" class="kot_btnGetByItemCode" value="Search">
                                 <img src="images/plusicon.png" alt=""></span>
                            </td>
                            <td>
                                <div id="wrap">
                                    <input class="txt form-control ui-keyboard-input ui-widget-content ui-corner-all" placeholder="Search By Name" aria-describedby="basic-addon2" id="txtSearch" aria-haspopup="true" role="textbox" type="text">
                                    <span id="btnSearch" class="kot_btnGetByItemCode" value="Search">
                                    <img src="images/search-button.png" alt="">
                                </div>
                            </td>
                          </tr>
                   
                          <tr>
                               <%--<td>
                                   <table width="100%" id="CashCustomer" style="display: none; border: dashed 1px silver;background: cornsilk;margin-top: -98%;
                                     border-collapse: separate; border-spacing: 1" cellpadding="2">
                                    <tr>
                                    <td style="float:  right;background:  black;"> <img src="images/close-icon1.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                                   </tr>
                                   <tr>
                                  <td>
                                     <label id="lblCashCustomerName" style="font-weight: normal;overflow-y: scroll;height: 94px;"></label>
                                  </td>
                                    <asp:HiddenField ID="SendMessageCustomerName" runat="server" />
                                    <asp:HiddenField ID="SendMessagePhoneNumber" runat="server" />
                                  <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                           </tr>
                           <%--     <tr><td>
                                   </td></tr>--%>
                          </table>
                             <%--<td ><label  id="lblCashCustmorName" style="font-weight:normal;margin-top:1px"   ></label></td><td ><label  id="lblCustmorAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                            <asp:HiddenField ID="SendMessageCustomerName" runat="server" />
                           <asp:HiddenField ID="SendMessagePhoneNumber" runat="server" />
                             <%--          <table width="100%" id="CashCustomer" style="display: none;color: wheat;border: dashed 0px silver;
                          border-collapse: separate; border-spacing= "1" cellpadding="2"; margin:0 auto !important;>
                            <tbody style="float:  left;">
                          <tr>
                            <td>
                               <label id="lblCashCustomerName" style="font-weight: normal;margin-top: 1px;overflow-y: scroll;width: 336px;height: 81px;margin-left: -15px;"></label>
                            </td>
                            <td style="padding-left:7px"> <img src="images/close-icon1.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                          </tr>
                        </tbody>
                      </table>--%>
                   </div>
                </div>
             </div> 
             </div>
         </div>

        <div class="row" id="TransferDialog" style="display:none">

<div class="x_panel table_transfer">
  <div class="x_title">
                                    <h2>Table Transfer</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                  
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="formbarcode" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>


                                     <tr>
                                         <td> <label class="table_lable">Source Table  </label></td>
                                         <td> <label class="table_lable">Destination Table  </label></td>
                                     </tr>

                                     <tr>
                                         <td><asp:DropDownList class="table_select" ID ="ddlSourceTable" runat="server"></asp:DropDownList></td>
                                         <td><asp:DropDownList class="table_select" ID ="ddlDestTable" runat="server"></asp:DropDownList> </td>
                                        </tr>
               
                                    <tr>
                                    <td colspan = "2">

                                     <div id="dvkotlist" class="table_transfer_list" style="display: block;">
                               <div class="leftside">    
                        <table style="width: 100%">
                            <tr>
                                <th style="width:80px; text-align: center; color: #FFFFFF;">
                                    KotNo
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Time
                                </th>
                                 <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                   Value
                                </th>
                               
                            </tr>
                        </table>
                    </div>  
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll; margin-left:-4px">
                        <table style="width: 100%; font-size: 10px" id="tbkotinfo">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                  
                                </td>
                            </tr>
                        </table>
                    </div>
                               </div>     
                                    </td>
                                    
                                    
                                    </tr>
                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             <tr>

                              <table>
                             <tr>
                                        <td>

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="dvSave" style="display:block;width:70px" class="btn btn-primary" ><i class="fa fa-external-link"></i> Save</div>
                                          
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-12" style="padding-left:20px">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="dvExit" style="display:block;width:70px" class="btn btn-primary" ><i class="fa fa-external-link"></i> Exit</div>
                                           </div>
                                       

                                            
                                            </div>
                                         
                                        </div>
                                        
                                        </td>
                             </tr>
                             </table>
                             </tr>
                             </table>
                             
                           
                                    
                                       

                                    </form>
                                </div>
                            </div>
</div>


   


        <div class="row bill_detail_screen1">
            <div class="col-xs-7">
            <div class="manage_kotscreen_left">
                <div id="dvProductList" class="product_items" style="display: block">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 30px;text-align: center;color: #FFFFFF;">
                                    Name
                                </th>
                                <th style="width: 30px;text-align: center;color: #FFFFFF;">
                                    Qty
                                </th>
                                <th style="width: 30px;text-align: center;color: #FFFFFF;">
                                    Price
                                </th>


                                 <th style="width: 30px;text-align: center;color: #FFFFFF;">
                                Amt
                                </th>
                               
                                 <th style="width: 30px;text-align: center;color: #FFFFFF;">
                                    Add-Ons
                                </th>
                                
                               
                            </tr>
                        </table>
                    </div>

                    <div class="cate" id="dvProductInfo">
                        <table id="tbProductInfo" style="width: 100%; font-size: 10px;">
                            <tr>
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px;">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="CustomerSearchWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        CUSTOMER SEARCH
                    </div>
                    <div class="cate">
                        <div id="dvSearch">
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith" value="S" name="searchcriteria" />
                                                    <label for="rbStartingWith" style="font-weight: normal">
                                                        Start With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining" checked="checked" name="searchcriteria" />
                                                    <label for="rbContaining" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith" value="E" name="searchcriteria" />
                                                    <label for="rbEndingWith" style="font-weight: normal">
                                                        End With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact" name="searchcriteria" value="EX" />
                                                    <label for="rbExact" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control input-small" placeholder="Enter Customer Name"
                                                        style="width: 196px; padding: 5px; margin-right: 5px; margin-bottom: 5px; margin-top: 5px"
                                                        id="txtSearch1" />
                                                </td>
                                                <td>
                                                    <div id="btnSearch1" class="btn btn-primary btn-small">
                                                        Search</div>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                        Close</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="dvBillWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>
                    <div class="cate">
                        <table style="border-collapse: separate; border-spacing: 1">
                            <tr>
                                <td>
                                    Amt:
                                </td>
                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" id="txtFinalBillAmount" />
                                </td>
								
                                <td style="padding-left: 10px; color: white; font-size: 16px; font-weight: bold">Comp Value:</td>
								<td style="padding-left:10px;color:white;font-size:16px;font-weight:bold">
								<span id="lblCompValue">0.00</span></td>
								<td style="padding-left:10px;color:white;font-size:16px;font-weight:bold">Balance:</td>
								<td style="padding-left:10px;color:white;font-size:16px;font-weight:bold"><span id="lblcombal">0.00</span></td>
                                <td>
                                    <select id="ddlbillttype" style="height: 25px; width: 70px; padding-left: 0px" class="form-control">
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="100%">
                                    <table width="100%" id="creditCustomer" style="display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            <td style="padding-left: 10px">
                                                <div style='cursor: pointer' id="dvGetCreditCustomers">
                                                    <img src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" /></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="lblCashHeading" style="font-weight: normal">
                                        Cash Rec:</label>
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="txtCashReceived" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Credit Card
                                </td>
                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="Text13" />
                                </td>
                                <td>
                                    Type:
                                </td>
                                <td>
                                    <select id="ddlType" style="height: 25px; width: 90" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cc No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text14" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cheque:
                                </td>
                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" id="Text15"
                                        value="0" />
                                </td>
                                <td>
                                    Bank:
                                </td>
                                <td>
                                    <select id="ddlBank" style="height: 25px; width: 90px" class="form-control">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text16" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1">
                                        <tr>
                                            <td style="width: 64px">
                                                Balance:
                                            </td>
                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px" id="txtBalanceReturn"
                                                    readonly="readonly" />
                                            </td>
                                            <td colspan="100%">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk" class="btn btn-primary btn-small" style="margin: 2px">
                                                                Generate Bill</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose" class="btn btn-primary btn-small">
                                                                Close</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="dvCreditCustomerSearch" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Credit Customers Search
                    </div>
                    <div class="cate">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div id="dvLeft1" style="border: 1px solid silver">
                                        <table cellpadding="5" cellspacing="0" width="100%" style="background: silver;">
                                            <tr>
                                                <td style="padding: 5px">
                                                    <input type="radio" id="rbPhoneNo1" value="M" checked="checked" name="searchon1" />
                                                    <label for="rbPhoneNo1" style="font-weight: normal">
                                                        Phone No.</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbCustomerName1" value="N" name="searchon1" />
                                                    <label for="rbCustomerName1" style="font-weight: normal">
                                                        Customer Name</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="dvRight1" style="float: left; width: 100%; border: 1px solid silver; display: none">
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr style="background-color: #E6E6E6">
                                                <td colspan="100%">
                                                    Search Criteria:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith1" value="S" name="searchcriteria1" />
                                                    <label for="rbStartingWith1" style="font-weight: normal">
                                                        Starting With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining1" checked="checked" name="searchcriteria1" />
                                                    <label for="rbContaining1" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1" />
                                                    <label for="rbEndingWith1" style="font-weight: normal">
                                                        Ending With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact1" name="searchcriteria1" value="EX" />
                                                    <label for="rbExact1" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control input-small" style="width: 190px" placeholder="Enter Search Keyword"
                                                    id="Txtsrchcredit" />
                                            </td>
                                            <td>
                                                <div id="btncreditsrch" class="btn btn-primary btn-small">
                                                    Search</div>
                                            </td>
                                            <td style="padding-left: 5px">
                                                <div onclick="javascript:OpenBillWindow();" class="btn btn-primary btn-small">
                                                    Close</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="jQGridDemoCredit">
                                    </table>
                                    <div id="jQGridDemoPagerCredit">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>


                 <div id="dvAdd" style="display: none">
                    <div>
                        <table style="width: 100%">
                              <tr><td colspan="100%"><table><tr><td style="color:White;font-size:18px">ADD-ONS</td>
                              <td  style="padding:12px"><input type="text" id ="txtAddon" style="width:200px" /></td><td><div id = "btnAddonsave" style="width:60px;height:40px" class="btn btn-success" >Add</div>
                              </td>
                               <td style="padding-left:50px"> <img src="images/close-icon.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                              </tr></table></td></tr>
                        </table>
                    </div>               
                </div>

                <div id="dvHoldList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Hold No
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 120px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbHoldList">
                        </table>
                    </div>
                </div>

                <div class="cate2">
                    <table id="tbamountinfo" style="width: 100%;">
                        <tr>
                            <td valign="top">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 1%; font-weight: bold;font-size:large;text-align:right;background-color:#333;color:#f0656f;" text>
                                            Amt&nbsp;
                                        </td>
                                        <td style="font-weight: bold;font-size:large;text-align:left;background-color:#333;color:#f0656f;" id="td_deliverycharges">+Delivery charges&nbsp;(<span id="spn_delcharge">0</span>)</td>
                                        <td  style="text-align: left;font-size:large;font-weight:bold;background-color:#333;color:#f0656f;">
                                            <div style="float: left;">Amt:</div>
                                         <div id="dvsbtotal">
                                            </div>   
                                        </td>
                                        <td  style="text-align: left;font-size:large;font-weight:bold;background-color:#333;color:#f0656f;display:none">
                                             <div style="float: left;">NetAmt:</div>
                                         <div id="dvnettotal" style="text-align: left;font-size:large;font-weight:bold;background-color:#333;color:#f0656f;">
                                            </div> 

                                        </td>
                                    </tr>
                                  <%--  <tr>
                                        <td style="width: 60%; font-weight: bold; text-align: right">
                                            Discount:
                                        </td>
                                        <td style="width: 30px; text-align: right;">
                                            <div id="dvdisper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right;">
                                            <div id="dvdiscount">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%; font-weight: bold; text-align: right">
                                            ServiceTax:
                                        </td>
                                        <td style="width: 30px; text-align: right;">
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right">
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%; font-weight: bold; text-align: right">
                                            Vat:
                                        </td>
                                        <td style="width: 100px; text-align: right;">
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%; font-weight: bold; text-align: right">
                                            Net Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right">
                                            <div id="dvnetAmount">
                                            </div>
                                        </td>
                                    </tr>--%>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table class="button_options kot_button_option" style="width: 100%;">
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tbody>
                                        <tr>
                                            <%--<td>
                                                <input type="text" placeholder="Mobile Number" style="border: solid 5px silver; padding: 5px"
                                                    id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all"
                                                    aria-haspopup="true" role="textbox"style:position: absolute; top: 165.5px; left: 634.5px;">
                                            </td>
                                            <td>
                                                <div id="btnCustomer">
                                                    <img src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>
                                            </td>--%>
                                         <%--   <td style="padding-left: 10px">
                                                <asp:DropDownList ID="ddloption" runat="server" Style="width: 100px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>--%>
                                           <%-- <td>
                                             <select id="ddlTable" style="height: 25px; width: 90px" class="form-control">
                                        
                                            </td>--%>

                                        </tr>
               
                                    </tbody>
                                </table>
                            </td>
                        </tr>
				
                        <tr>
						<td style="display:none;">

                  
                              <%--  <div class="button1" id="btnqty" style="width:63px;color:white;font-size:medium;margin-top:-55px;margin-left:22px;margin-right: 8px;background:rgba(0, 0, 0, 0.9);border-color:#db3030;border-width:2px;border-style:width;  ">
                                     qty.</div>--%>
                                     <div> <input id="num"placeholder="QTY" onfocus="this.placeholder = ''"onblur="this.placeholder = 'QTY'" class="alignRight" type="text" style="width:100px;margin-bottom: 12px;font-size:medium;background:#db3030;border-color:White;border-style:solid;border-width:1px;border-radius: 3px;height: 50px;color: #ffffff;font-color:white;Font-weight:bold;text-align:center;" fixed /></div>
                       
                 
                            </td>
							<td style="display:none;">
                                <div class="button1" id="btnUp">
                                     UP</div>
									 </td>
                   
							<td style="display:none;">
                        
                                <div class="button1" id="btnDown" onclick="incrementValue()" value="Increment Value">
                                     Down</div>
                            </td>
						
                      
                            <td id="tdnsaveKot">
                              <div class="button1" id="btnsaveKot">
                                     Save</div>
                            </td>
                            
                                    <td id="tdsave2" style="display:none">
                                <div class="button1" id="btnsave2">
                                    Save</div>

                            </td>
                            <td id="tdraisbill">
                              <div class="button1" id="btnRaiseBill">
                                   Raise Bill</div>
                            </td>
                            <td>
                       <div class="button1" id="btnSettlement">
                                   Settlement</div>
                            </td>
                                              <td>
                                <div class="button1" id="btnTransfer">
                                    Transfer</div>
                            </td>
                          <td>
                                <div class="button1" id="btnCal">
                                    Cal</div>
                            </td>
                          <br />
                             <td>
                                <div class="button1" id="btnExit">
                                    Exit</div>
                            </td>
                          <td>  <div class="MangeBill" style="
    background-color:  green;
"> <a href="BillScreen.aspx" style="float:right;width:109px;font-size: 14px;color:  white;">Retail BILL</a>
                            </div>
                            </td>
                        </tr>
                       
               


                      
                        <tr>
                           
                            <td>
                                <div class="button1" id="btnCash"  style="display:none">
                                    Update KOT</div>
                            </td>
                           
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left; display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            </div>




          <div id="getCalculator"  style="display:none;">
              <div class="calculator" align="center" >
                 <div class="x_panel">
      <div class="row displayBox">
        <p class="displayText" id="display">0</p>
      </div>
      <div class="row numberPad">
        <div class="col-md-9">
          <div class="row">
            <button class="btn clear hvr-back-pulse change-font" id="clear">C</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="sqrt">√</button>
            <button class="btn btn-calc hvr-radial-out hvr-radial-out change-font" id="square">x<sup>2</sup></button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="seven" >7</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="eight">8</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="nine">9</button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="four">4</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="five">5</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="six">6</button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="one">1</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="two">2</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="three">3</button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="plus_minus">&#177;</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="zero">0</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="decimal">.</button>
          </div>
        </div>
        <div class="col-md-3 operationSide">
          <button id="divide" class="btn btn-operation hvr-fade change-font">÷</button>
          <button id="multiply" class="btn btn-operation hvr-fade change-font">×</button>
          <button id="subtract" class="btn btn-operation hvr-fade change-font">−</button>
          <button id="add" class="btn btn-operation hvr-fade change-font">+</button>
          <button id="equals" class="btn btn-operation equals hvr-back-pulse change-font">=</button>
        </div>
      </div>
    </div>
</div>
</div>
            <div class="row" id="CustomerDialog" style="display:none">
<div class="form-horizontal form-label-left input_mask">
<uc3:AddCashCustomer ID="ucAddCashCustomer" runat="server" />
</div>


</div>

         <div class="col-xs-5">
                <div id="products" class="kotscreen_boxes">
            
                </div>
          <%--   <input type="text" id="CopyValue"/>--%>
          
              <div id="numericInput" >
    <div id="numBox" margin-top= "-256px"display="none"></div>
    
    <table id="keypad" >

        <tr>
            <td class="key">1</td>
            <td class="key" width="54px">2</td>
            <td class="key">3</td>
        </tr>
        <tr>
            <td class="key">4</td>
            <td class="key">5</td>
            <td class="key">6</td>
        </tr>
        <tr>
            <td class="key">7</td>
            <td class="key">8</td>
            <td class="key">9</td>
        </tr>
        <tr>
            <td class="btn">DEL</td>
            <td class="key" height="20px">0</td>
            <td class="btn">CLR</td>
        </tr>
    </table>
   
</div>
            <%--<div id="KeypadDialog" style="display:none" title="Payment Receipt">--%>
          
<%--</div>--%>
        </div>

         

    
    </div>
   
    </form>

    <script>

        $('input[type="text"]').click(function () {
            $.ajax({
                type: "POST",
                url: "billscreen.aspx/Keyboard",
                contentType: "application/json",
                dataType: "json",

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {
                }

            });
            $(this).focus();
        });

        $('.txt').click(function () {
            $.ajax({
                type: "POST",
                url: "billscreen.aspx/Keyboard",
                contentType: "application/json",
                dataType: "json",

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {
                }

            });
            $(this).focus();
        });
        $("#btnAddCustomer").click(
  function () {

      $("#CustomerDialog").dialog({
          autoOpen: true,
          width: 800,
          resizable: false,
          modal: true
      });
  });



    </script>
  <script type="text/javascript">
      function AdjustMinues() {
          var liWidthCount = 0;
          var UlWidth = 1200;
          $("#categories").children().each(function (i, obj) {
              liWidthCount = parseInt(liWidthCount) + parseInt($(obj).width())
          })
          if (liWidthCount >= UlWidth) {
              $("#categories").remove('style');
              $("#categories").attr('style', 'width:' + liWidthCount + 'px;table-layout:fixed;float: left;height:50px;margin-left: 0px;overflow-x: hidden !important;')
              $("#MenuScroll").removeAttr('style');
              $("#MenuScroll").attr('style', 'margin-top: -40px;max-width: 1300px;overflow-y:auto;width:100%;')
          }
      }
      function Scrolldown() {
          setTimeout(function () {
              var elem = document.getElementById('dvProductInfo');
              elem.scrollTop = elem.scrollHeight;
          }, 100);
      }
  </script>
  <script type="text/javascript">
      document.getElementById('cm_DOB').valueAsDate = new Date();
      document.getElementById('cm_DOA').valueAsDate = new Date();
  </script>
 
</body>
</html>
