﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class managecustomer : System.Web.UI.Page
{
    mst_customer mstcust = new mst_customer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
            bindgride();
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {if (btnsave.Text == "Submit")
        {
            mstcust.req = "insert";
            mstcust.customer_name = tbName.Value.Trim();
            mstcust.status = ddstatus.SelectedValue;
            mstcust.remark = tbremark.Text.Trim();
            mstcust.discount =tbdiscount.Value.Trim();
            mstcust.userid = HttpContext.Current.Request.Cookies[Constants.AdminId].Value;
            mstcust.insert_update_customer();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Customer Added Successfully!');", true);
 


        }
        else {
            mstcust.cst_id = Convert.ToInt32(ViewState["glbl_cst_id"]);
            mstcust.req = "update";
        
             mstcust.customer_name = tbName.Value.Trim();
             mstcust.status = ddstatus.SelectedValue;
             mstcust.remark = tbremark.Text.Trim();
            mstcust.discount = tbdiscount.Value.Trim();
            mstcust.userid = HttpContext.Current.Request.Cookies[Constants.AdminId].Value;
            btnsave.Text = "Submit";
            mstcust.insert_update_customer();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Customer Updated Successfully!');", true);
  
       
       
        }
        tbName.Value = "";
        tbremark.Text = "";
        tbdiscount.Value = "";
        bindgride();

    }

    protected void bindgride()
    {
        mstcust.req = "bindgrid";

        DataTable dt = mstcust.bindgride();
        gv_display.DataSource = dt;
        gv_display.DataBind();


    }
    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {
        gv_display.PageIndex = e.NewPageIndex;
        gv_display.DataBind();
        bindgride();
    }

    protected void gv_display_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

        int cst_id = Convert.ToInt32(e.CommandArgument);
        ViewState["glbl_cst_id"] = cst_id;
        if (e.CommandName == "select")
        {

            mstcust.req = "getcustomer";
            mstcust.cst_id = cst_id;
            mstcust.get_customer();
            tbName.Value = mstcust.customer_name;
            tbremark.Text = mstcust.remark;
            ddstatus.SelectedValue = mstcust.status;
            tbdiscount.Value = mstcust.discount;

            btnsave.Text = "Update";

        }
        else if (e.CommandName == "del")
        {

            mstcust.req = "deletecustomer";
            mstcust.cst_id = cst_id;
            mstcust.del_customer();
            bindgride();

        }
    }
}