﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="ManageHeaderFooter.aspx.cs" Inherits="ManageHeaderFooter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/customcss/setup.css" rel="stylesheet" />

  <style>
    th {
      text-align: center;
    }
    .table table  tbody  tr  td a ,
.table table  tbody  tr  td  span {
position: relative;
float: left;
padding: 6px 12px;
margin-left: -1px;
line-height: 1.42857143;
color: #337ab7;
text-decoration: none;
background-color: #fff;
border: 1px solid #ddd;
}

.table table > tbody > tr > td > span {
z-index: 3;
color: #fff;
cursor: default;
background-color: #337ab7;
border-color: #337ab7;
}

.table table > tbody > tr > td:first-child > a,
.table table > tbody > tr > td:first-child > span {
margin-left: 0;
border-top-left-radius: 4px;
border-bottom-left-radius: 4px;
}

.table table > tbody > tr > td:last-child > a,
.table table > tbody > tr > td:last-child > span {
border-top-right-radius: 4px;
border-bottom-right-radius: 4px;
}

.table table > tbody > tr > td > a:hover,
.table   table > tbody > tr > td > span:hover,
.table table > tbody > tr > td > a:focus,
.table table > tbody > tr > td > span:focus {
z-index: 2;
color: #23527c;
background-color: #eee;
border-color: #ddd;
}
  </style>
      <form runat="server" id="formID" method="post">
      <div class="right_col" role="main">
          <div class="">

   
            <div class="clearfix"></div>
              <div class="x_title setup_title">
                <h2>Manage Header/Footer</h2>

                <div class="clearfix"></div>
              </div>

            <div class="x_panel">
                  <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
        <asp:UpdateProgress ID="updateProgress" runat="server">
          <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
              <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
            </div>
          </ProgressTemplate>
        </asp:UpdateProgress>
              <div class="x_content">
              
                  <asp:GridView ID="gv_display" runat="server" AutoGenerateColumns="false" CellPadding="6" CssClass= "table table-striped table-bordered table-condensed" OnRowCancelingEdit="gv_display_RowCancelingEdit"   
  
OnRowEditing="gv_display_RowEditing" OnRowUpdating="gv_display_RowUpdating">

 <Columns>
          <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Edit" runat="server" Text="Edit" class="btn btn-primary" CommandName="Edit" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" class="btn btn-success" CommandName="Update"/>  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" class="btn btn-danger" CommandName="Cancel"/>  
                    </EditItemTemplate>  
                </asp:TemplateField> 
     <asp:TemplateField HeaderText="SNO">
         <ItemTemplate>
             <asp:Label ID="lbl_sno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
         </ItemTemplate>
     </asp:TemplateField>
        <asp:TemplateField HeaderText="TYPE">
         <ItemTemplate>
             <asp:Label ID="lbl_Type" runat="server" Text='<%#Eval("Type") %>'></asp:Label>
         </ItemTemplate>
     </asp:TemplateField>

          <asp:TemplateField HeaderText="HEADER TEXT">
         <ItemTemplate>
             <asp:Label ID="lbl_headertxt" runat="server" Text='<%#Eval("HEADERTEXT") %>'></asp:Label>
         </ItemTemplate>
                             <EditItemTemplate>  
                        <asp:TextBox ID="txt_headertxt" runat="server" Text='<%#Eval("HEADERTEXT") %>'></asp:TextBox>  
                    </EditItemTemplate> 
     </asp:TemplateField>


      <asp:BoundField DataField="FONTNAME" HeaderText="FONT NAME" ReadOnly="true"/>
      <asp:BoundField DataField="FONTSIZE" HeaderText="FONT SIZE" ReadOnly="true"/>
      <asp:BoundField DataField="FONTBOLD" HeaderText="FONT BOLD" ReadOnly="true"/>
      <asp:BoundField DataField="FONTUNDERLINE" HeaderText="FONT UNDERLINE" ReadOnly="true"/>
      <asp:BoundField DataField="REPORTCOLS" HeaderText="REPORT COLS" ReadOnly="true"/>
           <asp:BoundField DataField="BranchId" HeaderText="BRANCH ID" ReadOnly="true"/>
 </Columns>
                  </asp:GridView>
              </div>
             </ContentTemplate>
    </asp:UpdatePanel>
            </div>





          </div>


        </div>
          </form>
</asp:Content>

