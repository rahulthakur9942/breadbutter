﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageroles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string Insert(int RoleId, string Title, bool IsActive)
    {

        Roles objRoles = new Roles()
        {
            RoleId = RoleId,
            Title = Title.Trim().ToUpper(),
            IsActive = IsActive

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new RolesBLL().InsertUpdate(objRoles);
        var JsonData = new
        {
            Role = objRoles,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}