﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managecustomer_rate.aspx.cs" Inherits="managecustomer_rate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>


     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


 <form   runat="server" id="formID" method="post">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <script type="text/javascript">
    var xPos, yPos;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    function BeginRequestHandler(sender, args) {
        if ($get('dvProducts') != null) {
            xPos = $get('dvProducts').scrollLeft;
            yPos = $get('dvProducts').scrollTop;
        }
    }

    function EndRequestHandler(sender, args) {
        if ($get('dvProducts') != null) {
            $get('dvProducts').scrollLeft = xPos;
            $get('dvProducts').scrollTop = yPos;
        }
    }

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);
</script>
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="updateProgress" runat="server">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
                                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                                        ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Rates</h3>
                        </div>

                    </div>
                    <div class="clearfix"></div>

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Rates</h2>
                             
                            <div class="clearfix"></div>
                            <asp:DropDownList ID="dd_customername" runat="server" CssClass="form-control" AutoPostBack="true" style="height: 30px;width: 20%;" OnSelectedIndexChanged="dd_customername_SelectedIndexChanged"></asp:DropDownList>
                            <asp:TextBox ID="tbsearch" runat="server" CssClass="form-control" style="height: 30px;width: 20%;"></asp:TextBox>&nbsp;&nbsp;<asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="search" CssClass="btn btn-success"/>
                        </div>
                        <div class="x_content">

                            <asp:GridView ID="gv_display" runat="server" OnPageIndexChanging="OnPaging" AutoGenerateColumns="false"
                      EnableModelValidation="True" AllowPaging="True"
                      PagerSettings-PageButtonCount="10" PagerStyle-HorizontalAlign="Right"   class="table table-striped table-bordered table-hover" PageSize="50">

                                <Columns>
                                        <asp:TemplateField HeaderText="Item Code">
                                         <ItemTemplate>
                                           <asp:Label ID="lblitemcode"  runat="server" Text='<%#Eval("Item_Code") %>'></asp:Label>
                                             </ItemTemplate>
                                            </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="Item_Name" HeaderText="Item_Name"/>
                                     <asp:BoundField DataField="max_retail_price" HeaderText="MRP"/>
                                 
                                     <asp:BoundField DataField="sale_rate" HeaderText="Sale Rate"/>
                                     <asp:TemplateField HeaderText="Customer Rate">
                                         <ItemTemplate>
                                             <asp:TextBox ID="tbrate" runat="server" Text='<%#Eval("rate") %>'></asp:TextBox>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                    <asp:TemplateField>
                                        
                                        <ItemTemplate >

                                            <asp:Button ID="btnsubmite" runat="server" Text="Update" OnClick="SubmiteDetails_Click"  class="btn btn-primary btn-small"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                     <%--          <table class="table">
                                   <tr><th>Item_Code</th>
                                       <th>Item_Name</th>
                                          <th>MRP</th>
                                          <th>Sale Rate</th>
                                          <th>Customer Rate</th>
                                        <th></th>
                                       
                                     <%-- <asp:Button ID="btnsave"  class="btn btn-primary btn-small" runat="server" Text="Submit" OnClick="btnsave_Click" Visible="false" style="float:right"/>--%>

    <%--                               </tr>
                                <tbody  id="dvProducts">
                                    <asp:Repeater ID="rpt_itemlist" runat="server" OnItemCommand="rpt_itemlist_Itemcommand"><ItemTemplate>
                                <tr>
                                    <td> <asp:Label ID="lblitemcode"  runat="server" Text='<%#Eval("Item_Code") %>'></asp:Label></td>
                                      <td><%#Eval("Item_Name") %></td>
                                      <td><%#Eval("max_retail_price") %></td>
                                      <td><%#Eval("sale_rate") %></td>
                                  
                                      <td>
                                          <asp:TextBox ID="tbrate" runat="server" Text='<%#Eval("rate") %>'></asp:TextBox>
                                        </td>
                                    <td style="padding-left: 70px;"> 
                                     
                                        <asp:Button ID="btnsubmite" runat="server" Text="Update" OnClick="SubmiteDetails"  class="btn btn-primary btn-small"/>

                                    </td>

                                </tr>
                                        </ItemTemplate></asp:Repeater>
                                </tbody>
    
                                </table>--%>

  
                       
                        </div>
           
                    </div>


                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
          <%--     <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>--%>
                <!-- /footer content -->

            </div>


</ContentTemplate>
                     </asp:UpdatePanel>
          </form>
</asp:Content>

