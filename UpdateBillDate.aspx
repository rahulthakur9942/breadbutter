﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateBillDate.aspx.cs" Inherits="UpdateBillDate" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
    <div>
    <table>
    <tr>
    <td>Enter Bill No :-</td><td><asp:TextBox ID="txtBillNo" runat="server"></asp:TextBox></td><td>
        <asp:Button ID="btnGetDate" runat="server" Text="Get Bill Date" 
            onclick="btnGetDate_Click" /></td>
    </tr>
     <tr>
    <td></td><td><asp:TextBox ID="txtBillDate" runat="server"></asp:TextBox></td><td><asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtBillDate" EnabledOnClient="true"  runat="server"></asp:CalendarExtender> </td>
    </tr>
    <tr><td></td><td><asp:Button ID="btnUpdateDate" runat="server" Text="Update Date" 
            onclick="btnUpdateDate_Click" /></td></tr>
    </table>
    </div>
    </form>
</body>
</html>
