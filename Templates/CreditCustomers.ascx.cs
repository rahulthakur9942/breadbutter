﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Templates_CreditCustomers : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindDropDownList();
        BindAccsgroup();
    }


    void BindDropDownList()
    {

        DataSet ds = new CommonMasterDAL().GetAll();

        cm_cr_ddlCities.DataSource = ds.Tables[0];
        cm_cr_ddlCities.DataValueField = "CIty_ID";
        cm_cr_ddlCities.DataTextField = "City_Name";
        cm_cr_ddlCities.DataBind();

        cm_cr_ddlArea.DataSource = ds.Tables[1];
        cm_cr_ddlArea.DataTextField = "Area_Name";
        cm_cr_ddlArea.DataValueField = "Area_ID";
        cm_cr_ddlArea.DataBind();


        cm_cr_ddlState.DataSource = ds.Tables[2];
        cm_cr_ddlState.DataTextField = "State_Name";
        cm_cr_ddlState.DataValueField = "STATE_ID";
        cm_cr_ddlState.DataBind();

        cm_cr_ddlPrefix.DataSource = ds.Tables[3];
        cm_cr_ddlPrefix.DataTextField = "PROP_NAME";
        cm_cr_ddlPrefix.DataValueField = "PROP_NAME";
        cm_cr_ddlPrefix.DataBind();


    }

    void BindAccsgroup()
    {
        cm_cr_ddlAccSGroup.DataSource = new AccSGroupBLL().GetAll();
        cm_cr_ddlAccSGroup.DataValueField = "SS_CODE";
        cm_cr_ddlAccSGroup.DataTextField = "SS_NAME";
        cm_cr_ddlAccSGroup.DataBind();
    }
}