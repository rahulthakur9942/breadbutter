﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KOT
/// </summary>
public class KOT
{


    public Int32 KOTNo { get; set; }
    public DateTime KotDate { get; set; }
    public Int32 MKOTNo { get; set; }
    public DateTime Time { get; set; }
    public Int32 TableID { get; set; }
    public Int32 PaxNo { get; set; }
    public Int32 R_Code { get; set; }
    public Int32 M_Code { get; set; }
    public decimal Value { get; set; }
    public decimal DisPercentage { get; set; }
    public decimal DisAmount { get; set; }
    public decimal ServiceCharges { get; set; }
    public decimal TaxAmount { get; set; }
    public decimal TotalAmount { get; set; }
    public bool Active { get; set; }
    public bool Complementary { get; set; }
    public bool Happy { get; set; }
    public Int32 EmpCode { get; set; }
    public DateTime ModifiedDate { get; set; }
    public DateTime Stamp { get; set; }
    public string BillNoWPrefix { get; set; }
    public int KotPrinted { get; set; }
    public bool Pass { get; set; }
    public int BranchId { get; set; }
    public string strKD { get { return KotDate.ToString("d"); } }
    public string strMD { get { return ModifiedDate.ToString("d"); } }
    public string EmpName { get; set; }
    public string KotTime { get; set; }
    public string KotDate2 { get; set; }



	public KOT()
	{
        KOTNo = 0;
        KotDate = DateTime.Now;
        MKOTNo = 0;
        Time = DateTime.Now;
        TableID = 0;
        PaxNo = 0;
        R_Code = 0;
        M_Code = 0;
        Value = 0;
        DisAmount = 0;
        DisPercentage = 0;
        ServiceCharges = 0;
        TaxAmount = 0;
        TotalAmount = 0;
        Active = false;
        Complementary = false;
        Happy = false;
        ModifiedDate = DateTime.Now;
        EmpCode = 0;
        Stamp = DateTime.Now;
        BillNoWPrefix = "";
        KotPrinted = 0;
        Pass = false;
        BranchId = 0;
        EmpName = "";
        KotTime = "";
        KotDate2 = "";

	}
}