﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="SettingExPrining.aspx.cs" Inherits="SettingExPrining" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <style>

        th {
            text-align: center;
        }

    </style>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <form   runat="server" id="formID" method="post">
         <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="updateProgress" runat="server">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
                                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                                        ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Printers</h3>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Printers</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                                                            
                             <tr>
                  <td class="headings">Department Name:</td>
                                 <td> <input type="text" required   name="tbdeptname" class="form-control validate required numeric" runat="server"  data-index="1" id="tbdeptname" style="width: 213px"/></td>

                             </tr>
                             <tr>
                  <td class="headings">Printer Name:</td>
                                 <td> <input type="text" required   name="tbPrinterName" class="form-control validate required numeric" runat="server"  data-index="1" id="tbPrinterName" style="width: 213px"/></td>

                             </tr>

                                              <tr>
                  <td class="headings">Dos:</td>
                                 <td> <input type="text"  name="tbdos" class="form-control validate" runat="server"  data-index="1" id="tbdos" style="width: 213px"/></td>

                             </tr>
                         
                         <tr>
                  <td class="headings">Net User:</td>
                                 <td> <input type="text"   name="tbnetuser" class="form-control validate" runat="server"  data-index="1" id="tbnetuser" style="width: 213px"/></td>

                             </tr>


                                          <tr>
                  <td class="headings">Net Del:</td>
                                 <td> <input type="text"   name="tbNet_Del" class="form-control validate" runat="server"  data-index="1" id="tbNet_Del" style="width: 213px"/></td>

                             </tr>
                                   <tr>
      <td>


      </td>


                                        <td>
                                             <asp:Button ID="btnsave"  class="btn btn-primary btn-small" runat="server" Text="Submit" OnClick="btnsave_Click" />
                                            <asp:Button ID="btncancel"  class="btn btn-primary btn-small" runat="server" Text="Cancel" OnClick="btncancel_Click" />
                                        </td>
                                   </tr>
             

                     </table>  
                       
                        </div>
           
                    </div>


     <div class="x_panel">

                        <div class="x_content">

                               <div class="youhave"  >

             <div>
                        <asp:GridView ID="gv_display" runat="server" OnPageIndexChanging="OnPaging"
                                EnableModelValidation="True" AllowPaging="True"
                                PagerSettings-PageButtonCount="10" PagerStyle-HorizontalAlign="Right" OnRowCommand="gv_display_OnRowCommand" AutoGenerateColumns="false" 
                            class="table table-striped table-bordered table-hover" PageSize="13">

                                <Columns>
                                    <asp:BoundField DataField="dep_name" HeaderText="Dept.Name" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                          <asp:BoundField DataField="Print_Com" HeaderText="Printer Name" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>

                                          <asp:BoundField DataField="Dos" HeaderText="DOS" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>

                                            <asp:BoundField DataField="net_use" HeaderText="Net Use" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>

                                              <asp:BoundField DataField="Net_Del" HeaderText="Net Del" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
               
                                       <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="lnkedit" runat="server" CommandName="select" CommandArgument='<%#Eval("dep_name") %>'><i class="fa fa-edit" style="font-size:20px;color:red"></i></asp:LinkButton>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                    
                                    </Columns>


                        </asp:GridView></div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
          <%--     <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>--%>
                <!-- /footer content -->

            </div>

</ContentTemplate>
                     </asp:UpdatePanel>
         </form>
</asp:Content>

