﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true"
    CodeFile="settlement.aspx.cs" Inherits="settlement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnRoles" runat="server" />
    <asp:HiddenField ID="hdnDate" runat="server" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
    <link href="css/css.css" rel="stylesheet" />    
                <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
  <script src="js/Jquery.numpad.js" type="text/javascript"></script>
    <link href="css/Jquery.numpad.css" rel="stylesheet" />
    <link href="css/customcss/settlement.css" rel="stylesheet" type="text/css" />
         <%--<script>

            
                $(function () {
                    $( 'input[type="text"]').keyboard();

                });

               
            </script>--%>
     
    <script language="javscript" type="text/javascript">


        var cst_id = 0;
        $(document).on('click', 'input[type="text"]', function () {
            $.ajax({
                type: "POST",
                url: "billscreen.aspx/Keyboard",
                contentType: "application/json",
                dataType: "json",

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {
                }

            });
            $(this).focus();
        });
        var CrdCustSelId = 0;
        var CrdCustSelName = "";
        var Bank = 0;
        var BillNowPrefix1 = "";
        function ApplyRoles(Roles) {
            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }


        function Reset() {
            CrdCustSelId = 0;
            CrdCustSelName = "";
            $("#txtCash").val("");
            $("#txtCredit").val("");
            $("#txtCard").val("");
            $("#ddlBank").val("");
            $("#txtCODAmount").val(0);
            $("#txtpaymentAmount").val(0);

            $("#txtKotBillNo").val("");
            $("#txtkotsteward").val("");
            $("#txtKotTable").val("");
            $("#txtKotDate").val("");
            $("#txtCustomer").val("");
            $("#txtNetAmount").val("");
            $("#lblCreditCustomerName").text("");
            $("#ddlChosseCredit").val(0);
            $("#ddlCreditCustomers").val("")
            $("#ddlCreditCustomers").html("");
            $("#txtddlCreditCustomers").val("");
            $("#ddlPayMode").val("");
            $("#txtpaymentAmount").val("");
            $("#Coupans").val("");
            $("#txtCoupan").val("");
            $("#holder").val("");
            $("#lblAmt").text("");
            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("Cash");

            $("#No").hide();
            $("#Amt").hide();
            $("#ddlOtherPayment").hide();
            $("#Td1").hide();
            $("#txtCODAmount").hide();
            $("#rdoCOD").hide();
            $("#rdoOnline").hide();
            $("#COD").hide();
            $("#Online").hide();

            $("#txtpaymentAmount").hide();
            $("#Coupans").hide();
            $("#OP").hide();
            $("#coupan").hide();
            $("#txtCoupan0").hide();
            var onlineoption = "COD";

            if ($("#ddlbillttype").val() == "Cash") {
                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#hdnCreditCustomerId").val(0);
                $("#lblCreditCustomerName").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers").html("");
                $("#txtddlCreditCustomers").val("");

                $("#lblCashHeading").text("Cash Rec:");
                $("#creditCustomer").css("display", "none");

                $("#dvOuter_ddlCreditCustomers").hide();

                $("#ddlChosseCredit").hide();
                $("#ddlChosseCredit").val(0);

                $("#txtCashReceived").val("0").prop("readonly", false);
                $("#Text13").val("0").prop("readonly", true);
                $("#Text14").val("").prop("readonly", true);
                $("#Text15").val("0").prop("readonly", true);
                $("#Text16").val("").prop("readonly", true);
                $("#ddlType").prop("disabled", true);
                $("#ddlBank").prop("disabled", true);


            }

            GetBill();

        }

        //                  function DEVBalanceCalculation() {


        //                      var txtCashReceived = $("#txtCash");
        //                      var txtCredit = $("#txtCredit");
        //                      var txtCreditCard = $("#txtCard");
        //                      var txtFinalBillAmount = $("#txtNetAmount");


        //                      if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
        //                          txtCreditCard.val(0);
        //                          txtCredit.val(0);
        //                          CrdCustSelId = "CASH";
        //                          CrdCustSelName = "CASH";
        //                          Bank = 0;
        //                          
        //                      }
        //                      else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
        //                          txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
        //                        
        //                      }
        //                      else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCredit.val())) >= Number(txtFinalBillAmount.val())) {
        //                          txtCredit.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

        //                      }



        //                     // var balReturn = Number((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) - Number(txtFinalBillAmount.val())));


        //                   


        //                  }





        $(document).ready(function () {
            $("#txtCashReceived").keyup(function () {
                var Cash = $("#txtCashReceived").val();
                var Amount = $("#txtFinalBillAmount").val();
                if (parseFloat(Cash) < parseFloat(Amount)) {
                    $("#Text13").removeAttr('readonly');
                    var RemainingAmount = parseFloat(Amount) - parseFloat(Cash)
                    $("#Text13").val(RemainingAmount);
                    $("#Text14").removeAttr('readonly');
                    $("#ddlBank").removeAttr('disabled');
                    $("#ddlType").removeAttr('disabled');

                }
                else if (parseFloat(Cash) === parseFloat(Amount)) {
                    $("#Text13").attr('readonly', 'readonly');
                    $("#Text14").attr('readonly', 'readonly');
                    $("#ddlBank").attr('disabled', 'disabled');
                    $("#ddlType").attr('disabled', 'disabled');
                    $("#Text14").val(0);
                    $("#Text13").val(0);
                    $("#ddlType").val("Visa")
                }
                else if (parseFloat(Cash) > parseFloat(Amount)) {
                    $("#Text13").attr('readonly', 'readonly');
                    $("#Text14").attr('readonly', 'readonly');
                    $("#ddlBank").attr('disabled', 'disabled');
                    $("#ddlType").attr('disabled', 'disabled');
                    $("#Text14").val(0);
                    $("#ddlType").val("Visa")
                    $("#Text13").val(0);
                }
            })
        });

        function DEVBalanceCalculation() {


            var txtCashReceived = $("#txtCashReceived");
            var txtCreditCard = $("#Text13");
            var txtCheque = $("#Text15");
            var txtFinalBillAmount = $("#txtFinalBillAmount");
            var txtOnlineAmount = $("#txtpaymentAmount");
            var txtCoupanAmount = $("#Coupans");
            var txtCODAmount = $("#txtCODAmount");


            if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(0);
                txtCheque.val(0);
            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCheque.val(0);

            }

            else if ((Number(txtCashReceived.val()) + Number(txtOnlineAmount.val())) >= Number(txtFinalBillAmount.val())) {
                txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCreditCard.val(0);
                txtCheque.val(0);

            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

            }
            else if ((Number(txtOnlineAmount.val()) + Number(txtCoupanAmount.val())) >= Number(txtFinalBillAmount.val())) {
                txtCoupanAmount.val(Number(txtFinalBillAmount.val()) - Number(txtOnlineAmount.val()));
                txtCheque.val(0);

            }


            var balReturn = Number(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtOnlineAmount.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) + Number(txtCoupanAmount.val()) + Number(txtCODAmount.val())));


            $("#txtBalanceReturn").val(balReturn.toFixed(2));


        }







        $(document).ready(
         function () {

             
             $("#lblCustomer").hide();
             $("#No").hide();
             $("#Amt").hide();
             $("#ddlOtherPayment").hide();
             $("#Td1").hide();
             $("#txtCODAmount").hide();
             $("#rdoCOD").hide();
             $("#rdoOnline").hide();
             $("#COD").hide();
             $("#Online").hide();

             $("#txtpaymentAmount").hide();
             $("#Coupans").hide();
             $("#OP").hide();
             $("#coupan").hide();
             $("#txtCoupan0").hide();
             var onlineoption = "COD";

             if ($("#ddlbillttype").val() == "Cash") {
                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();

                 $("#ddlChosseCredit").hide();
                 $("#ddlChosseCredit").val(0);

                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", true);
                 $("#Text16").val("").prop("readonly", true);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlBank").prop("disabled", true);
                 $("#codChk").hide();
                 $("#OnlineChk").hide();

             }




             //             $("#Coupans").change(function () {

             //                 // The easiest way is of course to delete all textboxes before adding new ones
             //                 //$("#holder").html("");

             //                 var count = $("#holder input").size();
             //                 var requested = parseInt($("#Coupans").val(), 10);

             //                 if (requested > count) {
             //                     for (i = count; i < requested; i++) {
             //                         var $ctrl = $('<input/>').attr({ type: 'text', id: 'txtCoupan' + [i], name: 'text', value: '0' });
             //                         $("#holder").append($ctrl);
             //                     }
             //                 }
             //                 else if (requested < count) {
             //                     var x = requested - 1;
             //                     $("#holder input:gt(" + x + ")").remove();
             //                 }
             //             });




             $("#Coupans").change(function () {

                 // The easiest way is of course to delete all textboxes before adding new ones
                 //$("#holder").html("");
                 $("#holder input").val("");
                 $("#holder2 input").val("");
                 if ($("#Coupans").val() > 10) {
                     alert("Coupans more than 10 cannot be accepted...");
                     $("#Coupans").focus();
                     $("#Coupans").val(" ");
                     return;
                 }
                 $("#No").show();
                 $("#Amt").show();
                 $("#holder2").show();
                 $("#holder").show();
                 var count = $("#holder input").size();
                 var requested = parseInt($("#Coupans").val(), 10);

                 if (requested > count) {
                     for (i = count; i < requested; i++) {
                         var $ctrl = $('<input/>').attr({ type: 'text', id: 'txtCoupan' + [i], name: 'text', value: '0' });
                         $("#holder").append($ctrl);
                     }
                 }
                 else if (requested < count) {
                     var x = requested - 1;
                     $("#holder input:gt(" + x + ")").remove();
                 }

                 var count1 = $("#holder2 input").size();
                 var requested = parseInt($("#Coupans").val(), 10);

                 if (requested > count1) {
                     for (i = count1; i < requested; i++) {
                         var $ctrl = $('<input/>').attr({ type: 'text', id: 'txtCoupanAmt' + [i], name: 'text', value: '0' });
                         $("#holder2").append($ctrl);
                     }
                 }
                 else if (requested < count1) {
                     var x = requested - 1;
                     $("#holder2 input:gt(" + x + ")").remove();
                 }
             });



             //             $("#rdoCOD").change(
             //             function () {
             //                 if ($("#rdoCOD").prop('checked') == true) {
             //                     onlineoption = "COD"
             //                     $("#Td1").show();
             //                     $("#txtCODAmount").show();
             //                     $("#txtCODAmount").val("");
             //                     $("#txtPaymentAmount").val("");
             //                     $("#txtPaymentAmount").hide();
             //                     $("#Coupans").val("0").prop("readonly", true);
             //                     $("#No").hide();
             //                     $("#Amt").hide();
             //                     $("#holder").hide();
             //                     $("#holder2").hide();

             //                 }

             //             });



             $("#txtpaymentAmount").keyup(
     function () {
         var regex = /^[0-9\.]*$/;

         onlineoption = "COD"
         $("#Td1").show();

         $("#txtPaymentAmount").val("");
         $("#txtPaymentAmount").hide();
         $("#Coupans").val("0").prop("readonly", true);
         $("#No").hide();
         $("#Amt").hide();
         $("#holder").hide();
         $("#holder2").hide();
         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {
                 $(this).val(0);
             }
         }
         var txtcodAmount = $("#txtCODAmount").val();
         var txtFinalBillAmount = $("#txtFinalBillAmount").val();
         var txtPaymentAmount = $("#txtpaymentAmount").val();
         //var txtCODAmount = $("#txtCODAmount");
         //txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCODAmount.val()));
         if (Number(txtPaymentAmount) > 0) {
             $("#txtCODAmount").val(0);
         }
         DEVBalanceCalculation();

     }
     );






             GetBill();
             $("#RaiseBill").click(
              function () {
                  Reset();
                  window.location = 'kotraisebill.aspx?Id=A&tblno=0&cashcustcode=0&cashcustName=Cash&cashcustmob=0&compnycustid=0';

              }


            );
             $("#Kot").click(
              function () {
                  Reset();
                  window.location = 'manageKotScreen.aspx';

              }


            );

             $("#ddlCreditCustomers").css("margin-top", "20px");
             $("#ddlCreditCustomers").supersearch({
                 Type: "Accounts",
                 Caption: "Please enter Customer Name/Code ",
                 AccountType: "D",
                 Width: 100,
                 DefaultValue: 0,
                 Godown: 0

             });





             //             $("#rdoOnline").change(
             //             function () {
             //                 if ($("#rdoOnline").prop('checked') == true) {
             //                     if($("#txtCODAmount").val() != 0)
             //                     {
             //                         onlineoption = "COD"

             //                     }
             //                     else{
             //                     onlineoption = "Onlinepayment"
             //                     }
             //                     $("#Td1").hide();
             //                     //  $("#txtCODAmount").hide();
             //                     $("#txtPaymentAmount").show();
             //                     //  $("#txtCODAmount").val("");
             //                     $("#txtPaymentAmount").val("");
             //                     $("#Coupans").val("0").prop("readonly", false);
             //                 }

             //             });

             $("#btnClear").click(
              function () {
                  Reset();
                  window.location = "kotraisebill.aspx";

              }


            );


             $("#ddlbillttype").change(
         function () {



             if ($(this).val() == "Credit") {

                 
                 $("#dvOuter_ddlCreditCustomers").show();
                 $("#lblCashHeading").text("Receipt Amt:");
                 $("#ddlbillttype option[value='Credit']").prop("selected", true);
                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", true);
                 $("#Text16").val("").prop("readonly", true);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlOtherPayment").hide();
                 $("#rdoCOD").hide();
                 $("#rdoOnline").hide();
                 $("#Td1").hide();
                 $("#txtCODAmount").hide();
                 $("#COD").hide();
                 $("#Online").hide();

                 $("#txtpaymentAmount").hide();

                 $("#No").hide();
                 $("#Amt").hide();
                 $("#Coupans").hide();
                 $("#OP").hide();
                 $("#coupan").hide();
                 $("#holder input").hide();
                 $("#holder2 input").hide();
                 $("#No").hide();
                 $("#Amt").hide();
                 $("#codChk").hide();
                 $("#OnlineChk").hide();
                 $("#ddlBank").prop("disabled", true);
                 $("#ddlChosseCredit").show();
                 $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                 $("#lblCustomer").show();
                 var FinalAmount = $("#txtFinalBillAmount").val();
                 $("#txtCashReceived").val(parseFloat(FinalAmount).toFixed(2))
                 $("#txtCashReceived").hide();
                 $("#lblCashHeading").hide();
                 $("#Text13").hide();
                 $("#Text14").hide();
                 $("#Text15").hide();
                 $("#Text16").hide();
                 $("#ddlType").hide();

                 $("#CrCardlbl").hide();
                 $("#CClbl").hide();
                 $("#lblbank").hide();
                 $("#ddlBank").hide();
                 $("#ddlType").hide();
                 $("#lbltype").hide();
                 $("#txtBalanceReturn").val(0);
             }
             else if ($(this).val() == "CreditCard") {
                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");
                 $("#Coupans").hide();
                 $("#OP").hide();
                 $("#coupan").hide();
                 $("#No").hide();
                 $("#Amt").hide();
                 $("#COD").hide();
                 $("#Online").hide();
                 $("#rdoOnline").hide();
                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");
                 $("#ddlType").prop("disabled", true);
                 $("#ddlOtherPayment").hide();
                 $("#txtpaymentAmount").hide();
                 $("#rdoCOD").hide();
                 $("#Td1").hide();
                 $("#txtCODAmount").hide();
                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();
                 $("#holder input").hide(); $("#holder2 input").hide();
                 $("#No").hide();
                 $("#Amt").hide();
                 $("#codChk").hide();
                 $("#OnlineChk").hide();
                 //                   $("#ddlOtherPayment").hide();

                 $("#txtCashReceived").val("0").prop("readonly", true);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);
                 $("#Text13").val($("#txtFinalBillAmount").val());
                 $("#txtBalanceReturn").val(" ");
                 $("#lblCustomer").hide();
                 $("#CrCardlbl").show();
                 $("#CClbl").show();
                 $("#lblbank").show();
                 $("#ddlBank").show();
                 $("#ddlType").show();
                 $("#lbltype").show();
                 $("#Text13").show();
                 $("#Text14").show();
                 $("#Text16").show();
                 $("#txtBalanceReturn").val(0);
             }

             else if ($(this).val() == "Cash") {


                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");
                 $("#Coupans").hide();
                 $("#OP").hide();
                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");
                 $("#coupan").hide();
                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();
                 $("#ddlType").prop("disabled", true);
                 $("#ddlOtherPayment").hide();
                 $("#COD").hide();
                 $("#No").hide();
                 $("#Amt").hide();
                 $("#Online").hide();
                 $("#txtpaymentAmount").hide();
                 $("#rdoCOD").hide();
                 $("#Td1").hide();
                 $("#txtCODAmount").hide();
                 //                   $("#ddlOtherPayment").hide();
                 $("#holder input").hide();
                 $("#holder2 input").hide();
                 $("#No").hide();
                 $("#Amt").hide();
                 $("#codChk").hide();
                 $("#OnlineChk").hide();
                 $("#rdoOnline").hide();
                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", true);
                 $("#Text16").val("").prop("readonly", true);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlBank").prop("disabled", true);
                 $("#txtCashReceived").val($("#txtFinalBillAmount").val());
                 $("#txtCashReceived").show();
                 $("#txtBalanceReturn").val(" ");
                 $("#lblCustomer").hide();
                 $("#CrCardlbl").hide();
                 $("#CClbl").hide();
                 $("#lblbank").hide();
                 $("#ddlBank").hide();
                 $("#ddlType").hide();
                 $("#lbltype").hide();
                 $("#lblCashHeading").show();
                 $("#txtBalanceReturn").val(0);
                 $("#Text13").hide(); 
                 $("#Text14").hide();
             }
             else if ($(this).val() == "OnlinePayment") {

                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");
                 $("#coupan").hide();
                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();
                 $("#COD").show();
                 $("#Online").show();
                 $("#No").show();
                 $("#Amt").show();
                 $("#ddlOtherPayment").show();
                 $("#txtpaymentAmount").show();
                 // $("#rdoOnline").show();
                 //$("#rdoCOD").show();
                 if ($("#txtCODAmount").val() != 0) {
                     onlineoption = "COD"


                 }
                 $("#codChk").show();
                 $("#OnlineChk").show();
                 $("#Coupans").val("0").prop("readonly", true);
                 $("#Td1").show();
                 $("#txtCODAmount").show();
                 $("#holder2").hide();
                 $("#holder").hide();
                 $("#No").hide();
                 $("#Amt").hide();

                 //                 if ($("#rdoOnline").prop('checked') == true) {
                 //                     onlineoption = "OnlinePayment"
                 //                 }

                 $("#Coupans").show();
                 $("#coupan").show();
                 $("#OP").show();
                 $("#txtCashReceived").val("0").prop("readonly", true);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlBank").prop("disabled", true);
                 $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                 $("#lblCustomer").hide();
                 $("#CrCardlbl").hide();
                 $("#CClbl").hide();
                 $("#lblbank").hide();
                 $("#ddlBank").hide();
                 $("#ddlType").hide();
                 $("#lbltype").hide();
                 $("#Text13").hide();
                 $("#lblCashHeading").hide();
                 $("#txtCashReceived").hide();
                 $("#Text14").hide();
             }

         }
         );


             $("#Text13").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }


                 DEVBalanceCalculation();

             }
             );
             $("#Text15").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }

                 DEVBalanceCalculation();

             }
             );





             $("#txtCashReceived").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }


         DEVBalanceCalculation();

     }
     );

             $("#txtpaymentAmount").keyup(
     function () {
         var regex = /^[0-9\.]*$/;
         if ($("#txtCODAmount").val() != 0) {
             onlineoption = "COD"

         }
         else {
             onlineoption = "Onlinepayment"
             $("#Td1").hide();
             //  $("#txtCODAmount").hide();
             // $("#txtPaymentAmount").show();
             //  $("#txtCODAmount").val("");
             $("#txtPaymentAmount").val("");
             $("#Coupans").val("0").prop("readonly", false);
         }
         if ($("#txtCODAmount").val() == "") {
             $("#txtCODAmount").val(0);
         }

         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         var txtcodAmount = $("#txtCODAmount").val();
         var txtFinalBillAmount = $("#txtFinalBillAmount").val();
         var txtPaymentAmount = $("#txtpaymentAmount").val();
         //var txtCODAmount = $("#txtCODAmount");
         //txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCODAmount.val()));
         if (Number(txtPaymentAmount) > 0) {
             $("#txtCODAmount").val(0);
         }

         DEVBalanceCalculation();


     }
     );



             $("#txtCredit").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         DEVBalanceCalculation();



     }
     );



             $("#txtCard").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         DEVBalanceCalculation();



     }
     );

             $("#ddlChosseCredit").change(
                 function () {


                     if ($("#ddlChosseCredit").val() != "0") {


                         CrdCustSelId = $("#ddlChosseCredit").val();
                         $("#hdnCreditCustomerId").val(CrdCustSelId);
                         CrdCustSelName = $("#ddlChosseCredit option:selected").text();

                         $("#lblCreditCustomerName").text($("#ddlChosseCredit option:selected").text())

                         $("#creditCustomer").css("display", "block");

                     }




                 });


             function Printt(celValue) {

                 $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                 var iframe = document.getElementById('reportout');
                 iframe = document.createElement("iframe");

                 iframe.setAttribute("id", "reportout");
                 iframe.style.width = 0 + "px";

                 iframe.style.height = 0 + "px";
                 document.body.appendChild(iframe);

                 document.getElementById('reportout').contentWindow.location = "Reports/Retailbillrpt.aspx?BillNowPrefix=" + celValue;


             }


             $("#btnSave").click(
              function () {
                  if ($("#ddlOtherPayment").val() === "0" && $("#ddlbillttype").val() === "OnlinePayment") {
                      alert("Please select online payment mode");
                      return;
                  }
                  var FinalAmt = $("#txtFinalBillAmount").val();
                  var CashRecieve = $("#txtCashReceived").val();
                  var CrCardAmt = $("#Text13").val();
                  var COD = $("#txtCODAmount").val();
                  var OnlinePayment = $("#txtpaymentAmount").val();
                  var AmtTotal = 0;
                  AmtTotal = parseFloat(CashRecieve) + parseFloat(CrCardAmt) + parseFloat(COD) + parseFloat(OnlinePayment);
                  if (AmtTotal != FinalAmt) {

                      alert("Please check your entered amount!");
                      return false;
                  }

                  if (cst_id != 0 && $("#ddlbillttype").val() != 'OnlinePayment') {

                      alert("This sale is not belong to the SHOP.Please check your BillType!");
                      return false;
                  }

                  $("#btnSave").attr('disabled', 'disabled');
                  if ($("#txtKotBillNo").val() == "") {
                      alert("First Choose BillNo for settlement");
                      return;
                  }

                  var billtype = $("#ddlbillttype").val();
                  if (billtype == "") {
                      alert("Select BillType First");
                      $("#ddlbillttype").focus();
                      return;

                  }


                  if (billtype == "Credit") {

                      if (CrdCustSelId == 0) {

                          alert("Please select Credit Customer");
                          $("#txtddlCreditCustomers").focus();
                          $("#btnSave").removeAttr('disabled');
                          return;
                      }


                  }







                  var txtcreditcardcheck = $("#Text13").val();

                  if (txtcreditcardcheck != "0") {
                      if ($("#ddlType").val() == "") {
                          $.uiUnlock();
                          alert("Please Select Credit Card Type");
                          $("#ddlType").focus();
                          $("#btnSave").removeAttr('disabled');
                          return;
                      }
                      //if ($("#Text14").val() == "") {
                      //    $.uiUnlock();
                      //    alert("Please Enter Credit Card No");
                      //    $("#Text14").focus();
                      //    return;
                      //}

                  }

                  var txtchequecheck = $("#Text15").val();
                  if (txtchequecheck != "0") {
                      if ($("#ddlBank").val() == "") {
                          $.uiUnlock();
                          alert("Please Select Bank");
                          $("#ddlBank").focus();
                          $("#btnSave").removeAttr('disabled');
                          return;
                      }
                      if ($("#Text16").val() == "") {
                          $.uiUnlock();
                          alert("Please Enter Cheque No");
                          $("#Text16").focus();
                          $("#btnSave").removeAttr('disabled');
                          return;
                      }
                  }


                  var cashamount = $("#txtCashReceived").val();


                  if (billtype == "Cash" || billtype == "CreditCard" || billtype == "OnlinePayment") {
                      if (Number($("#txtBalanceReturn").val()) < 0) {
                          $.uiUnlock();
                          alert("Total amount is not equal to Bill Amount....Please first tally amount.");
                          $("#btnSave").removeAttr('disabled');
                          return;
                      }
                      else {
                          cashamount = cashamount - Number($("#txtBalanceReturn").val());
                      }

                  }


                  //                  if (Number(cashamount) < 0) {
                  //                      $.uiUnlock();
                  //                      alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
                  //                      $("#btnSave").removeAttr('disabled');
                  //                      return;
                  //                  }


                  if (billtype == "CreditCard") {


                      var CustomerId = $("#ddlBank option:selected").text();
                      var CustomerName = $("#ddlBank option:selected").text();
                      var billmode = billtype;
                      var NetAmt = $("#txtFinalBillAmount").val();

                      var CashAmt = 0
                      var creditAmt = 0;
                      var OnlinePayment = 0;
                      var BIllValue = $("#dvsbtotal").html();


                      var CreditBank = $("#ddlBank").val();
                      var CreditCardAmt = $("#Text13").val();


                      var cashcustcode = 0;
                      var cashcustName = "";

                      cashcustcode = 0;
                      cashcustName = "CASH"


                  }
                  if (billtype == "Cash") {




                      var CustomerId = "CASH";
                      var CustomerName = "CASH";
                      var billmode = billtype;
                      var NetAmt = $("#txtFinalBillAmount").val();

                      //var CashAmt = $("#txtFinalBillAmount").val();
                      var CashAmt = $("#txtCashReceived").val();
                      var creditAmt = 0;
                      var OnlinePayment = 0;
                      var BIllValue = $("#dvsbtotal").html();


                      var CreditBank = $("#ddlBank").val();
                      var CreditCardAmt = $("#Text13").val();


                      var cashcustcode = 0;
                      var cashcustName = "";

                      cashcustcode = 0;
                      cashcustName = "CASH"


                  }


                  if (billtype == "OnlinePayment") {


                      var CustomerId = "CASH";
                      var CustomerName = "CASH";
                      var billmode = billtype;
                      var NetAmt = $("#txtFinalBillAmount").val();

                      var CashAmt = 0;
                      if (onlineoption == "COD") {
                          CashAmt = $("#txtCODAmount").val();
                      }

                      var creditAmt = 0;

                      var OnlinePayment = $("#txtpaymentAmount").val();


                      var CreditBank = "";
                      var CreditCardAmt = 0;


                      var cashcustcode = 0;
                      var cashcustName = "";

                      cashcustcode = 0;
                      cashcustName = "CASH"


                  }

                  else if (billtype == "Credit") {


                      var CustomerId = 0;
                      var CustomerName = "";
                      if (CrdCustSelId == "0") {
                          CustomerId = 0;
                          CustomerName = "";
                      }
                      else {
                          CustomerId = CrdCustSelId;
                          CustomerName = CrdCustSelName;
                      }

                      var NetAmt = $("#txtFinalBillAmount").val();

                      var billmode = billtype;

                      var CreditBank = $("#ddlBank").val();
                      var OnlinePayment = 0;
                      var CashAmt = $("#txtCashReceived").val();

                      var BIllValue = $("#dvsbtotal").html();
                      var creditAmt = 0;
                      if (billmode == "Credit") {

                          creditAmt = Number(NetAmt) - Number(CashAmt);


                      }

                      var CreditCardAmt = $("#Text13").val();


                      var cashcustcode = 0;
                      var cashcustName = "";




                  }





                  $.ajax({
                      type: "POST",
                      data: '{"CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '", "billmode": "' + billmode + '", "CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CreditCard": "' + CreditCardAmt + '","Bank": "' + CreditBank + '","cashcustcode": "' + cashcustcode + '","cashcustName": "' + cashcustName + '","BillNowPrefix": "' + BillNowPrefix1 + '","OnlineAmt": "' + OnlinePayment + '"}',
                      url: "settlement.aspx/savesettlement",
                      contentType: "application/json",
                      dataType: "json",
                      success: function (msg) {

                          var obj = jQuery.parseJSON(msg.d);


                          if (obj.Status == 0) {
                              alert("An Error Occured. Please try again Later");
                              return;

                          }

                          else {
                              $("#txtFinalBillAmount").val(0);
                              $("#txtCashReceived").val(0);
                              $("#Text13").val(0);

                              alert("Bill Settled Successfully");


                          }


                      },
                      error: function (xhr, ajaxOptions, thrownError) {

                          var obj = jQuery.parseJSON(xhr.responseText);
                          alert(obj.Message);
                      },
                      complete: function () {

                          if ($("#ddlbillttype").val() == "OnlinePayment") {
                              InsertOnlineOtherPayment(BillNowPrefix1);
                          }
                          GetBill();
                          Reset();
                          $.uiUnlock();


                          //  window.location = "managekotscreen.aspx";


                      }

                  });

                  //window.location = "Settlement.aspx"

              });

         });






        function GetPluginData(Type) {



            var customerId = $("#ddlCreditCustomers").val();
            CrdCustSelId = customerId;
            $("#hdnCreditCustomerId").val(customerId);


            $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
            CrdCustSelName = $("#ddlCreditCustomers option:selected").text();



            $("#creditCustomer").css("display", "block");

        }



        function GetBill() {
            $.ajax({
                type: "POST",
                data: '{}',
                url: "settlement.aspx/GetBill",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#products").html(obj.productData);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                }
            }
          );
        }


        function InsertOnlineOtherPayment(billno) {


            var Id = 0;
            var OtherPayment_ID = $("#ddlOtherPayment").val();
            //var OtherPayment_ID = OtherPayment_ID;

            var BillNo = billno;
            var Billmode = "";

            var CashAmt = $("#txtCODAmount").val();
            if (CashAmt > 0) {
                Billmode = "COD"
            }
            else {
                Billmode = "OnlinePayment"
            }


            var OnlineAmt = $("#txtpaymentAmount").val();
            //var Coupan= Coupan;
            if (Billmode == "COD") {
                var Coupan = [];
                var CoupanAmt = [];
            }
            else {

                var Coupan = [];
                var CoupanAmt = [];

                for (var i = 0; i < $("#holder input").length; i++) {
                    if ($("#txtCoupan" + i).val() > "0") {
                        Coupan[i] = $("#txtCoupan" + i).val();
                    }
                }
                // var Coupan= $("#holder input").val();

                for (var i = 0; i < $("#holder2 input").length; i++) {
                    if ($("#txtCoupanAmt" + i).val() > "0") {
                        CoupanAmt[i] = $("#txtCoupanAmt" + i).val();
                    }
                }
            }

            $.ajax({

                type: "POST",
                data: '{ "ID": "' + Id + '","OtherPayment_ID": "' + OtherPayment_ID + '","Bill_No": "' + BillNo + '","CoupanNo": "' + Coupan + '","CoupanAmt": "' + CoupanAmt + '","Mode": "' + Billmode + '","CashAmt": "' + CashAmt + '","OnlineAmt": "' + OnlineAmt + '"}',
                url: "settlement.aspx/InsertOnlineOtherPayment",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -12) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    RestControls();
                    $.uiUnlock();
                }

            });

        }



        function GetByBillNowPrefix(BillNowPrefix, cstid) {

            PaymentModeID = cstid;
           // if (PaymentModeID != 0) {
                $.ajax({
                    type: "POST",
                    data: "{'PaymentModeID':" + PaymentModeID + "}",
                    url: "BillScreen.aspx/BindDropOnlinePayemntmode",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d.length != 0) {
                            var Target = $("#ddlOtherPayment");
                            Target.empty();
                            $.each(msg.d, function (i, item) {
                                Target.append("<option value=" + item.OtherPaymentModeID + ">" + item.OtherPaymentName + "</option>")
                            });
                        }
                        $("#txtpaymentAmount").val(0);
                        $("#txtCODAmount").val(0);
                        $("#codChk").prop('checked', false);
                        $("#OnlineChk").prop('checked', false);
                    },
                });
           // }
            BillNowPrefix1 = BillNowPrefix;
            cst_id = cstid;
            if (cst_id != 0) {
               
                $('select#ddlbillttype option').removeAttr("selected");
                 
                $("select#ddlbillttype option").filter(function () {
                    return $(this).val() == "OnlinePayment";
                }).prop('selected', true);
                $('#ddlbillttype').attr("disabled", true);
                OnlinePayment();
            }
            else {
                $('select#ddlbillttype option').removeAttr("selected");
                $("select#ddlbillttype option").filter(function () {
                    return $(this).val() == "Cash";
                }).prop('selected', true);
                $('#ddlbillttype').removeAttr('disabled');
                CashPayment();
            }

            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
                url: "settlement.aspx/GetByBillNowPrefix",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    //$("#txtBalanceReturn").val(" ");
                    $("#txtkotsteward").val(obj.BillNowPrefix.EmpName);
                    $("#txtKotBillNo").val(obj.BillNowPrefix.BillNo);
                    $("#txtKotDate").val(obj.BillNowPrefix.strBD);
                    $("#txtCustomer").val(obj.BillNowPrefix.CustomerName);
                    $("#txtKotTable").val(obj.BillNowPrefix.tableNo);
                    $("#txtNetAmount").val(obj.BillNowPrefix.NetAmount);
                    $("#txtFinalBillAmount").val(obj.BillNowPrefix.NetAmount);
                    $("#txtFinalBillAmount").prop('disabled', 'disabled');
                    if ($("#ddlbillttype").val() == "Cash") {
                        $("#txtCashReceived").val(obj.BillNowPrefix.NetAmount);
                    }
                    if ($("#ddlbillttype").val() == "Credit") {
                        $("#txtBalanceReturn").val(obj.BillNowPrefix.NetAmount);
                    }
                    if ($("#ddlbillttype").val() == "CreditCard") {
                        $("#Text13").val(obj.BillNowPrefix.NetAmount);
                    }
                    if ($("#ddlbillttype").val() == "OnlinePayment") {
                        $("#txtBalanceReturn").val(obj.BillNowPrefix.NetAmount);
                    }
                    $("#lblAmt").text("Net Amount :" + obj.BillNowPrefix.NetAmount);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                }
            }
          );
        }
        function OnlinePayment() {
            var customerId = 0;
            CrdCustSelId = 0;
            CrdCustSelName = "";
         
            $("#hdnCreditCustomerId").val(0);
            $("#lblCreditCustomerName").text("");
            CrdCustSelName = "";
            $("#ddlCreditCustomers").html("");
            $("#txtddlCreditCustomers").val("");

            $("#lblCashHeading").text("Cash Rec:");
            $("#creditCustomer").css("display", "none");
            $("#coupan").hide();
            $("#dvOuter_ddlCreditCustomers").hide();
            $("#ddlChosseCredit").hide();
            $("#COD").show();
            $("#Online").show();
            $("#No").show();
            $("#Amt").show();
            $("#ddlOtherPayment").show();
            $("#txtpaymentAmount").show();
            // $("#rdoOnline").show();
            //$("#rdoCOD").show();
            if ($("#txtCODAmount").val() != 0) {
                onlineoption = "COD"


            }
            $("#codChk").show();
            $("#OnlineChk").show();
            $("#Td1").show();
            $("#txtCODAmount").show();
            $("#holder2").hide();
            $("#holder").hide();
            $("#No").hide();
            $("#Amt").hide();

            //                 if ($("#rdoOnline").prop('checked') == true) {
            //                     onlineoption = "OnlinePayment"
            //                 }

            $("#Coupans").show();
            $("#coupan").show();
            $("#OP").show();
            $("#txtCashReceived").val("0").prop("readonly", true);
            $("#Text13").val("0").prop("readonly", true);
            $("#Text14").val("").prop("readonly", true);
            $("#Text15").val("0").prop("readonly", false);
            $("#Text16").val("").prop("readonly", false);
            $("#ddlType").prop("disabled", true);
            $("#ddlBank").prop("disabled", true);
            $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
            $("#lblCustomer").hide();
            $("#CrCardlbl").hide();
            $("#CClbl").hide();
            $("#lblbank").hide();
            $("#ddlBank").hide();
            $("#ddlType").hide();
            $("#lbltype").hide();
            $("#Text13").hide();
            $("#lblCashHeading").hide();
            $("#txtCashReceived").hide();
            $("#Text14").hide();

        }
        function CashPayment() {

            var customerId = 0;
            CrdCustSelId = 0;
            CrdCustSelName = "";
            $("#hdnCreditCustomerId").val(0);
            $("#lblCreditCustomerName").text("");
            CrdCustSelName = "";
            $("#ddlCreditCustomers").html("");
            $("#txtddlCreditCustomers").val("");
            $("#Coupans").hide();
            $("#OP").hide();
            $("#lblCashHeading").text("Cash Rec:");
            $("#lblCashHeading").show(); 
            $("#txtCashReceived").show();
            $("#creditCustomer").css("display", "none");
            $("#coupan").hide();
            $("#dvOuter_ddlCreditCustomers").hide();
            $("#ddlChosseCredit").hide();
            $("#ddlType").prop("disabled", true);
            $("#ddlOtherPayment").hide();
            $("#COD").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#Online").hide();
            $("#txtpaymentAmount").hide();
            $("#rdoCOD").hide();
            $("#Td1").hide();
            $("#txtCODAmount").hide();
            //                   $("#ddlOtherPayment").hide();
            $("#holder input").hide();
            $("#holder2 input").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#codChk").hide();
            $("#OnlineChk").hide();
            $("#rdoOnline").hide();
            $("#txtCashReceived").val("0").prop("readonly", false);
            $("#Text13").val("0").prop("readonly", true);
            $("#Text14").val("").prop("readonly", true);
            $("#Text15").val("0").prop("readonly", true);
            $("#Text16").val("").prop("readonly", true);
            $("#ddlType").prop("disabled", true);
            $("#ddlBank").prop("disabled", true);
            $("#txtCashReceived").val($("#txtFinalBillAmount").val());
            $("#txtBalanceReturn").val(0);
            $("#lblCustomer").hide();
            $("#CrCardlbl").hide();
            $("#CClbl").hide();
            $("#lblbank").hide();
            $("#ddlBank").hide();
            $("#ddlType").hide();
            $("#lbltype").hide();
            $("#Text13").hide();
            $("#Text14").hide();
        }
        $.ajax({
            type: "POST",
            data: '{}',
            url: "screen.aspx/BindBanks",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                $("#ddlBank").html(obj.BankOptions);
                $("#ddlBank option[value='" + DefaultBank + "']").prop("selected", true);
            }
        });
</script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtCODAmount").keyup(
                function () {
                    var regex = /^[0-9\.]*$/;

                    onlineoption = "COD"
                    $("#Td1").show();

                    $("#txtPaymentAmount").val("");
                    $("#txtPaymentAmount").hide();
                    $("#Coupans").val("0").prop("readonly", true);
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#holder").hide();
                    $("#holder2").hide();
                    var value = jQuery.trim($(this).val());
                    var count = value.split('.');
                    if (value.length >= 1) {
                        if (!regex.test(value) || value <= 0 || count.length > 2) {
                            $(this).val(0);
                        }
                    }
                    var txtcodAmount = $("#txtCODAmount").val();
                    var txtFinalBillAmount = $("#txtFinalBillAmount").val();
                    var txtPaymentAmount = $("#txtpaymentAmount").val();
                    if (Number(txtcodAmount) > 0) {
                        $("#txtpaymentAmount").val(0);
                    }
                    DEVBalanceCalculation();
                }
                )
            })
        </script>
       <script type="text/javascript">
           $(document).ready(function () {
               $("#OnlineChk").change(function () {
                   if ($("#OnlineChk").prop('checked')) {
                       var FinalAmount = $("#txtFinalBillAmount").val();
                       $("#txtpaymentAmount").attr('readonly', 'readonly');
                       $("#txtCODAmount").attr('readonly', 'readonly');
                       $("#txtBalanceReturn").val(0)
                       $("#txtpaymentAmount").val(FinalAmount);
                       $("#txtCODAmount").val(0);
                       $("#codChk").prop('checked', false);
                   }
               })
               $("#codChk").change(function () {
                   if ($("#codChk").prop('checked')) {
                       var FinalAmount = $("#txtFinalBillAmount").val();
                       $("#txtpaymentAmount").attr('readonly', 'readonly');
                       $("#txtCODAmount").attr('readonly', 'readonly');
                       $("#txtpaymentAmount").val(0);
                       $("#txtBalanceReturn").val(0)
                       $("#txtCODAmount").val(FinalAmount);
                       $("#OnlineChk").prop('checked', false);
                   }
               })
           })
    </script>
<%--<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script>
        $(function () {


            $("#txtddlCreditCustomers").keyboard();

            $('#txtCash,#txtCredit,#txtCard').keyboard({
                layout: 'custom',
                customLayout: {
                    'default': [
                '1 2 3 4 5',
                '6 7 8 9 0',
                ' {bksp}',
                '{a} {c}'
               ]
                },
                maxLength: 10,
                restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                useCombos: false // don't want A+E to become a ligature
            }).addTyping();




            //                            $('#txtCredit').keyboard({
            //                                layout: 'custom',
            //                                customLayout: {
            //                                    'default': [
            //                '9 8 7 6 5',
            //                '4 3 2 1 0',
            //                ' . {bksp}',
            //                '{a} {c}'
            //               ]
            //                                },
            //                                maxLength: 6,
            //                                restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            //                                useCombos: false // don't want A+E to become a ligature
            //                            }).addTyping();


        });
	</script>--%>



     <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix">
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel settlement_panel">
                        <div class="x_title">
                        <h1 class="settlement_header">Settlement</h1>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <input type="hidden" id="hdnnetamt" value="0" />
                                <input type="hidden" id="hdnCreditCustomerId" value="0" />
                                <div class="container">                                  
                                    <div class="row">
                                        <div class="col-xs-12">
                                    
                                                <div class="Search">
                                                 
                                                        <table width="100%">
                                                            <tr>
                                                                <td style="color: White; font-weight: bold">
                                                                    BillNo
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtKotBillNo" disabled="disabled" style="width:157px;font-size:17px;color:Black" />
                                                                </td>
                                                                <td style="color: White; font-weight: bold">
                                                                    BillDate
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtKotDate" disabled="disabled" style="width:157px;font-size:17px;color:Black" />
                                                                </td>                                                                
                                                            </tr>
                                                            <tr>                                                                                                                               
                                                                <td style="color: White; font-weight: bold;padding-top:5px">
                                                                    Steward
                                                                </td>
                                                                <td style="padding-top:5px;">
                                                                    <input type="text" id="txtkotsteward" disabled="disabled" style="width:157px;font-size:17px;color:Black" />
                                                                </td>
                                                                <td style="color: White; font-weight: bold;padding-top:5px">
                                                                    Customer
                                                                </td>
                                                                <td style="padding-top:5px;">
                                                                     <input type="text" id="txtCustomer" disabled="disabled" style="width:157px;font-size:17px;color:Black"  />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color: White; font-weight: bold;padding-top:5px">
                                                                    TableNo
                                                                </td>
                                                                <td style="padding-top:5px;">
                                                                    <input type="text" id="txtKotTable" disabled="disabled" style="width:157px;font-size:17px;color:Black" />
                                                                </td>
                                                               <td style="color: White; font-weight: bold;padding-top:5px">
                                                                    Net Amt
                                                                </td>
                                                                <td style="padding-top:5px;">
                                                                    <input type="text" id="txtNetAmount" disabled="disabled" style="width:157px;font-size:17px;color:Black"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                  
                                                </div>
                                      
                                            <%--<div class ="col-xs-3" style="background-color:Black;height:87px;margin-top:4px">                
                </div>--%>
                                        </div>
                                        <div class="col-xs-12" style="display: none; margin-bottom: 5px" id="colProducts">
                                        </div>
                                        <div class="col-xs-12 settlement_product">
                                            <div class="col-xs-5">
                                                <div id="products">
                                                
                                                </div>

                                                  

                                            </div>


                                            <div class="col-xs-7" id="colDvProductList">
                                                <div id="dvProductList" style="display: block;">
                                                    <div class="leftside">



                                                    <div class="col-xs-12" style="padding-left: 0px" id="Div1">
                <div id="Div2" style="display: none; margin-top: -10px;">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tbody><tr>
                               <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                    Code
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>
                                  <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    
                                </th>
                                <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    Qty
                                </th>
                                   <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    
                                </th>
                                <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                 <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Amount
                                </th>
                                
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                              
                               
                            </tr>
                        </tbody></table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo">
                            <tbody>
                        <tr><td style="width:30px;text-align:center;font-size:13px">101</td><td style="width:200px;text-align:center;font-size:13px">ALOO PARANTHA</td><td style="width:25px;text-align:center"><div id="btnMinus" class="btn btn-primary btn-small" style="height:30px;width:25px;text-align: center">-</div></td><td style="width:25px;text-align:center"><input name="txtBillQty" style="width:38px;height:30px;font-size:13px;text-align:center" value="1" id="txtBillQty1" type="txtBillQty"></td><td style="width:25px;text-align:center"><div id="btnPlus" class="btn btn-primary btn-small" style="height:30px;width:25px;text-align: center">+</div></td><td style="width:70px;text-align:center;font-size:17px"><input name="txtBillPrice" style="width:70px;text-align:center" disabled="disabled" value="50" id="txtBillPrice1" type="txtBillPrice"></td><td style="width:70px;text-align:center;font-size:17px"><input name="txtBillAmount" style="width:70px;text-align:center" value="50.00" id="txtBillAmount1" type="txtBillAmount"></td><td style="width:150px;text-align:center;display:none"><input style="width:70px;padding:0px" data-toggle="tooltip" title="" id="txtRemarks1" onkeyup="javascript:AddRemarks(1);" value="" type="text"></td><td style="display:none"><img src="images/addon.png" style="cursor:pointer;height:23px" onclick="javascript:BindAddOn(1)"></td><td style="width:50px;text-align:center"><i id="dvClose" style="cursor:pointer"><img src="images/trash.png"></i></td></tr></tbody></table>
                    </div>
                </div>
                <div id="CustomerSearchWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        CUSTOMER SEARCH
                    </div>
                    <div class="cate">
                        <div id="dvSearch">
                            <table>
                                <tbody><tr>
                                    <td>
                                        <table width="100%" cellpadding="5" cellspacing="0">
                                            <tbody><tr>
                                                <td>
                                                    <input id="rbStartingWith" value="S" name="searchcriteria" type="radio">
                                                    <label for="rbStartingWith" style="font-weight: normal">
                                                        Start With</label>
                                                </td>
                                                <td>
                                                    <input value="C" id="rbContaining" checked="checked" name="searchcriteria" type="radio">
                                                    <label for="rbContaining" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input id="rbEndingWith" value="E" name="searchcriteria" type="radio">
                                                    <label for="rbEndingWith" style="font-weight: normal">
                                                        End With</label>
                                                </td>
                                                <td>
                                                    <input id="rbExact" name="searchcriteria" value="EX" type="radio">
                                                    <label for="rbExact" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tbody><tr>
                                                <td>
                                                    <input class="form-control input-small" placeholder="Enter Customer Name" id="txtSearch1" type="text">
                                                </td>
                                                <td>
                                                    <div id="btnSearch1" class="btn btn-primary btn-small">
                                                        Search</div>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                        Close</div>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </div>
                    </div>
                </div>
                <div id="dvBillWindow" class="settlement_window">
               <%--     <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>--%>
                    
                    
               
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px">
                            <tbody>
                                 <tr>
                                    <td colspan="8">
                                    <select id="ddlbillttype" class="form-control" multiple="multiple">
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>
                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>
                                    </select>
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                    Amount:
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tbody><tr>

                               <td>
                                    <input  class="form-control input-small" id="txtFinalBillAmount" type="text">
                                </td>
                               
                                <td>
                                    <span id="lblCustomer">Customer Name</span>

                                 <%-- <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
    height:35px;
    display:none;
">
                                             <option value="0"></option>
                                             </select>--%>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server">
                                           
                                             </asp:DropDownList></td>
                                        </tr>
                                      
                                    </tbody></table>

                                </td>
                               
                                
                            </tr>
                             
                          <tr>
                                <td>
                                    <label id="lblCashHeading">
                                        Cash Rec:</label>
                                </td>
                              
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>


                                        <input  class="form-control input-small" value="0" id="txtCashReceived" type="text">
                                  
                                            </td>
                                            
                                            <td>

                                                  <table width="100%" id="creditCustomer" class="select_customer" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td id="CrCardlbl">
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tbody><tr>
                                                <td>
                                    <input  class="form-control input-small" value="0" id="Text13" type="text">
                                </td>
                                <td id="lbltype">
                                    Type:
                                </td>
                                <td>
                                    <select  id="ddlType"class="form-control">
                                      <option value="" selected="selected"></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>

                               
                                        </tr>

                                    </tbody></table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td id="CClbl">
                                    Cc No:
                                </td>
                                <td colspan="100%">
                                    <input  class="form-control input-small" id="Text14" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td id="lblbank">
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank" class="form-control"><option></option><option value="1">HSBC</option><option value="2">PAYTM</option></select>

                                    <input class="form-control input-small" id="Text15" value="0" type="text">
                                </td>

                            </tr>
                            
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input class="form-control input-small" id="Text16" type="text">
                                </td>
                            </tr>

                            <tr><td style="display: table-cell;" id="OP">Online payment</td>

                             <td> <select id="ddlOtherPayment" class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select></td>
                                        
                                        
                                             </tr>

                                         <tr><td  style="text-align:left;width:50px"><input type="radio"   id="rdoCOD" name="abc" />
                         &nbsp <input type="checkbox" id="codChk" style="width:15px;height:19px" />  <label class="headings" id="COD" for="rdoCOD">COD</label></td>
                            <%--     <td id="Td1">Rs.</td>--%>

                             <td colspan="3">
                                    <table>
                                        <tbody><tr>
                                                <td>
                                   <input class="form-control input-small" id="txtCODAmount" type="text" value="0"/>&nbsp
                                </td>
                                <td>
                                    <input type="checkbox" id="OnlineChk" style="width:15px;height:19px"/>  <label class="headings" id="Online" for="rdoOnline">Online</label>
                                </td>
                                <td>
                                  <input type="text" class="form-control input-small" value="0" id="txtpaymentAmount" />
                                </td>

                               
                                        </tr>

                                    </tbody></table>


                                </td>








                               
                 
 </tr>







                                             <tr>       <td style="display: table-cell;" id="coupan">Enter Coupan Qty</td>

                               <td>
                                    <input  class="form-control input-small" value="0" id="Coupans" type="text">
                                        </td>
                                <%--      <td style="width: 50px"><label id="No" style="display: none; margin-left: -150px;">CoupanNo</label>
                                      <div id="Div3" style="display: none; margin-left: -150px;"></div></td>--%>
                                       <td style="width: 50px"><label id="No" style="margin-left: -85px">CoupanNo</label>
                                      <div style="margin-left: -125px;color:Black" id="holder"></div></td>
                                      <td style="width: 50px"><label  id="Amt">CoupanAmt</label>
                                      <div style="margin-left:-26px;color:Black" id="holder2"></div></td>
                                       
                                        </tr>
                            <tr>
                                <td">
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tbody>
                                        <tr>
                                            
                                            <td>Balance:</td>
                                            <td style="width: 90px">
                                                <input class="form-control input-small" id="txtBalanceReturn"  type="text" value="0" readonly="readonly">
                                            </td>
                                           
                                           
                                        </tr>
                                       
                                    </tbody></table>
                                </td>
                            </tr>

                             <tr> <td style="width: 90px">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tbody>
                                                       
                                                    

                                                    
                                                  
                                                </tbody></table>
                                            </td></tr>
                        </tbody></table>
                    
                </div>
                <div id="dvCreditCustomerSearch" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Credit Customers Sea  
                        
                        /jmv 8lisbnjirch
                    </div>
                    <div class="cate">
                        <table width="100%">
                            <tbody><tr>
                                <td>
                                    <div id="dvLeft1" style="border: 1px solid silver">
                                        <table style="background: silver;" width="100%" cellpadding="5" cellspacing="0">
                                            <tbody><tr>
                                                <td style="padding: 5px">
                                                    <input id="rbPhoneNo1" value="M" checked="checked" name="searchon1" type="radio">
                                                    <label for="rbPhoneNo1" style="font-weight: normal">
                                                        Phone No.</label>
                                                </td>
                                                <td>
                                                    <input id="rbCustomerName1" value="N" name="searchon1" type="radio">
                                                    <label for="rbCustomerName1" style="font-weight: normal">
                                                        Customer Name</label>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                    <div id="dvRight1" style="float: left; width: 100%; border: 1px solid silver; display: none">
                                        <table width="100%" cellpadding="5" cellspacing="0">
                                            <tbody><tr style="background-color: #E6E6E6">
                                                <td colspan="100%">
                                                    Search Criteria:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="rbStartingWith1" value="S" name="searchcriteria1" type="radio">
                                                    <label for="rbStartingWith1" style="font-weight: normal;">
                                                        Starting With</label>
                                                </td>
                                                <td>
                                                    <input value="C" id="rbContaining1" checked="checked" name="searchcriteria1" type="radio">
                                                    <label for="rbContaining1" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input id="rbEndingWith1" value="E" name="searchcriteria1" type="radio">
                                                    <label for="rbEndingWith1" style="font-weight: normal">
                                                        Ending With</label>
                                                </td>
                                                <td>
                                                    <input id="rbExact1" name="searchcriteria1" value="EX" type="radio">
                                                    <label for="rbExact1" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tbody><tr>
                                            <td>
                                                <input class="form-control input-small" placeholder="Enter Search Keyword" id="Txtsrchcredit" type="text">
                                            </td>
                                            <td>
                                                <div id="btncreditsrch" class="btn btn-primary btn-small">
                                                    Search</div>
                                            </td>
                                            <td style="padding-left: 5px">
                                                <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Close</div>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="jQGridDemoCredit">
                                    </table>
                                    <div id="jQGridDemoPagerCredit">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                             <td colspan="100%">
                             <table>
                             <tbody><tr><td>  <div id="btnOk" class="btn btn-primary btn-small">
                                                    Ok</div></td>

                                                     <td style="padding-left: 5px">
                                                <div id="btnCancel" onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Cancel</div>
                                            </td>
                             </tr></tbody></table>
                                              
                                           
                                           
                            </td></tr>
                        </tbody></table>
                    </div>
                </div>
                <div id="dvHoldList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tbody><tr>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Hold No
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 120px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                            </tr>
                        </tbody></table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 17px" id="tbHoldList">
                        </table>
                    </div>
                </div>

                 <div id="dvOrderList" style="display: none">
                    <div class="leftside" >
                        <table style="width: 100%">
                            <tbody><tr>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Order No
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Table
                                </th>
                                <th style="width:130px; text-align: center; color: #FFFFFF;">
                                </th>
                                 
                            </tr>
                        </tbody></table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 10px" id="tbOrderList">
                        </table>
                    </div>
                </div>



                <div class="cate2" style="padding:0px;margin-top:0px;width:572px; display:none; background: #F5FFFA" >
                    <table id="tbamountinfo" style="width: 100%; text-align: right; background: #F5FFFA;padding:0px;font-size:17px;margin-left:0px">
                        <tbody><tr>
                            <td style="padding-top:10px" valign="top">
							

                                   
                                <div>
                               
                                     </div></td></tr><tr>
                                  
                                     
                                     <td>No Of Items: </td><td colspan="100%">
                            
                              <div style="text-align:justify;width:60%;margin:0px 0px 0px;border-left:0px;border-top:0px;padding:5px">
                                  <span id="lblNoItems">1</span>
                                  </div>
                            
                                           </td>
                        </tr>

                              

                                

                            

                            
                            
                                           
                                </tbody></table>
                            </div>


                </div>

                                                        <table style="font-size: 17px">
                                                        <tr><td colspan="4"><label id="lblAmt" style="font-size:larger;font-weight:bold;font-style:normal;text-align:center;color:Red"></label></td></tr>
                                                                                                                     
                                                      
                                                            <%--<tr style="display:none;">
                                                      
                                                                <td style="padding-left: 0px;padding-top:5px; height: 25px; text-align: left;display:none;">
                                                                     <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
">
                                             <option value="0"></option>
                                             </select>
                                                                </td><td style="padding-left:5px;padding-top:5px">
                                                                <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server"  style=" display: inline-block; font-size: 11px;
    font-weight: normal;color:Black;
    height: 45px;
    padding-left: 0;
    width: 132px;
" class="form-control">
                                           
                                             </asp:DropDownList>
                                                                
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="padding-top:5px" colspan="3"> 

                                                                <table width="100%" id="creditCustomer" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            
                                        </tr>
                        --%>            </table>

                                                            </td>
                                                            </tr>
                                                          
                                                             <tr>
                                                            
                                                                  <%--<td style="padding-left: 10px; display:none;padding-top:5px; height: 25px; text-align: left;display:none;">
                                                                    PAYMENT MODE
                                                                </td>--%>
                                                                <td id="Td1" style="padding-left: 0px;padding-top:5px; height: 25px; text-align: left" colspan="3" runat="server"> 
                                                                    <asp:DropDownList id="ddlPayMode" ClientIDMode="Static" style="height: 35px;display:none; color:Black;width: 160px;" class="form-control" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>

                                                    
                                                            <tr>
                                                           <td style="text-align:left;width:50px">






                                                            </tr>
                                                        

                                                        </table>
                                                        

                                                    </div>
                                                    <div>
                                                        <table>
                                                           
                                                        </table>
                                                    </div>
                                                </div>                                                
                                            </div>

                                                        <table class="settlement_buttons">
                                                            <tr>
                                                                <td>
                                                                    <div class="button1" id="RaiseBill">
                                                                        RaiseBill</div>
                                                                </td>
                                                                  <td>
                                                                    <div class="button1" id="Kot">
                                                                        KOT</div>
                                                                </td>
                                                               
                                                       
                                                                <td>
                                                                    <div class="button1" id="btnSave">
                                                                        Save</div>
                                                                </td>
                                                                <td>
                                                                    <div class="button3" id="btnClear">
                                                                        Exit</div>
                                                                </td>
                                                           
                                                                
                                                            </tr>
                                                        </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>    
    </form>
</asp:Content>
