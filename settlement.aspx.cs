﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class settlement : System.Web.UI.Page
{
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    protected void Page_Load(object sender, EventArgs e)
    {
   
        if (!IsPostBack)
        {
            dayopencloseprm();
            hdnDate.Value = DateTime.Now.ToShortDateString();
           
            //string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);
            Session["RetValue"] = -21;
            //if (strDate == "")
            //{
            //    Response.Redirect("index.aspx?DayOpen=Close");
            //}

           
        }
        CheckRole();
        BindCreditCustomers();
        BindPaymentMode();
        BindOtherPayment();
    }

    public void dayopencloseprm()
    {

        string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);
        string Pendingsettelment = new DayOpenCloseDAL().ChkPendingSettlement(Branch);

        if (strDate == "" && Pendingsettelment == "")
        {

            Response.Redirect("index.aspx?DayOpen=Close");
        }

    }
    void BindPaymentMode()
    {

        ddlPayMode.DataSource = new OtherPaymentModeBLL().GetAll();
        ddlPayMode.DataValueField = "OtherPayment_ID";
        ddlPayMode.DataTextField = "OtherPayment_Name";
        ddlPayMode.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose PaymentMode--";
        li1.Value = "0";
        ddlPayMode.Items.Insert(0, li1);

    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.HOLD).ToString() | m == Convert.ToInt16(Enums.Roles.UNHOLD).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.RATEEDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }
    }


    void BindOtherPayment()
    {

        ddlOtherPayment.DataSource = new OtherPaymentModeBLL().GetAll();
        ddlOtherPayment.DataValueField = "OtherPayment_ID";
        ddlOtherPayment.DataTextField = "OtherPayment_Name";
        ddlOtherPayment.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Other--";
        li1.Value = "0";
        ddlOtherPayment.Items.Insert(0, li1);

    }

    void BindCreditCustomers()
    {

        ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit.DataValueField = "CCODE";
        ddlChosseCredit.DataTextField = "CNAME";
        ddlChosseCredit.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Customer--";
        li1.Value = "0";
        ddlChosseCredit.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string GetBill()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string prodData = new kotBLL().GetBill(Branch);
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetByBillNowPrefix(string BillNowPrefix)
    {
        Settlement objSettlement = new Settlement();
        objSettlement.BillNowPrefix = BillNowPrefix;
        new kotBLL().GetByBillNowPrefix(objSettlement);
        var JsonData = new
        {
            BillNowPrefix = objSettlement
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string InsertOnlineOtherPayment(int ID, int OtherPayment_ID, string Bill_No, string CoupanNo, string CoupanAmt, string Mode, decimal CashAmt, decimal OnlineAmt)
    {
        string[] ItemCode = CoupanNo.Split(',');
        string[] Amount = CoupanAmt.Split(',');
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        if (ID == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Bill objOnlineOtherPayment = new Bill();
        if (CoupanNo != string.Empty)
        {


            for (int i = 0; i < ItemCode.Length; i++)
            {
                objOnlineOtherPayment = new Bill()
                {
                    ID = ID,
                    OtherPayment_ID = Convert.ToInt32(OtherPayment_ID),

                    BillNowPrefix = Bill_No,
                    CoupanNo = Convert.ToString(ItemCode[i]),
                    CouponAmt = Convert.ToDecimal(Amount[i]),
                    Mode = Mode,
                    CashAmt = CashAmt,
                    OnlineAmt = OnlineAmt,

                };
                status = new BillBLL().InsertOnlineOtherPayment(objOnlineOtherPayment);
            }
        }
        else
        {
            objOnlineOtherPayment = new Bill()
            {
                ID = ID,
                OtherPayment_ID = Convert.ToInt32(OtherPayment_ID),

                BillNowPrefix = Bill_No,
                CoupanNo = "",
                CouponAmt = 0,
                Mode = Mode,
                CashAmt = CashAmt,
                OnlineAmt = OnlineAmt,

            };
            status = new BillBLL().InsertOnlineOtherPayment(objOnlineOtherPayment);
        }




        var JsonData = new
        {
            bill = objOnlineOtherPayment,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string savesettlement(string CustomerId, string CustomerName, string billmode, decimal CashAmt, decimal CreditAmt, decimal CreditCard, string Bank, Int32 cashcustcode, string cashcustName, string BillNowPrefix, decimal OnlineAmt)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,
            BillMode = billmode,
            Cash_Amount = CashAmt,
            Credit_Amount = CreditAmt,
            CreditBank = Bank,
            CrCard_Amount = CreditCard,
            Customer_ID = CustomerId,
            Customer_Name = CustomerName,
            OnlinePayment = OnlineAmt,
            CashCust_Code = cashcustcode,
            CashCust_Name = cashcustName

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new kotBLL().BillSettlement(objBill);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);
    }

}