﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for UserWiseSaleReport
/// </summary>
public class UserWiseSaleReport : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
	private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private PageHeaderBand PageHeader;
    private GroupHeaderBand GroupHeader1;
    private XRLabel xrLabel2;
    private XRLabel xrLabel15;
    private XRLabel xrLabel25;
    private XRLabel xrLabel14;
    private XRLabel xrLabel3;
    private XRLabel xrLabel11;
    private XRLine xrLine5;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell3;
    private XRTableCell xrTableCell4;
    private XRLine xrLine1;
    private XRTable xrTable1;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell6;
    private XRTableCell clOrderID;
    private XRTableCell clCustomerID;
    private XRTableCell clSalesperson;
    private XRTableCell clOrderDate;
    private XRLabel lbBillTo;
    private XRLabel lbCountry;
    private XRLine xrLine3;
    private XRLine xrLine2;
    private GroupFooterBand GroupFooter1;
    private XRLabel xrLabel4;
    private XRLabel xrLabel1;
    private XRLine xrLine4;
    private XRLabel xrLabel16;
    private XRLabel xrLabel17;
    private dsSaleReturn dsSaleReturn1;
    private dsSaleReturnTableAdapters.pos_sp_UserWiseSaleReturnReportTableAdapter pos_sp_UserWiseSaleReturnReportTableAdapter1;
    private XRLabel xrLabel9;
    private XRLabel lblDateTo;
    private XRLabel xrLabel10;
    private XRLabel lblDateFrom;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

    public UserWiseSaleReport(DateTime FromDate, DateTime ToDate, int BranchId, int UserId,int Pos_id)
	{
        Connection con = new Connection();
        InitializeComponent();
        pos_sp_UserWiseSaleReturnReportTableAdapter1.Connection = new System.Data.SqlClient.SqlConnection(con.sqlDataString);
        pos_sp_UserWiseSaleReturnReportTableAdapter1.Fill(dsSaleReturn1.pos_sp_UserWiseSaleReturnReport, Convert.ToDateTime(FromDate), Convert.ToDateTime(ToDate), BranchId, UserId,Pos_id);            
        lblDateFrom.Text = (FromDate).ToString("dd-MM-yyyy");
        lblDateTo.Text = (ToDate).ToString("dd-MM-yyyy");
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "UserWiseSaleReport.resx";
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.clOrderID = new DevExpress.XtraReports.UI.XRTableCell();
        this.clCustomerID = new DevExpress.XtraReports.UI.XRTableCell();
        this.clSalesperson = new DevExpress.XtraReports.UI.XRTableCell();
        this.clOrderDate = new DevExpress.XtraReports.UI.XRTableCell();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblDateTo = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblDateFrom = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
        this.lbBillTo = new DevExpress.XtraReports.UI.XRLabel();
        this.lbCountry = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
        this.dsSaleReturn1 = new dsSaleReturn();
        this.pos_sp_UserWiseSaleReturnReportTableAdapter1 = new dsSaleReturnTableAdapters.pos_sp_UserWiseSaleReturnReportTableAdapter();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReturn1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3,
            this.xrTable1});
        this.Detail.HeightF = 25.83333F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLine3
        // 
        this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 15.70834F);
        this.xrLine3.Name = "xrLine3";
        this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine3.SizeF = new System.Drawing.SizeF(267.9167F, 8.000011F);
        // 
        // xrTable1
        // 
        this.xrTable1.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable1.SizeF = new System.Drawing.SizeF(267.9167F, 15.70834F);
        this.xrTable1.StylePriority.UseFont = false;
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
        this.xrTableRow2.BorderColor = System.Drawing.Color.White;
        this.xrTableRow2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.clOrderID,
            this.clCustomerID,
            this.clSalesperson,
            this.clOrderDate});
        this.xrTableRow2.Font = new System.Drawing.Font("Tahoma", 8.25F);
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow2.Weight = 0.25842696629213485D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.BackColor = System.Drawing.Color.White;
        this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.Bill_No")});
        this.xrTableCell6.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.StylePriority.UseBackColor = false;
        this.xrTableCell6.StylePriority.UseFont = false;
        this.xrTableCell6.StylePriority.UseTextAlignment = false;
        xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell6.Summary = xrSummary1;
        this.xrTableCell6.Text = "xrTableCell6";
        this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell6.Weight = 0.078805016222901259D;
        // 
        // clOrderID
        // 
        this.clOrderID.BackColor = System.Drawing.Color.White;
        this.clOrderID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.Short_Name1")});
        this.clOrderID.Font = new System.Drawing.Font("Calibri", 8F);
        this.clOrderID.Name = "clOrderID";
        this.clOrderID.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clOrderID.StylePriority.UseBackColor = false;
        this.clOrderID.StylePriority.UseFont = false;
        this.clOrderID.StylePriority.UsePadding = false;
        this.clOrderID.StylePriority.UseTextAlignment = false;
        this.clOrderID.Text = "clOrderID";
        this.clOrderID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.clOrderID.Weight = 0.20282399200400628D;
        // 
        // clCustomerID
        // 
        this.clCustomerID.BackColor = System.Drawing.Color.White;
        this.clCustomerID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.QTY", "{0:.000}")});
        this.clCustomerID.Font = new System.Drawing.Font("Calibri", 8F);
        this.clCustomerID.Name = "clCustomerID";
        this.clCustomerID.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clCustomerID.StylePriority.UseBackColor = false;
        this.clCustomerID.StylePriority.UseFont = false;
        this.clCustomerID.StylePriority.UsePadding = false;
        this.clCustomerID.StylePriority.UseTextAlignment = false;
        this.clCustomerID.Text = "clCustomerID";
        this.clCustomerID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clCustomerID.Weight = 0.08942322300111D;
        // 
        // clSalesperson
        // 
        this.clSalesperson.BackColor = System.Drawing.Color.White;
        this.clSalesperson.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.RATE")});
        this.clSalesperson.Font = new System.Drawing.Font("Calibri", 8F);
        this.clSalesperson.Name = "clSalesperson";
        this.clSalesperson.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clSalesperson.StylePriority.UseBackColor = false;
        this.clSalesperson.StylePriority.UseFont = false;
        this.clSalesperson.StylePriority.UsePadding = false;
        this.clSalesperson.StylePriority.UseTextAlignment = false;
        this.clSalesperson.Text = "clSalesperson";
        this.clSalesperson.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clSalesperson.Weight = 0.080287741661486139D;
        // 
        // clOrderDate
        // 
        this.clOrderDate.BackColor = System.Drawing.Color.White;
        this.clOrderDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.AMOUNT", "{0:#.00}")});
        this.clOrderDate.Font = new System.Drawing.Font("Calibri", 8F);
        this.clOrderDate.Name = "clOrderDate";
        this.clOrderDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 1, 0, 0, 100F);
        this.clOrderDate.StylePriority.UseBackColor = false;
        this.clOrderDate.StylePriority.UseFont = false;
        this.clOrderDate.StylePriority.UsePadding = false;
        this.clOrderDate.StylePriority.UseTextAlignment = false;
        this.clOrderDate.Text = "clOrderDate";
        this.clOrderDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.clOrderDate.Weight = 0.098985777203646416D;
        // 
        // TopMargin
        // 
        this.TopMargin.HeightF = 1F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 1F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.lblDateTo,
            this.xrLabel10,
            this.lblDateFrom,
            this.xrLine5,
            this.xrLabel2,
            this.xrLabel15,
            this.xrLabel25,
            this.xrLabel14,
            this.xrLabel3,
            this.xrLabel11});
        this.PageHeader.HeightF = 162.2917F;
        this.PageHeader.Name = "PageHeader";
        // 
        // xrLabel9
        // 
        this.xrLabel9.BackColor = System.Drawing.Color.White;
        this.xrLabel9.BorderColor = System.Drawing.Color.White;
        this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel9.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel9.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(137.1063F, 134.9999F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel9.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel9.SizeF = new System.Drawing.SizeF(29.16667F, 18.29172F);
        this.xrLabel9.StylePriority.UseBackColor = false;
        this.xrLabel9.StylePriority.UseFont = false;
        this.xrLabel9.StylePriority.UseTextAlignment = false;
        this.xrLabel9.Text = "To";
        this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // lblDateTo
        // 
        this.lblDateTo.BackColor = System.Drawing.Color.White;
        this.lblDateTo.BorderColor = System.Drawing.Color.White;
        this.lblDateTo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblDateTo.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblDateTo.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lblDateTo.LocationFloat = new DevExpress.Utils.PointFloat(166.273F, 134.9999F);
        this.lblDateTo.Name = "lblDateTo";
        this.lblDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lblDateTo.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.lblDateTo.SizeF = new System.Drawing.SizeF(91.71303F, 18.29172F);
        this.lblDateTo.StylePriority.UseBackColor = false;
        this.lblDateTo.StylePriority.UseFont = false;
        this.lblDateTo.StylePriority.UseTextAlignment = false;
        this.lblDateTo.Text = "xrLabel6";
        this.lblDateTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel10
        // 
        this.xrLabel10.BackColor = System.Drawing.Color.White;
        this.xrLabel10.BorderColor = System.Drawing.Color.White;
        this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel10.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel10.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(6.152674F, 134.9999F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel10.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel10.SizeF = new System.Drawing.SizeF(40.171F, 18.29169F);
        this.xrLabel10.StylePriority.UseBackColor = false;
        this.xrLabel10.StylePriority.UseFont = false;
        this.xrLabel10.StylePriority.UseTextAlignment = false;
        this.xrLabel10.Text = "From";
        this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // lblDateFrom
        // 
        this.lblDateFrom.BackColor = System.Drawing.Color.White;
        this.lblDateFrom.BorderColor = System.Drawing.Color.White;
        this.lblDateFrom.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblDateFrom.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblDateFrom.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lblDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(46.32367F, 134.9999F);
        this.lblDateFrom.Name = "lblDateFrom";
        this.lblDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lblDateFrom.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.lblDateFrom.SizeF = new System.Drawing.SizeF(90.78267F, 18.29172F);
        this.lblDateFrom.StylePriority.UseBackColor = false;
        this.lblDateFrom.StylePriority.UseFont = false;
        this.lblDateFrom.StylePriority.UseTextAlignment = false;
        this.lblDateFrom.Text = "xrLabel6";
        this.lblDateFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLine5
        // 
        this.xrLine5.LineWidth = 3;
        this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(5F, 153.2916F);
        this.xrLine5.Name = "xrLine5";
        this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine5.SizeF = new System.Drawing.SizeF(258.3333F, 8F);
        // 
        // xrLabel2
        // 
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.header1")});
        this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.StylePriority.UseTextAlignment = false;
        this.xrLabel2.Text = "xrLabel2";
        this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel15
        // 
        this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(5F, 91.99991F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(260F, 23.00002F);
        this.xrLabel15.StylePriority.UseFont = false;
        this.xrLabel15.StylePriority.UseTextAlignment = false;
        this.xrLabel15.Text = "[Header5]";
        this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel25
        // 
        this.xrLabel25.BackColor = System.Drawing.Color.White;
        this.xrLabel25.BorderColor = System.Drawing.Color.White;
        this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel25.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel25.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(70.97343F, 114.9999F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(128.417F, 20F);
        this.xrLabel25.StylePriority.UseBackColor = false;
        this.xrLabel25.StylePriority.UseFont = false;
        this.xrLabel25.StylePriority.UsePadding = false;
        this.xrLabel25.StylePriority.UseTextAlignment = false;
        this.xrLabel25.Text = "USER SALE REPORT";
        this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel14
        // 
        this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.header4")});
        this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(5F, 68.99992F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel14.StylePriority.UseFont = false;
        this.xrLabel14.StylePriority.UseTextAlignment = false;
        this.xrLabel14.Text = "xrLabel14";
        this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel3
        // 
        this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.header2")});
        this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(5F, 22.99999F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel3.StylePriority.UseFont = false;
        this.xrLabel3.StylePriority.UseTextAlignment = false;
        this.xrLabel3.Text = "xrLabel3";
        this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel11
        // 
        this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.header3")});
        this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(5F, 45.99994F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel11.StylePriority.UseFont = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.Text = "xrLabel11";
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // GroupHeader1
        // 
        this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.lbBillTo,
            this.lbCountry,
            this.xrLine1,
            this.xrTable3});
        this.GroupHeader1.HeightF = 62.8333F;
        this.GroupHeader1.Name = "GroupHeader1";
        // 
        // xrLine2
        // 
        this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 54.83327F);
        this.xrLine2.Name = "xrLine2";
        this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine2.SizeF = new System.Drawing.SizeF(267.9167F, 8.000011F);
        // 
        // lbBillTo
        // 
        this.lbBillTo.BackColor = System.Drawing.Color.White;
        this.lbBillTo.BorderColor = System.Drawing.Color.White;
        this.lbBillTo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lbBillTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbBillTo.ForeColor = System.Drawing.Color.Black;
        this.lbBillTo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 34.83328F);
        this.lbBillTo.Name = "lbBillTo";
        this.lbBillTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbBillTo.SizeF = new System.Drawing.SizeF(57.08766F, 20F);
        this.lbBillTo.StylePriority.UseBackColor = false;
        this.lbBillTo.StylePriority.UseFont = false;
        this.lbBillTo.StylePriority.UseTextAlignment = false;
        this.lbBillTo.Text = "Bill Date:";
        this.lbBillTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // lbCountry
        // 
        this.lbCountry.BackColor = System.Drawing.Color.White;
        this.lbCountry.BorderColor = System.Drawing.Color.White;
        this.lbCountry.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lbCountry.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.BILL_DATE", "{0:dd-MMM-yy}")});
        this.lbCountry.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lbCountry.ForeColor = System.Drawing.SystemColors.WindowText;
        this.lbCountry.LocationFloat = new DevExpress.Utils.PointFloat(57.08766F, 34.83328F);
        this.lbCountry.Name = "lbCountry";
        this.lbCountry.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.lbCountry.SizeF = new System.Drawing.SizeF(66.4917F, 20F);
        this.lbCountry.StylePriority.UseBackColor = false;
        this.lbCountry.StylePriority.UseFont = false;
        this.lbCountry.StylePriority.UsePadding = false;
        this.lbCountry.StylePriority.UseTextAlignment = false;
        this.lbCountry.Text = "lbCountry";
        this.lbCountry.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLine1
        // 
        this.xrLine1.LineWidth = 3;
        this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 26.83328F);
        this.xrLine1.Name = "xrLine1";
        this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine1.SizeF = new System.Drawing.SizeF(258.3333F, 8F);
        // 
        // xrTable3
        // 
        this.xrTable3.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(5F, 0F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(265F, 26.83328F);
        this.xrTable3.StylePriority.UseFont = false;
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(167)))), ((int)(((byte)(73)))));
        this.xrTableRow3.BorderColor = System.Drawing.Color.White;
        this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4});
        this.xrTableRow3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableRow3.ForeColor = System.Drawing.Color.White;
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow3.StylePriority.UseBackColor = false;
        this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow3.Weight = 0.25842696629213485D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.BackColor = System.Drawing.Color.White;
        this.xrTableCell5.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableCell5.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.StylePriority.UseBackColor = false;
        this.xrTableCell5.StylePriority.UseFont = false;
        this.xrTableCell5.StylePriority.UseForeColor = false;
        this.xrTableCell5.StylePriority.UseTextAlignment = false;
        this.xrTableCell5.Text = "Bill No";
        this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell5.Weight = 0.072113487085737824D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.BackColor = System.Drawing.Color.White;
        this.xrTableCell1.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell1.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell1.StylePriority.UseBackColor = false;
        this.xrTableCell1.StylePriority.UseFont = false;
        this.xrTableCell1.StylePriority.UseForeColor = false;
        this.xrTableCell1.StylePriority.UseTextAlignment = false;
        this.xrTableCell1.Text = "Item Name";
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell1.Weight = 0.21341570377283986D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.BackColor = System.Drawing.Color.White;
        this.xrTableCell2.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell2.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell2.StylePriority.UseBackColor = false;
        this.xrTableCell2.StylePriority.UseFont = false;
        this.xrTableCell2.StylePriority.UseForeColor = false;
        this.xrTableCell2.Text = "Qty";
        this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell2.Weight = 0.094092921690416179D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.BackColor = System.Drawing.Color.White;
        this.xrTableCell3.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell3.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell3.StylePriority.UseBackColor = false;
        this.xrTableCell3.StylePriority.UseFont = false;
        this.xrTableCell3.StylePriority.UseForeColor = false;
        this.xrTableCell3.Text = "Rate";
        this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell3.Weight = 0.084480476249175152D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.BackColor = System.Drawing.Color.White;
        this.xrTableCell4.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
        this.xrTableCell4.ForeColor = System.Drawing.Color.Black;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell4.StylePriority.UseBackColor = false;
        this.xrTableCell4.StylePriority.UseFont = false;
        this.xrTableCell4.StylePriority.UseForeColor = false;
        this.xrTableCell4.StylePriority.UseTextAlignment = false;
        this.xrTableCell4.Text = "Amt";
        this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell4.Weight = 0.1086576624027632D;
        // 
        // GroupFooter1
        // 
        this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel1,
            this.xrLine4,
            this.xrLabel16,
            this.xrLabel17});
        this.GroupFooter1.HeightF = 48.79169F;
        this.GroupFooter1.Name = "GroupFooter1";
        // 
        // xrLabel4
        // 
        this.xrLabel4.BackColor = System.Drawing.Color.White;
        this.xrLabel4.BorderColor = System.Drawing.Color.White;
        this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.AMOUNT")});
        this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel4.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(199.3904F, 30.49998F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(70.60947F, 17F);
        this.xrLabel4.StylePriority.UseBackColor = false;
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UsePadding = false;
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        xrSummary2.FormatString = "{0:#.00}";
        xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Page;
        this.xrLabel4.Summary = xrSummary2;
        this.xrLabel4.Text = "xrLabel4";
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel1
        // 
        this.xrLabel1.BackColor = System.Drawing.Color.White;
        this.xrLabel1.BorderColor = System.Drawing.Color.White;
        this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel1.ForeColor = System.Drawing.Color.Black;
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(113.773F, 30.49998F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(85.61748F, 18.29169F);
        this.xrLabel1.StylePriority.UseBackColor = false;
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.Text = "Grand Total:";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLine4
        // 
        this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 18.29169F);
        this.xrLine4.Name = "xrLine4";
        this.xrLine4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine4.SizeF = new System.Drawing.SizeF(267.9167F, 8.000011F);
        // 
        // xrLabel16
        // 
        this.xrLabel16.BackColor = System.Drawing.Color.White;
        this.xrLabel16.BorderColor = System.Drawing.Color.White;
        this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel16.ForeColor = System.Drawing.Color.Black;
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(151.5181F, 0F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel16.SizeF = new System.Drawing.SizeF(47.87239F, 18.29169F);
        this.xrLabel16.StylePriority.UseBackColor = false;
        this.xrLabel16.StylePriority.UseFont = false;
        this.xrLabel16.Text = "Total:";
        this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel17
        // 
        this.xrLabel17.BackColor = System.Drawing.Color.White;
        this.xrLabel17.BorderColor = System.Drawing.Color.White;
        this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_UserWiseSaleReturnReport.AMOUNT")});
        this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel17.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(199.3904F, 0F);
        this.xrLabel17.Name = "xrLabel17";
        this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 2, 0, 100F);
        this.xrLabel17.SizeF = new System.Drawing.SizeF(70.60947F, 17F);
        this.xrLabel17.StylePriority.UseBackColor = false;
        this.xrLabel17.StylePriority.UseFont = false;
        this.xrLabel17.StylePriority.UsePadding = false;
        this.xrLabel17.StylePriority.UseTextAlignment = false;
        xrSummary3.FormatString = "{0:#.00}";
        xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrLabel17.Summary = xrSummary3;
        this.xrLabel17.Text = "xrLabel17";
        this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // dsSaleReturn1
        // 
        this.dsSaleReturn1.DataSetName = "dsSaleReturn";
        this.dsSaleReturn1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // pos_sp_UserWiseSaleReturnReportTableAdapter1
        // 
        this.pos_sp_UserWiseSaleReturnReportTableAdapter1.ClearBeforeFill = true;
        // 
        // UserWiseSaleReport
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1});
        this.DataAdapter = this.pos_sp_UserWiseSaleReturnReportTableAdapter1;
        this.DataMember = "pos_sp_UserWiseSaleReturnReport";
        this.DataSource = this.dsSaleReturn1;
        this.Margins = new System.Drawing.Printing.Margins(0, 0, 1, 1);
        this.PageWidth = 270;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReturn1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
