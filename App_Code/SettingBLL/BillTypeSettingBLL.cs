﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillTypeSettingBLL
/// </summary>
public class BillTypeSettingBLL
{

    public Int16 UpdateBasicSettings(BillTypeSettings objSettings)
    {

        return new BillTypeSettingDAL().UpdateBasicSettings(objSettings);
    }

    public void GetSettings(BillTypeSettings objSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new BillTypeSettingDAL().GetMasterSettings(objSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.LocalBillling = Convert.ToBoolean(dr["LocalBillling"]);
                objSettings.VatBilling = Convert.ToBoolean(dr["VatBilling"]);
                objSettings.RetailBilling = Convert.ToBoolean(dr["RetailBilling"]);
                objSettings.Outstation = Convert.ToBoolean(dr["Outstation"]);
                objSettings.SaleTax = Convert.ToBoolean(dr["SaleTax"]);
                objSettings.Ret_Def_Series = Convert.ToString(dr["Ret_Def_Series"]);
                objSettings.Vat_Def_Series = Convert.ToString(dr["Vat_Def_Series"]);
                objSettings.CST_Def_Series = Convert.ToString(dr["CST_Def_Series"]);
             
               
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

}