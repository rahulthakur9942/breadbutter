﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ParamClass
/// </summary>

public class ParamsClass
{


    public static string gstrApplicationPreffix = System.Configuration.ConfigurationManager.AppSettings["SITE_PREFIX"];
    public static string sqlDataString = HttpContext.Current.Request.Cookies[Constants.DataBase].Value == "6" ? System.Configuration.ConfigurationManager.AppSettings["DB_CONN1"] : System.Configuration.ConfigurationManager.AppSettings["DB_CONN"];

   // public static string sqlDataString = System.Configuration.ConfigurationManager.AppSettings["DB_CONN"];
    public static string gstrStoredProcPrefix = gstrApplicationPreffix + "SP_";

}