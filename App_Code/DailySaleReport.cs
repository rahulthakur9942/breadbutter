﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for DailySaleReport
/// </summary>
public class DailySaleReport : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private PageHeaderBand PageHeader;
    private XRLabel xrLabel14;
    private XRLabel xrLabel15;
    private XRLabel xrLabel11;
    private XRLabel xrLabel3;
    private XRLabel xrLabel2;
    private GroupHeaderBand GroupHeader1;
    private XRLabel xrLabel1;
    private XRLine xrLine1;
    private XRLabel xrLabel20;
    private XRLabel xrLabel6;
    private XRLabel xrLabel4;
    private XRLine xrLine5;
    private XRLabel xrLabel31;
    private dsSaleReports dsSaleReports1;
    private dsSaleReportsTableAdapters.pos_sp_DailySaleReportTableAdapter pos_sp_DailySaleReportTableAdapter1;
    private XRLabel xrLabel7;
    private XRLabel xrLabel13;
    private XRLabel xrLabel12;
    private XRLabel xrLabel10;
    private XRLabel xrLabel9;
    private XRLabel xrLabel8;
    private XRLabel xrLabel16;
    private XRLabel xrLabel17;
    private XRLine xrLine2;
    private XRLabel xrLabel5;
    private dsSaleReports dsSaleReports2;
    private XRLabel xrLabel24;
    private XRLabel xrLabel23;
    private XRLabel xrLabel22;
    private XRLabel xrLabel21;
    private XRLabel xrLabel19;
    private XRLabel xrLabel18;
    private XRLine xrLine3;
    private XRLabel lbBillTo;
    private ReportHeaderBand ReportHeader;
    private ReportFooterBand ReportFooter;
    private XRLabel xrLabel26;
    private XRLabel xrLabel25;
    private dsSaleReports dsSaleReports3;
    private XRLabel xrLabel28;
    private XRLabel xrLabel27;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public DailySaleReport(DateTime DateFrom,DateTime DateTo, int branchId,int Pos_id)
	{
		Connection con = new Connection();
		InitializeComponent();
        pos_sp_DailySaleReportTableAdapter1.Connection = new System.Data.SqlClient.SqlConnection(con.sqlDataString);
        pos_sp_DailySaleReportTableAdapter1.Fill(dsSaleReports1.pos_sp_DailySaleReport,  Convert.ToDateTime(DateFrom), Convert.ToDateTime(DateTo),branchId, Pos_id);
        xrLabel8.Text = DateFrom.ToString("dd-MMM-yyyy");
        xrLabel12.Text = DateTo.ToString("dd-MMM-yyyy");
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "DailySaleReport.resx";
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
        this.lbBillTo = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
        this.dsSaleReports1 = new dsSaleReports();
        this.pos_sp_DailySaleReportTableAdapter1 = new dsSaleReportsTableAdapters.pos_sp_DailySaleReportTableAdapter();
        this.dsSaleReports2 = new dsSaleReports();
        this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.dsSaleReports3 = new dsSaleReports();
        this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReports1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReports2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReports3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLabel16,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel4});
        this.Detail.HeightF = 27.91678F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLine2
        // 
        this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 18.29172F);
        this.xrLine2.Name = "xrLine2";
        this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine2.SizeF = new System.Drawing.SizeF(263.5F, 8.000013F);
        // 
        // xrLabel16
        // 
        this.xrLabel16.BackColor = System.Drawing.Color.White;
        this.xrLabel16.BorderColor = System.Drawing.Color.White;
        this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel16.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel16.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(86.02612F, 0F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel16.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel16.SizeF = new System.Drawing.SizeF(13.77621F, 18.29172F);
        this.xrLabel16.StylePriority.UseBackColor = false;
        this.xrLabel16.StylePriority.UseFont = false;
        this.xrLabel16.StylePriority.UseTextAlignment = false;
        this.xrLabel16.Text = "-";
        this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel7
        // 
        this.xrLabel7.BackColor = System.Drawing.Color.White;
        this.xrLabel7.BorderColor = System.Drawing.Color.White;
        this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.ToBill")});
        this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 12F);
        this.xrLabel7.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(99.80232F, 0F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel7.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel7.SizeF = new System.Drawing.SizeF(69.8535F, 18.29172F);
        this.xrLabel7.StylePriority.UseBackColor = false;
        this.xrLabel7.StylePriority.UseFont = false;
        this.xrLabel7.StylePriority.UseTextAlignment = false;
        this.xrLabel7.Text = "xrLabel7";
        this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel6
        // 
        this.xrLabel6.BackColor = System.Drawing.Color.White;
        this.xrLabel6.BorderColor = System.Drawing.Color.White;
        this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.FromBill")});
        this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 12F);
        this.xrLabel6.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(6.999939F, 0F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
        this.xrLabel6.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel6.SizeF = new System.Drawing.SizeF(79.02618F, 18.29172F);
        this.xrLabel6.StylePriority.UseBackColor = false;
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.StylePriority.UsePadding = false;
        this.xrLabel6.StylePriority.UseTextAlignment = false;
        this.xrLabel6.Text = "xrLabel6";
        this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel4
        // 
        this.xrLabel4.BackColor = System.Drawing.Color.White;
        this.xrLabel4.BorderColor = System.Drawing.Color.White;
        this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.Amount")});
        this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 12F);
        this.xrLabel4.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(174.8492F, 0F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel4.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel4.SizeF = new System.Drawing.SizeF(86.98401F, 15.45826F);
        this.xrLabel4.StylePriority.UseBackColor = false;
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        this.xrLabel4.Text = "xrLabel4";
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel20
        // 
        this.xrLabel20.BackColor = System.Drawing.Color.White;
        this.xrLabel20.BorderColor = System.Drawing.Color.White;
        this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.BillDate", "{0:dd-MMM-yy}")});
        this.xrLabel20.Font = new System.Drawing.Font("Tahoma", 12F);
        this.xrLabel20.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(87.73451F, 1.708285F);
        this.xrLabel20.Name = "xrLabel20";
        this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel20.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel20.SizeF = new System.Drawing.SizeF(165.9059F, 18.29172F);
        this.xrLabel20.StylePriority.UseBackColor = false;
        this.xrLabel20.StylePriority.UseFont = false;
        this.xrLabel20.StylePriority.UseTextAlignment = false;
        this.xrLabel20.Text = "xrLabel20";
        this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // TopMargin
        // 
        this.TopMargin.HeightF = 0F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 0F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // PageHeader
        // 
        this.PageHeader.HeightF = 3.000005F;
        this.PageHeader.Name = "PageHeader";
        // 
        // xrLine3
        // 
        this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(3.499937F, 175.75F);
        this.xrLine3.Name = "xrLine3";
        this.xrLine3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine3.SizeF = new System.Drawing.SizeF(263.5F, 8.000013F);
        // 
        // xrLabel5
        // 
        this.xrLabel5.BackColor = System.Drawing.Color.White;
        this.xrLabel5.BorderColor = System.Drawing.Color.White;
        this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel5.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(6.999938F, 155.75F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel5.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel5.SizeF = new System.Drawing.SizeF(158.4274F, 18.29172F);
        this.xrLabel5.StylePriority.UseBackColor = false;
        this.xrLabel5.StylePriority.UseFont = false;
        this.xrLabel5.StylePriority.UseTextAlignment = false;
        this.xrLabel5.Text = "Bills";
        this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel1
        // 
        this.xrLabel1.BackColor = System.Drawing.Color.White;
        this.xrLabel1.BorderColor = System.Drawing.Color.White;
        this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel1.ForeColor = System.Drawing.Color.Black;
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(174.8492F, 155.75F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(78.79126F, 20F);
        this.xrLabel1.StylePriority.UseBackColor = false;
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.StylePriority.UseTextAlignment = false;
        this.xrLabel1.Text = "Amount";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel13
        // 
        this.xrLabel13.BackColor = System.Drawing.Color.White;
        this.xrLabel13.BorderColor = System.Drawing.Color.White;
        this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel13.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(8.708344F, 119.1666F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel13.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel13.SizeF = new System.Drawing.SizeF(258.2916F, 18.29173F);
        this.xrLabel13.StylePriority.UseBackColor = false;
        this.xrLabel13.StylePriority.UseFont = false;
        this.xrLabel13.StylePriority.UseTextAlignment = false;
        this.xrLabel13.Text = "Daily Sale Report";
        this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel12
        // 
        this.xrLabel12.BackColor = System.Drawing.Color.White;
        this.xrLabel12.BorderColor = System.Drawing.Color.White;
        this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
        this.xrLabel12.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(161.9274F, 137.4583F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel12.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel12.SizeF = new System.Drawing.SizeF(91.71303F, 18.29172F);
        this.xrLabel12.StylePriority.UseBackColor = false;
        this.xrLabel12.StylePriority.UseFont = false;
        this.xrLabel12.StylePriority.UseTextAlignment = false;
        this.xrLabel12.Text = "xrLabel6";
        this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel10
        // 
        this.xrLabel10.BackColor = System.Drawing.Color.White;
        this.xrLabel10.BorderColor = System.Drawing.Color.White;
        this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel10.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
        this.xrLabel10.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(1.807115F, 137.4583F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel10.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel10.SizeF = new System.Drawing.SizeF(40.171F, 18.29169F);
        this.xrLabel10.StylePriority.UseBackColor = false;
        this.xrLabel10.StylePriority.UseFont = false;
        this.xrLabel10.StylePriority.UseTextAlignment = false;
        this.xrLabel10.Text = "From";
        this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel9
        // 
        this.xrLabel9.BackColor = System.Drawing.Color.White;
        this.xrLabel9.BorderColor = System.Drawing.Color.White;
        this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel9.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
        this.xrLabel9.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(132.7607F, 137.4583F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel9.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel9.SizeF = new System.Drawing.SizeF(29.16667F, 18.29172F);
        this.xrLabel9.StylePriority.UseBackColor = false;
        this.xrLabel9.StylePriority.UseFont = false;
        this.xrLabel9.StylePriority.UseTextAlignment = false;
        this.xrLabel9.Text = "To";
        this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel8
        // 
        this.xrLabel8.BackColor = System.Drawing.Color.White;
        this.xrLabel8.BorderColor = System.Drawing.Color.White;
        this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel8.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
        this.xrLabel8.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(41.9781F, 137.4583F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel8.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel8.SizeF = new System.Drawing.SizeF(90.78267F, 18.29172F);
        this.xrLabel8.StylePriority.UseBackColor = false;
        this.xrLabel8.StylePriority.UseFont = false;
        this.xrLabel8.StylePriority.UseTextAlignment = false;
        this.xrLabel8.Text = "xrLabel6";
        this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel14
        // 
        this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.header4")});
        this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(6.999939F, 73.1666F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel14.StylePriority.UseFont = false;
        this.xrLabel14.StylePriority.UseTextAlignment = false;
        this.xrLabel14.Text = "xrLabel14";
        this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel15
        // 
        this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(6.999939F, 96.16658F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(260F, 23.00002F);
        this.xrLabel15.StylePriority.UseFont = false;
        this.xrLabel15.StylePriority.UseTextAlignment = false;
        this.xrLabel15.Text = "[Header5]";
        this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel11
        // 
        this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.header3")});
        this.xrLabel11.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(6.999939F, 50.16661F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel11.StylePriority.UseFont = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.Text = "xrLabel11";
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel3
        // 
        this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.header2")});
        this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(6.999939F, 27.16665F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel3.StylePriority.UseFont = false;
        this.xrLabel3.StylePriority.UseTextAlignment = false;
        this.xrLabel3.Text = "xrLabel3";
        this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel2
        // 
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.header1")});
        this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(6.999939F, 4.166667F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(260F, 23F);
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.StylePriority.UseTextAlignment = false;
        this.xrLabel2.Text = "xrLabel2";
        this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // GroupHeader1
        // 
        this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lbBillTo,
            this.xrLabel20});
        this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("BillDate", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.GroupHeader1.HeightF = 28.00001F;
        this.GroupHeader1.Name = "GroupHeader1";
        // 
        // xrLine1
        // 
        this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20F);
        this.xrLine1.Name = "xrLine1";
        this.xrLine1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine1.SizeF = new System.Drawing.SizeF(263.5F, 8.000013F);
        // 
        // lbBillTo
        // 
        this.lbBillTo.BackColor = System.Drawing.Color.White;
        this.lbBillTo.BorderColor = System.Drawing.Color.White;
        this.lbBillTo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lbBillTo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
        this.lbBillTo.ForeColor = System.Drawing.Color.Black;
        this.lbBillTo.LocationFloat = new DevExpress.Utils.PointFloat(3.500001F, 0F);
        this.lbBillTo.Name = "lbBillTo";
        this.lbBillTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.lbBillTo.SizeF = new System.Drawing.SizeF(84.23452F, 20F);
        this.lbBillTo.StylePriority.UseBackColor = false;
        this.lbBillTo.StylePriority.UseFont = false;
        this.lbBillTo.StylePriority.UseTextAlignment = false;
        this.lbBillTo.Text = "Bill Date";
        this.lbBillTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel24
        // 
        this.xrLabel24.BackColor = System.Drawing.Color.White;
        this.xrLabel24.BorderColor = System.Drawing.Color.White;
        this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.CrCard")});
        this.xrLabel24.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel24.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(112.0009F, 64.87509F);
        this.xrLabel24.Name = "xrLabel24";
        this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLabel24.SizeF = new System.Drawing.SizeF(141.6395F, 18.2917F);
        this.xrLabel24.StylePriority.UseBackColor = false;
        this.xrLabel24.StylePriority.UseFont = false;
        this.xrLabel24.StylePriority.UsePadding = false;
        this.xrLabel24.StylePriority.UseTextAlignment = false;
        this.xrLabel24.Text = "xrLabel24";
        this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel23
        // 
        this.xrLabel23.BackColor = System.Drawing.Color.White;
        this.xrLabel23.BorderColor = System.Drawing.Color.White;
        this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel23.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel23.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(20.41663F, 65.3335F);
        this.xrLabel23.Name = "xrLabel23";
        this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel23.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel23.SizeF = new System.Drawing.SizeF(67.31789F, 18.29171F);
        this.xrLabel23.StylePriority.UseBackColor = false;
        this.xrLabel23.StylePriority.UseFont = false;
        this.xrLabel23.StylePriority.UseTextAlignment = false;
        this.xrLabel23.Text = "CrCard";
        this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel22
        // 
        this.xrLabel22.BackColor = System.Drawing.Color.White;
        this.xrLabel22.BorderColor = System.Drawing.Color.White;
        this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.Credit")});
        this.xrLabel22.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel22.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(112.0009F, 101.9169F);
        this.xrLabel22.Name = "xrLabel22";
        this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLabel22.SizeF = new System.Drawing.SizeF(141.6395F, 18.29168F);
        this.xrLabel22.StylePriority.UseBackColor = false;
        this.xrLabel22.StylePriority.UseFont = false;
        this.xrLabel22.StylePriority.UsePadding = false;
        this.xrLabel22.StylePriority.UseTextAlignment = false;
        this.xrLabel22.Text = "xrLabel22";
        this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel21
        // 
        this.xrLabel21.BackColor = System.Drawing.Color.White;
        this.xrLabel21.BorderColor = System.Drawing.Color.White;
        this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel21.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel21.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(20.41663F, 101.9169F);
        this.xrLabel21.Name = "xrLabel21";
        this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel21.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel21.SizeF = new System.Drawing.SizeF(67.31789F, 18.29172F);
        this.xrLabel21.StylePriority.UseBackColor = false;
        this.xrLabel21.StylePriority.UseFont = false;
        this.xrLabel21.StylePriority.UseTextAlignment = false;
        this.xrLabel21.Text = "Credit";
        this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel19
        // 
        this.xrLabel19.BackColor = System.Drawing.Color.White;
        this.xrLabel19.BorderColor = System.Drawing.Color.White;
        this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.Cash")});
        this.xrLabel19.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel19.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(112.0009F, 28.29173F);
        this.xrLabel19.Name = "xrLabel19";
        this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLabel19.SizeF = new System.Drawing.SizeF(141.6395F, 18.29169F);
        this.xrLabel19.StylePriority.UseBackColor = false;
        this.xrLabel19.StylePriority.UseFont = false;
        this.xrLabel19.StylePriority.UsePadding = false;
        this.xrLabel19.StylePriority.UseTextAlignment = false;
        this.xrLabel19.Text = "xrLabel19";
        this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel18
        // 
        this.xrLabel18.BackColor = System.Drawing.Color.White;
        this.xrLabel18.BorderColor = System.Drawing.Color.White;
        this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel18.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel18.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(20.41663F, 28.29173F);
        this.xrLabel18.Name = "xrLabel18";
        this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel18.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel18.SizeF = new System.Drawing.SizeF(67.31789F, 18.29172F);
        this.xrLabel18.StylePriority.UseBackColor = false;
        this.xrLabel18.StylePriority.UseFont = false;
        this.xrLabel18.StylePriority.UseTextAlignment = false;
        this.xrLabel18.Text = "Cash";
        this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrLabel18.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel18_BeforePrint);
        // 
        // xrLabel17
        // 
        this.xrLabel17.BackColor = System.Drawing.Color.White;
        this.xrLabel17.BorderColor = System.Drawing.Color.White;
        this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel17.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel17.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(20.41663F, 10F);
        this.xrLabel17.Name = "xrLabel17";
        this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel17.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel17.SizeF = new System.Drawing.SizeF(67.31789F, 18.29172F);
        this.xrLabel17.StylePriority.UseBackColor = false;
        this.xrLabel17.StylePriority.UseFont = false;
        this.xrLabel17.StylePriority.UseTextAlignment = false;
        this.xrLabel17.Text = "Total";
        this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel31
        // 
        this.xrLabel31.BackColor = System.Drawing.Color.White;
        this.xrLabel31.BorderColor = System.Drawing.Color.White;
        this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.Amount")});
        this.xrLabel31.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel31.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(98.881F, 7.999992F);
        this.xrLabel31.Name = "xrLabel31";
        this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLabel31.SizeF = new System.Drawing.SizeF(154.7594F, 18.29169F);
        this.xrLabel31.StylePriority.UseBackColor = false;
        this.xrLabel31.StylePriority.UseFont = false;
        this.xrLabel31.StylePriority.UsePadding = false;
        this.xrLabel31.StylePriority.UseTextAlignment = false;
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrLabel31.Summary = xrSummary1;
        this.xrLabel31.Text = "xrLabel31";
        this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLine5
        // 
        this.xrLine5.LineWidth = 3;
        this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(3.499937F, 0F);
        this.xrLine5.Name = "xrLine5";
        this.xrLine5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLine5.SizeF = new System.Drawing.SizeF(258.3333F, 8F);
        // 
        // dsSaleReports1
        // 
        this.dsSaleReports1.DataSetName = "dsSaleReports";
        this.dsSaleReports1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // pos_sp_DailySaleReportTableAdapter1
        // 
        this.pos_sp_DailySaleReportTableAdapter1.ClearBeforeFill = true;
        // 
        // dsSaleReports2
        // 
        this.dsSaleReports2.DataSetName = "dsSaleReports";
        this.dsSaleReports2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel11,
            this.xrLabel14,
            this.xrLabel15,
            this.xrLabel13,
            this.xrLabel8,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel12,
            this.xrLabel5,
            this.xrLabel1,
            this.xrLine3});
        this.ReportHeader.HeightF = 186.8749F;
        this.ReportHeader.Name = "ReportHeader";
        // 
        // ReportFooter
        // 
        this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel18,
            this.xrLabel21,
            this.xrLabel23,
            this.xrLabel17,
            this.xrLabel31,
            this.xrLabel19,
            this.xrLabel22,
            this.xrLabel24,
            this.xrLine5});
        this.ReportFooter.HeightF = 126.5837F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // xrLabel26
        // 
        this.xrLabel26.BackColor = System.Drawing.Color.White;
        this.xrLabel26.BorderColor = System.Drawing.Color.White;
        this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel26.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel26.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(20.41663F, 83.62516F);
        this.xrLabel26.Name = "xrLabel26";
        this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel26.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel26.SizeF = new System.Drawing.SizeF(88.32397F, 18.29171F);
        this.xrLabel26.StylePriority.UseBackColor = false;
        this.xrLabel26.StylePriority.UseFont = false;
        this.xrLabel26.StylePriority.UseTextAlignment = false;
        this.xrLabel26.Text = "OnlPay";
        this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel25
        // 
        this.xrLabel25.BackColor = System.Drawing.Color.White;
        this.xrLabel25.BorderColor = System.Drawing.Color.White;
        this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.OnlinePayment")});
        this.xrLabel25.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel25.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(112.001F, 83.16682F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(141.6395F, 18.2917F);
        this.xrLabel25.StylePriority.UseBackColor = false;
        this.xrLabel25.StylePriority.UseFont = false;
        this.xrLabel25.StylePriority.UsePadding = false;
        this.xrLabel25.StylePriority.UseTextAlignment = false;
        this.xrLabel25.Text = "xrLabel25";
        this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // dsSaleReports3
        // 
        this.dsSaleReports3.DataSetName = "dsSaleReports";
        this.dsSaleReports3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // xrLabel27
        // 
        this.xrLabel27.BackColor = System.Drawing.Color.White;
        this.xrLabel27.BorderColor = System.Drawing.Color.White;
        this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel27.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel27.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(20.41663F, 46.58346F);
        this.xrLabel27.Name = "xrLabel27";
        this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 0, 0, 0, 100F);
        this.xrLabel27.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel27.SizeF = new System.Drawing.SizeF(67.31789F, 18.29172F);
        this.xrLabel27.StylePriority.UseBackColor = false;
        this.xrLabel27.StylePriority.UseFont = false;
        this.xrLabel27.StylePriority.UseTextAlignment = false;
        this.xrLabel27.Text = "COD";
        this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel28
        // 
        this.xrLabel28.BackColor = System.Drawing.Color.White;
        this.xrLabel28.BorderColor = System.Drawing.Color.White;
        this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "pos_sp_DailySaleReport.COD")});
        this.xrLabel28.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
        this.xrLabel28.ForeColor = System.Drawing.SystemColors.WindowText;
        this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(112.001F, 46.58343F);
        this.xrLabel28.Name = "xrLabel28";
        this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrLabel28.SizeF = new System.Drawing.SizeF(141.6395F, 18.29169F);
        this.xrLabel28.StylePriority.UseBackColor = false;
        this.xrLabel28.StylePriority.UseFont = false;
        this.xrLabel28.StylePriority.UsePadding = false;
        this.xrLabel28.StylePriority.UseTextAlignment = false;
        this.xrLabel28.Text = "xrLabel28";
        this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // DailySaleReport
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.GroupHeader1,
            this.ReportHeader,
            this.ReportFooter});
        this.DataAdapter = this.pos_sp_DailySaleReportTableAdapter1;
        this.DataMember = "pos_sp_DailySaleReport";
        this.DataSource = this.dsSaleReports3;
        this.Margins = new System.Drawing.Printing.Margins(0, 2, 0, 0);
        this.PageHeight = 400;
        this.PageWidth = 270;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReports1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReports2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSaleReports3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion

    private void xrLabel18_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {

    }
}
