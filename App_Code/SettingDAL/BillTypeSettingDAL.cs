﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BillTypeSettingDAL
/// </summary>
public class BillTypeSettingDAL:Connection
{
    public Int16 UpdateBasicSettings(BillTypeSettings objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[12];

        objParam[0] = new SqlParameter("@LocalBillling", objSettings.LocalBillling);
        objParam[1] = new SqlParameter("@RetailBilling", objSettings.RetailBilling);
        objParam[2] = new SqlParameter("@VatBilling", objSettings.VatBilling);
        objParam[3] = new SqlParameter("@Outstation", objSettings.Outstation);
        objParam[4] = new SqlParameter("@SaleTax", objSettings.SaleTax);
        objParam[5] = new SqlParameter("@Ret_Def_Series", objSettings.Ret_Def_Series);
        objParam[6] = new SqlParameter("@Vat_Def_Series", objSettings.Vat_Def_Series);
        objParam[7] = new SqlParameter("@CST_Def_Series", objSettings.CST_Def_Series);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@Default_Branch", objSettings.Default_Branch);
        objParam[10] = new SqlParameter("@Default_BranchId", objSettings.Default_BranchId);
        objParam[11] = new SqlParameter("@UserId", objSettings.UserId);
        
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertMastersettingBillTypeOptions", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


   

    public SqlDataReader GetMasterSettings(BillTypeSettings objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Default_BranchId", objSetting.Default_BranchId);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMastersettingBillType", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}