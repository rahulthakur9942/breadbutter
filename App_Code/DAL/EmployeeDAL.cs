﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for EmployeeDAL
/// </summary>
public class EmployeeDAL:Connection
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@posid",HttpContext.Current.Request.Cookies[Constants.posid].Value);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllEmployees", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public int Delete(Employees objEmployee)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Code", objEmployee.Code);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_EmployeeDeleteGetById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objEmployee.Code = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

    public Int16 InsertUpdate(Employees objEmployee)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[20];

        objParam[0] = new SqlParameter("@Code", objEmployee.Code);
        objParam[1] = new SqlParameter("@Name", objEmployee.Name);
        objParam[2] = new SqlParameter("@Address1", objEmployee.Address1);
        objParam[3] = new SqlParameter("@Address2", objEmployee.Address2);
        objParam[4] = new SqlParameter("@Address3", objEmployee.Address3);
        objParam[5] = new SqlParameter("@ContactNo", objEmployee.ContactNo);
        objParam[6] = new SqlParameter("@Remarks", objEmployee.Remarks);
        objParam[7] = new SqlParameter("@Auth", objEmployee.Auth);
       
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@UserId", objEmployee.UserId);
        objParam[10] = new SqlParameter("@IsActive", objEmployee.IsActive);
        objParam[11] = new SqlParameter("@AreaId", objEmployee.AreaId);
        objParam[12] = new SqlParameter("@CityId", objEmployee.CityId);
        objParam[13] = new SqlParameter("@StateId", objEmployee.StateId);
       
        objParam[14] = new SqlParameter("@BranchId", objEmployee.BranchId);
        objParam[15] = new SqlParameter("@KotPswrd", objEmployee.KotPswrd);
        objParam[16] = new SqlParameter("@ComplimentryValue", objEmployee.ComplimentryValue);
        objParam[17] = new SqlParameter("@Discount", objEmployee.Discount);
        objParam[18] = new SqlParameter("@Designation", objEmployee.Designation);
        objParam[19] = new SqlParameter("@IsDeliveryBoy", objEmployee.IsDeliveryBoy);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_EmployeeInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);
            objEmployee.Code = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public SqlDataReader GetById(Employees objEmployee)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Code", objEmployee.Code);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_EmployeeGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetCompByEmpId(Employees objEmployee)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@EmpId", objEmployee.Code);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetCompValue", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



  

}