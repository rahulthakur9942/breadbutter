﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AccountsDAL
/// </summary>
public class AccountsDAL:Connection
{
    
    public SqlDataReader GetByCODE(string CODE)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CODE", CODE);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsGetByCode", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public DataSet KeywordSearch(string Keyword,string Type,string AccountType,int GodownId,int BranchId)
    {
        int GstNo = 0;
        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@AccountType", AccountType);
        objParam[3] = new SqlParameter("@GodownId", GodownId);
        objParam[4] = new SqlParameter("@BranchId", BranchId);



        DataSet ds = null;
       
        DataSet Dts = new DataSet();
        try
        {
            DataTable dt = new DataTable();
            DataRow dtr = dt.NewRow();
            dt.Columns.Add("Customer_id");
            dt.Columns.Add("Prefix");
            dt.Columns.Add("Customer_Name");
            dt.Columns.Add("Address_1");
            dt.Columns.Add("Address_2");
            dt.Columns.Add("Area_ID");
            dt.Columns.Add("City_ID");
            dt.Columns.Add("State_ID");
            dt.Columns.Add("Date_of_Birth");
            dt.Columns.Add("Date_Anniversary");
            dt.Columns.Add("Discount");
            dt.Columns.Add("Contact_No");
            dt.Columns.Add("Tag");
            dt.Columns.Add("FocBill");
            dt.Columns.Add("grip");
            dt.Columns.Add("UserId");
            dt.Columns.Add("CreatedDate");
            dt.Columns.Add("ModifiedDate");
            dt.Columns.Add("IsActive");
            dt.Columns.Add("BranchId");
            dt.Columns.Add("EmailID");
            dt.Columns.Add("GSTNo");
            dt.Columns.Add("STATE_ID1");
            dt.Columns.Add("State_Name");
            dt.Columns.Add("stateint");
            dt.Columns.Add("statecode");
            dt.Columns.Add("State_Name1");
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsKeywordSearch", objParam);
           
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (ds.Tables[0].Rows[i].ItemArray[21].ToString() != null && ds.Tables[0].Rows[i].ItemArray[21].ToString() != "")
                {
                     GstNo = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[21].ToString().Length);
                    if (GstNo > 9)
                    {

                        dtr["Customer_id"] = ds.Tables[0].Rows[i].ItemArray[0];
                        dtr["Prefix"] = ds.Tables[0].Rows[i].ItemArray[1];
                        dtr["Customer_Name"] = ds.Tables[0].Rows[i].ItemArray[2];
                        dtr["Address_1"] = ds.Tables[0].Rows[i].ItemArray[3];
                        dtr["Address_2"] = ds.Tables[0].Rows[i].ItemArray[4];
                        dtr["Area_ID"] = ds.Tables[0].Rows[i].ItemArray[5];
                        dtr["City_ID"] = ds.Tables[0].Rows[i].ItemArray[6];
                        dtr["State_ID"] = ds.Tables[0].Rows[i].ItemArray[7];
                        dtr["Date_of_Birth"] = ds.Tables[0].Rows[i].ItemArray[8];
                        dtr["Date_Anniversary"] = ds.Tables[0].Rows[i].ItemArray[9];
                        dtr["Discount"] = ds.Tables[0].Rows[i].ItemArray[10];
                        dtr["Contact_No"] = ds.Tables[0].Rows[i].ItemArray[11];
                        dtr["Tag"] = ds.Tables[0].Rows[i].ItemArray[12];
                        dtr["FocBill"] = ds.Tables[0].Rows[i].ItemArray[13];
                        dtr["grip"] = ds.Tables[0].Rows[i].ItemArray[14];
                        dtr["UserId"] = ds.Tables[0].Rows[i].ItemArray[15];
                        dtr["CreatedDate"] = ds.Tables[0].Rows[i].ItemArray[16];
                        dtr["ModifiedDate"] = ds.Tables[0].Rows[i].ItemArray[17];
                        dtr["IsActive"] = ds.Tables[0].Rows[i].ItemArray[18];
                        dtr["BranchId"] = ds.Tables[0].Rows[i].ItemArray[19];
                        dtr["EmailID"] = ds.Tables[0].Rows[i].ItemArray[20];
                        dtr["GSTNo"] = ds.Tables[0].Rows[i].ItemArray[21];
                        //dtr["STATE_ID1"] = ds.Tables[0].Rows[i].ItemArray[22];
                        //dtr["State_Name"] = ds.Tables[0].Rows[i].ItemArray[23];
                        //dtr["stateint"] = ds.Tables[0].Rows[i].ItemArray[24];
                        //dtr["statecode"] = ds.Tables[0].Rows[i].ItemArray[25];
                        //dtr["State_Name1"] = ds.Tables[0].Rows[i].ItemArray[26];

                        // dt.Rows[i].ItemArray[27] = ds.Tables[0].Rows[i].ItemArray[27];
                        //  Dts = ds;
                        // ds;
                        dt.Rows.Add(dtr);

                    }
              

                }
                
               
            }
            Dts.Tables.Add(dt);
            
        }

        finally
        {
            objParam = null;
        }
        if (GstNo > 9)
        {
            return Dts;
        }
        else
        {
            return ds;

        }


    }




    public DataSet KeywordSearchSaleOnly(string Keyword, string Type, string AccountType, int GodownId,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@AccountType", AccountType);
        objParam[3] = new SqlParameter("@GodownId", GodownId);
        objParam[4] = new SqlParameter("@BranchId", BranchId);



        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsKeywordSearchSaleOnly", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public DataSet KeywordSearchRaw(string Keyword, string Type, string AccountType, int GodownId,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@AccountType", AccountType);
        objParam[3] = new SqlParameter("@GodownId", GodownId);
        objParam[4] = new SqlParameter("@BranchId", BranchId);



        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsKeywordSearchRaw", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }





    public DataSet GetAllDataSet()
    {
      
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }



    public DataSet KeywordSearchForDN(string Keyword, string Type, string AccountType, int GodownId, string ItemType,int BranchId,string Excise)
    {

        SqlParameter[] objParam = new SqlParameter[7];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@AccountType", AccountType);
        objParam[3] = new SqlParameter("@GodownId", GodownId);
        objParam[4] = new SqlParameter("@ItemType", ItemType);
        objParam[5] = new SqlParameter("@BranchId", BranchId);
        objParam[6] = new SqlParameter("@Excise", Excise);




        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsKeywordSearchForDN", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public DataSet KeywordSearchForStock(string Keyword, string Type, string AccountType, int GroupId, string ItemType, int GodownId,int branchId)
    {

        SqlParameter[] objParam = new SqlParameter[7];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@AccountType", AccountType);
        objParam[3] = new SqlParameter("@GroupId", GroupId);
        objParam[4] = new SqlParameter("@ItemType", ItemType);
        objParam[5] = new SqlParameter("@GodownId", GodownId);
        objParam[6] = new SqlParameter("@BranchId", branchId);




        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsKeywordSearchForStock", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public SqlDataReader GetAll()
    {
        List<Areas> AreaList = new List<Areas>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public int Delete(AccLedger objAccLedger)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@AccountId", objAccLedger.AccountId);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AccountDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objAccLedger.AccountId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }


    public Int16 InsertUpdate(AccLedger objAccLedgers)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[34];

        objParam[0] = new SqlParameter("@AccountId", objAccLedgers.AccountId);
        objParam[1] = new SqlParameter("@CODE", objAccLedgers.CODE);
        objParam[2] = new SqlParameter("@H_CODE", objAccLedgers.H_CODE);
        objParam[3] = new SqlParameter("@S_CODE", objAccLedgers.S_CODE);
        objParam[4] = new SqlParameter("@SS_CODE", objAccLedgers.SS_CODE);
        objParam[5] = new SqlParameter("@CNAME", objAccLedgers.CNAME);
        objParam[6] = new SqlParameter("@CADD1", objAccLedgers.CADD1);
        objParam[7] = new SqlParameter("@CADD2", objAccLedgers.CADD2);
        objParam[8] = new SqlParameter("@CITY_ID", objAccLedgers.CITY_ID);
        objParam[9] = new SqlParameter("@AREA_ID", objAccLedgers.AREA_ID);
        objParam[10] = new SqlParameter("@STATE_ID", objAccLedgers.STATE_ID);
        objParam[11] = new SqlParameter("@CST_NO", objAccLedgers.CST_NO);
        objParam[12] = new SqlParameter("@CST_DATE", objAccLedgers.CST_DATE);
        objParam[13] = new SqlParameter("@TINNO", objAccLedgers.TINNO);
        objParam[14] = new SqlParameter("@TOTNO", objAccLedgers.TOTNO);
        objParam[15] = new SqlParameter("@CR_LIMIT", objAccLedgers.CR_LIMIT );
        objParam[16] = new SqlParameter("@CR_DAYS", objAccLedgers.CR_DAYS);
        objParam[17] = new SqlParameter("@CONT_PER", objAccLedgers.CONT_PER);
        objParam[18] = new SqlParameter("@CONT_NO", objAccLedgers.CONT_NO);
        objParam[19] = new SqlParameter("@OP_BAL", objAccLedgers.OP_BAL);
        objParam[20] = new SqlParameter("@DR_CR", objAccLedgers.DR_CR);
        objParam[21] = new SqlParameter("@DIS_PER", objAccLedgers.DIS_PER);
        objParam[22] = new SqlParameter("@OS_BAL", objAccLedgers.OS_BAL);
        objParam[23] = new SqlParameter("@PURSALE_ACC_PERCENT", objAccLedgers.PURSALE_ACC_PERCENT);
        objParam[24] = new SqlParameter("@PURSALE_ACC_TYPE", objAccLedgers.PURSALE_ACC_TYPE);
        objParam[25] = new SqlParameter("@PREFIX", objAccLedgers.PREFIX);
        objParam[26] = new SqlParameter("@ACC_ZONE", objAccLedgers.ACC_ZONE);
        objParam[27] = new SqlParameter("@SRNO", objAccLedgers.SRNO);
        objParam[28] = new SqlParameter("@ShowInLedger", objAccLedgers.ShowInLedger);
        objParam[29] = new SqlParameter("@Narr", objAccLedgers.Narr);
        objParam[30] = new SqlParameter("@tAG", objAccLedgers.tAG);
        objParam[31] = new SqlParameter("@UserId", objAccLedgers.UserId);
        objParam[32] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[32].Direction = ParameterDirection.ReturnValue;

        objParam[33] = new SqlParameter("@BranchId", objAccLedgers.BranchId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_InsertUpdateCreditCustomer", objParam);
            retValue = Convert.ToInt16(objParam[32].Value);
            objAccLedgers.AccountId  = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetById(AccLedger objLedgers)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@AccountId", objLedgers.AccountId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAccledgerById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



}