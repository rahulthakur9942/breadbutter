﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for HeadingsDAL
/// </summary>
public class BreakageExpiryDAL:Connection
{



    public SqlDataReader GetByGodown(int godownId,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@GodownId", godownId);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_StockGetItemsInGodown", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetBreakageExpiryDetails(int BreakageId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BreakageId", BreakageId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_BreakageExpiryDetailGetByBreakageId", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_BreakageExpiryGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo,int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_BreakageExpiryGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;


    }
    public int RollBack(int BreakageId)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];



        objParam[0] = new SqlParameter("@BreakageId", BreakageId);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "salon_sp_BreakageExpiryRollBack", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }



    public int Update(StockExpiry objBreakageExpiry, DataTable dt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@DateOfBreakage", objBreakageExpiry.Ref_Date);
        objParam[1] = new SqlParameter("@GodownId", objBreakageExpiry.Godown_ID);


        objParam[2] = new SqlParameter("@BreakageDetails", dt);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@BreakageId",objBreakageExpiry.Ref_No);
        objParam[5] = new SqlParameter("@BranchId", objBreakageExpiry.BranchId);
        objParam[6] = new SqlParameter("@UserNo", objBreakageExpiry.UserNo);


        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_BreakageExpiryUpdate", objParam);
            retValue = Convert.ToInt32(objParam[3].Value);
            objBreakageExpiry.Ref_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }







    public int Insert(StockExpiry objBreakageExpiry, DataTable dt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];



        objParam[0] = new SqlParameter("@DateOfBreakage", objBreakageExpiry.Ref_Date);
        objParam[1] = new SqlParameter("@GodownId", objBreakageExpiry.Godown_ID);


        objParam[2] = new SqlParameter("@BreakageDetails", dt);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@BranchId", objBreakageExpiry.BranchId);
        objParam[5] = new SqlParameter("@UserNo", objBreakageExpiry.UserNo);



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_BreakageExpiryInsert", objParam);
            retValue = Convert.ToInt32(objParam[3].Value);
            objBreakageExpiry.Ref_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }


}