﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for StateDAL
/// </summary>
public class StateDAL:Connection
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_StatesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public Int16 InsertUpdate(States objState)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@State_ID", objState.State_ID);
        objParam[1] = new SqlParameter("@State_Name", objState.State_Name);
     
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@UserId", objState.UserId);
        objParam[4] = new SqlParameter("@IsActive", objState.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_StateInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objState.State_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public int Delete(States objState)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@State_ID", objState.State_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_StateDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objState.State_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

    public SqlDataReader GetById(States objState)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@State_ID", objState.State_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_StateGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

}