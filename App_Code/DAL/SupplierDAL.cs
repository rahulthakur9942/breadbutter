﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for SupplierDAL
/// </summary>
public class SupplierDAL:Connection
{
    public SqlDataReader GetById(Supplier objSupplier)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Supplier_ID", objSupplier.Supplier_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_SuppliersGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAll()
    {
        List<Supplier> SupplierList = new List<Supplier>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_SuppliersGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Supplier objSupplier,DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[19];

        objParam[0] = new SqlParameter("@Supplier_ID", objSupplier.Supplier_ID);
        objParam[1] = new SqlParameter("@Prefix", objSupplier.Prefix);
        objParam[2] = new SqlParameter("@Supplier_Name", objSupplier.Supplier_Name);
        objParam[3] = new SqlParameter("@Address_1", objSupplier.Address_1);
        objParam[4] = new SqlParameter("@Address_2", objSupplier.Address_2);
        objParam[5] = new SqlParameter("@Area_ID", objSupplier.Area_ID);
        objParam[6] = new SqlParameter("@City_ID", objSupplier.City_ID);
        objParam[7] = new SqlParameter("@State_ID", objSupplier.State_ID);
        objParam[8] = new SqlParameter("@CST_No", objSupplier.CST_No);
        objParam[9] = new SqlParameter("@CST_Date", objSupplier.CST_Date);
        objParam[10] = new SqlParameter("@TIN_No", objSupplier.TIN_No);
        objParam[11] = new SqlParameter("@TOT_No", objSupplier.TOT_No);
        objParam[12] = new SqlParameter("@Cont_Person", objSupplier.Cont_Person);
        objParam[13] = new SqlParameter("@Cont_No", objSupplier.Cont_No);
        objParam[14] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[14].Direction = ParameterDirection.ReturnValue;
        objParam[15] = new SqlParameter("@UserId", objSupplier.UserId);
        objParam[16] = new SqlParameter("@IsActive", objSupplier.IsActive);
        objParam[17] = new SqlParameter("@dt", dt);
        objParam[18] = new SqlParameter("@BranchId", objSupplier.BranchId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SupplierInserUpdate", objParam);
            retValue = Convert.ToInt16(objParam[14].Value);
            objSupplier.Supplier_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Supplier objSupplier)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Supplier_ID", objSupplier.Supplier_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SupplierDeleteGetById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objSupplier.Supplier_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}