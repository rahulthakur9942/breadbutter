﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for PayModeDAL
/// </summary>
public class PayModeDAL : Connection
{
    public SqlDataReader GetBillByBNF(string BNF)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BNF", BNF);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_GetAllBillsByBNF", ObjParam);
        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public Int32 ChangePaymode(string BillNowPrefix, string BillType, decimal CashAmount, decimal CrCardAmount, string Bank, string BankName, decimal CreditAmount, string CreditCustId, string CreditCustName, decimal OnlinePayment, int BranchId)
    {


        Int32 retval = 0;
        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[12];
        ObjParam[0] = new SqlParameter("@BillNowPrefix", BillNowPrefix);

        ObjParam[1] = new SqlParameter("@BillType", BillType);
        ObjParam[2] = new SqlParameter("@Cash_Amount", CashAmount);
        ObjParam[3] = new SqlParameter("@CrCard_Amount", CrCardAmount);
        ObjParam[4] = new SqlParameter("@Bank", Bank);
        ObjParam[5] = new SqlParameter("@BankName", BankName);
        ObjParam[7] = new SqlParameter("@Credit_Amount", CreditAmount);
        ObjParam[8] = new SqlParameter("@CreditId", CreditCustId);
        ObjParam[9] = new SqlParameter("@CreditCust", CreditCustName);
        ObjParam[10] = new SqlParameter("@OnlinePayment", OnlinePayment);
        ObjParam[11] = new SqlParameter("@BranchId", BranchId);
        ObjParam[6] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[6].Direction = ParameterDirection.ReturnValue;


        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure, "master_sp_ChangePaymode", ObjParam);
            retval = Convert.ToInt32(ObjParam[6].Value);

        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }


}