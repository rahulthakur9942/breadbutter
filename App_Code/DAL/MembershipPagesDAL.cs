﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for MembershipPagesDAL
/// </summary>
public class MembershipPagesDAL : Connection
{


    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_MembershipPagesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
 

    public int InsertUpdate(MembershipPages objMembershipPages,DataTable dt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];


        objParam[0] = new SqlParameter("@PagePrefix", objMembershipPages.PagePrefix);
        objParam[1] = new SqlParameter("@Title", objMembershipPages.Title);
        objParam[2] = new SqlParameter("@IsActive", objMembershipPages.IsActive);

        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@MembershipPageId",objMembershipPages.MembershipPageId);
        objParam[5] = new SqlParameter("@dtRoles", dt);

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_MembershipPagesInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[3].Value);
            objMembershipPages.MembershipPageId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    
      
       

    }
  

     

}