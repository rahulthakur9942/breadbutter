﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for kotDAL
/// </summary>
public class kotDAL
{
    public int companycustid { get; set; }
  
    public Int32 Insert(KOT objKOT, DataTable dt,int Userno)
    {
        Int32 retval = 0;
        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[23];
        ObjParam[0] = new SqlParameter("@MKOTNo", objKOT.MKOTNo);
        ObjParam[1] = new SqlParameter("@TableID", objKOT.TableID);
        ObjParam[2] = new SqlParameter("@PaxNo", objKOT.PaxNo);
        ObjParam[3] = new SqlParameter("@R_Code", objKOT.R_Code);
        ObjParam[4] = new SqlParameter("@M_Code", objKOT.M_Code);
        ObjParam[5] = new SqlParameter("@Value", objKOT.Value);
        ObjParam[6] = new SqlParameter("@DisPercentage", objKOT.DisPercentage);
        ObjParam[7] = new SqlParameter("@DisAmount", objKOT.DisAmount);
        ObjParam[8] = new SqlParameter("@ServiceCharges", objKOT.ServiceCharges);
        ObjParam[9] = new SqlParameter("@TaxAmount", objKOT.TaxAmount);
        ObjParam[10] = new SqlParameter("@TotalAmount", objKOT.TotalAmount);
        ObjParam[11] = new SqlParameter("@Complementary", objKOT.Complementary);
        ObjParam[12] = new SqlParameter("@Happy", objKOT.Happy);
        ObjParam[13] = new SqlParameter("@EmpCode", objKOT.EmpCode);
        ObjParam[14] = new SqlParameter("@BillNoWPrefix", objKOT.BillNoWPrefix);
        ObjParam[15] = new SqlParameter("@Pass", objKOT.Pass);
        ObjParam[16] = new SqlParameter("@BranchId", objKOT.BranchId);
        ObjParam[17] = new SqlParameter("@KOTDetail", dt);
        ObjParam[18] = new SqlParameter("@retval", SqlDbType.Decimal, 15);
        ObjParam[18].Direction = ParameterDirection.ReturnValue;
        ObjParam[19] = new SqlParameter("@KOTNo", objKOT.KOTNo);
        ObjParam[20] = new SqlParameter("@UserNo", Userno);
        ObjParam[21] = new SqlParameter("@TakeAway", objKOT.TakeAway);
        ObjParam[22] = new SqlParameter("@cstid",objKOT.cst_id);


        try
        {

            objKOT.KOTNo = Convert.ToInt32(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "pos_sp_insertkot", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam); 
            retval = Convert.ToInt32(ObjParam[18].Value);
            objKOT.KOTNo = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }



     public Int32 TransferTable(int FromTable,int ToTable,int BranchId,string qry)
    {
        Int32 retval = 0;
        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[5];
        ObjParam[0] = new SqlParameter("@FromTable",FromTable);
        ObjParam[1] = new SqlParameter("@ToTable", ToTable);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        ObjParam[3] = new SqlParameter("@qry", qry);
        ObjParam[4] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[4].Direction = ParameterDirection.ReturnValue;

        try
        {

            int retid = Convert.ToInt32(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "pos_sp_TransferTable", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[4].Value);
            retid = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }


    public SqlDataReader GetAll(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetKOTActiveTable", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetAllKot(int TableId,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@TableID", TableId);
        objParam[1] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetDetailByKotNo", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllKotByTable(int TableId, int BranchId)
     {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@TableNo", TableId);
        objParam[1] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetKotByTableNo", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetAllKOTDet(int TableId,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@TableID", TableId);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetDetailBtKotNoDet", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllKOTDetail(int TableId, int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@TableID", TableId);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetDetailBtKotNoDetail", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllBill(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetKOTActiveBill", objParam);
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByBillNowPrefix(string BillNowPrefix)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BillNowPrefix", BillNowPrefix);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "settlement_sp_GetDetailByBillNowPrefix", objParam);
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int32 BillSettlement(Bill objbill)
    {
        Int32 retval = 0;
        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[12];
        ObjParam[0] = new SqlParameter("@BillNowPrefix", objbill.BillNowPrefix);
        ObjParam[1] = new SqlParameter("@CashAmt", objbill.Cash_Amount);
        ObjParam[2] = new SqlParameter("@CreditAmt", objbill.Credit_Amount);
        ObjParam[3] = new SqlParameter("@CrCardAmt", objbill.CrCard_Amount);
        ObjParam[4] = new SqlParameter("@CreditBank", objbill.CreditBank);
        ObjParam[5] = new SqlParameter("@CustomerId", objbill.Customer_ID);
        ObjParam[6] = new SqlParameter("@CustomerName", objbill.Customer_Name);
        ObjParam[8] = new SqlParameter("@OnlineAmt", objbill.OnlinePayment);
        ObjParam[9] = new SqlParameter("@BillMode", objbill.BillMode);
        ObjParam[10] = new SqlParameter("@CashCustCode", objbill.CashCust_Code);
        ObjParam[11] = new SqlParameter("@CashCustName", objbill.CashCust_Name);
        ObjParam[7] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[7].Direction = ParameterDirection.ReturnValue;
     

        try
        {

            objbill.Bill_No = Convert.ToInt32(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "pos_sp_billSettlement", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[7].Value);
            objbill.Bill_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }


    public Int32 UpdateKOTDeActive(int TableID, int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@TableID", TableID);
        Int32 retval = 0;
        try
        {
            SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_UpdateKOTDeActive", objParam);
        }
        finally
        {
            objParam = null;
        }
        return retval;
    }
    
    public Int32 Insert(Bill objBill, DataTable dt, DataTable dt1)
    {

       
        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[43];
        ObjParam[0] = new SqlParameter("@Customer_ID", objBill.Customer_ID);
        ObjParam[1] = new SqlParameter("@Bill_Value", objBill.Bill_Value);
        ObjParam[2] = new SqlParameter("@DiscountPer", objBill.DiscountPer);
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objBill.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@BillMode", objBill.BillMode);
        ObjParam[7] = new SqlParameter("@CreditBank", objBill.CreditBank);
        ObjParam[8] = new SqlParameter("@UserNO", objBill.UserNO);
        ObjParam[9] = new SqlParameter("@Bill_Type", objBill.Bill_Type);
        ObjParam[10] = new SqlParameter("@Cash_Amount", objBill.Cash_Amount);
        ObjParam[11] = new SqlParameter("@Credit_Amount", objBill.Credit_Amount);
        ObjParam[12] = new SqlParameter("@CrCard_Amount", objBill.CrCard_Amount);
        ObjParam[13] = new SqlParameter("@Round_Amount", objBill.Round_Amount);
        ObjParam[14] = new SqlParameter("@Bill_Printed", objBill.Bill_Printed);
        ObjParam[15] = new SqlParameter("@Passing", objBill.Passing);
        ObjParam[16] = new SqlParameter("@CashCust_Code", objBill.CashCust_Code);
        ObjParam[17] = new SqlParameter("@CashCust_Name", objBill.CashCust_Name);
        ObjParam[18] = new SqlParameter("@Tax_Per", objBill.Tax_Per);
        ObjParam[19] = new SqlParameter("@R_amount", objBill.R_amount);
        ObjParam[20] = new SqlParameter("@tableno", objBill.tableno);
        ObjParam[21] = new SqlParameter("@remarks", objBill.remarks);
        ObjParam[22] = new SqlParameter("@servalue", objBill.servalue);
        ObjParam[23] = new SqlParameter("@ReceiviedGRNNo", objBill.ReceiviedGRNNo);
        ObjParam[24] = new SqlParameter("@EmpCode", objBill.EmpCode);
        ObjParam[25] = new SqlParameter("@OrderNo", objBill.OrderNo);
        ObjParam[26] = new SqlParameter("@BillDetail", dt);
        ObjParam[27] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@CustomerName", objBill.Customer_Name);
        ObjParam[30] = new SqlParameter("@BranchId", objBill.BranchId);
        ObjParam[31] = new SqlParameter("@OnlinePayment", objBill.OnlinePayment);
        ObjParam[32] = new SqlParameter("@DeliveryCharges", objBill.DeliveryCharges);
        ObjParam[33] = new SqlParameter("@KKCPer", objBill.KKCPer);
        ObjParam[34] = new SqlParameter("@KKCAmt", objBill.KKCAmt);
        ObjParam[35] = new SqlParameter("@SBCPer", objBill.SBCPer);
        ObjParam[36] = new SqlParameter("@SBCAmt", objBill.SBCAmt);
        ObjParam[37] = new SqlParameter("@BillRemarks", objBill.BillRemarks);
        ObjParam[38] = new SqlParameter("@cst_id", HttpContext.Current.Session["cst_id"]);
        ObjParam[39] = new SqlParameter("@postype", HttpContext.Current.Request.Cookies[Constants.posid].Value);
        ObjParam[40] = new SqlParameter("@Order_No", objBill.Order_No);
        ObjParam[41] = new SqlParameter("@OTP", objBill.OTP);
        ObjParam[42] = new SqlParameter("@RdoBillMode", objBill.RdoPaymode);

        try
        {

            objBill.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBillFromKot", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBill.Bill_No = retval;
        }
        finally
        {
            HttpContext.Current.Session.Remove("postype");
            HttpContext.Current.Session.Remove("cst_id");
            ObjParam = null;

        }
        HttpContext.Current.Session.Remove("postype");
        HttpContext.Current.Session.Remove("cst_id");
        return retval ;
      
    }

    public SqlDataReader KOTprint(int UserNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        SqlDataReader dr = null;
        objParam[0] = new SqlParameter("@UserNo", UserNo);

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_PrintKOTFromKot", objParam);

        }    
        finally
        {
            objParam = null;
        }

        return dr;
    }


    public int CancelKOt(KOT objKOt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@KOTNo", objKOt.KOTNo);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "pos_sp_cancelkot", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objKOt.KOTNo = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }


    public SqlDataReader GetAllKotforcancel(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetAllkot", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetByKotNo(int KotNo)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@KotNo", KotNo);
      
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetByKotNo", objParam);

        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int32 EmployeeLoginCheck(Employees objEmployee, int BranchId)
    {
        Int32 retVal = 0;

        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@Name", objEmployee.Name);
        //objParam[1] = new SqlParameter("@Password", HashSHA1(objReg.Password));
        objParam[1] = new SqlParameter("@KotPswrd", objEmployee.KotPswrd);
        objParam[2] = new SqlParameter("@retval", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@BranchId", BranchId);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "master_sp_EmployeeLoginCheck", objParam);
            retVal = Convert.ToInt32(objParam[2].Value);

        }

        finally
        {
            objParam = null;
        }


        return retVal;
    }
	
	  public int DeleteKot(string ProductCode , decimal KotNo,int Userid)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@ProductCode", ProductCode);
        objParam[1] = new SqlParameter("@KotNo", KotNo);
        objParam[2] = new SqlParameter("@Userid", Userid);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "pos_sp_ModifyKot", objParam);
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}