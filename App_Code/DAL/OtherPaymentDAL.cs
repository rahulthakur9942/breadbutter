﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
/// <summary>
/// Summary description for OtherPaymentDAL
/// </summary>
public class OtherPaymentDAL:Connection
{
    public SqlDataReader GetById(OtherPayment objOtherPayment)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@OtherPayment_ID", objOtherPayment);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "sp_OtherPaymentModeGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_OtherPaymentModeGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(OtherPayment objOtherPayment)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@OtherPayment_ID", objOtherPayment.OtherPayment_ID);
        objParam[1] = new SqlParameter("@OtherPayment_Name", objOtherPayment.OtherPayment_Name);
       
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@UserId", objOtherPayment.UserId);
        objParam[4] = new SqlParameter("@IsActive", objOtherPayment.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "sp_OtherPaymentModeInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objOtherPayment.OtherPayment_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(OtherPayment objOtherPayment)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@OtherPayment_ID", objOtherPayment.OtherPayment_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "sp_OtherPaymentModeDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objOtherPayment.OtherPayment_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}