﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for HappyHourDAL
/// </summary>
public class HappyHourDAL:Connection
{
    


     public SqlDataReader GetHappyHours(HappyHours objHappyHour)
    {
        SqlParameter[] objParam = new SqlParameter[0];



        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetHappyHours", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 InsertUpdate(HappyHours objHappyHour,DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[9];

        objParam[0] = new SqlParameter("@Id", objHappyHour.Id);
        objParam[1] = new SqlParameter("@CategoryId", objHappyHour.CategoryId);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@TimeFrom", objHappyHour.TimeFrom);
        objParam[4] = new SqlParameter("@TimeTo", objHappyHour.TimeTo);
        objParam[5] = new SqlParameter("@IsActive", objHappyHour.IsActive);
        objParam[6] = new SqlParameter("@UserId", objHappyHour.UserId);

        objParam[7] = new SqlParameter("@BranchId", objHappyHour.BranchId);
        objParam[8] = new SqlParameter("@IDDetail", dt);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_InsertUpdateHappyHours", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objHappyHour.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(HappyHours objHappyHour)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Id", objHappyHour.Id);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_HappyHourDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objHappyHour.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}