﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseReceiptSettings
/// </summary>
public class PurchaseReceiptSettings
{
    public bool Enable_Excise { get; set; }
    public bool UPDATE_SALERATE { get; set; }
    public bool Margine { get; set; }
    public bool ShowSaleOnly { get; set; }
    public bool RpCode { get; set; }
    public bool UPdate_mrp { get; set; }
    public int UserId { get; set; }
	public PurchaseReceiptSettings()
	{
        Enable_Excise = false;
        UPDATE_SALERATE = false;
        Margine = false;
        ShowSaleOnly = false;
        RpCode = false;
        UPdate_mrp = false;
        UserId = 0;
	}
}