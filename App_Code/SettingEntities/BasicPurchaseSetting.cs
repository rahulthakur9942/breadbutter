﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasicPurchaseSetting
/// </summary>
public class BasicPurchaseSetting
{
    public bool Check_MRP { get; set; }
    public bool Update_SaleRatePur { get; set; }
    public bool Update_MRPPur { get; set; }
    public bool Show_MarginPur { get; set; }
    public bool Show_SaleItemInPurOrder { get; set; }
    public string ReOrderLevel { get; set; }
    public string ReOrderLevelCal { get; set; }
    public string Defaultgodown { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }

	public BasicPurchaseSetting()
	{
		Check_MRP = false; 
        Update_SaleRatePur = false;
        Update_MRPPur=false;
        Show_MarginPur=false; 
        Show_SaleItemInPurOrder=false;
        ReOrderLevel= "";
        ReOrderLevelCal="";
        Defaultgodown="";
        UserId = 0;
        BranchId = 0;
	}
}