﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StockExpiry
/// </summary>
public class StockExpiry
{

    public int Ref_No { get; set; }
    public DateTime Ref_Date { get; set; }
    public string strBreakageDate { get { return Ref_Date.ToString("d"); } }
    public string Code { get; set; }
    public string IName { get; set; }
    public decimal MRP { get; set; }
    public decimal ActualStock { get; set; }
    public decimal StockExpired { get; set; }
    public int UserNo { get; set; }
    public string Unit { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public int Godown_ID { get; set; }
    public string master_code { get; set; }
    public decimal qty_to_less { get; set; }
    public decimal Crstock { get; set; }
    public string Godown { get; set; }
    public int BranchId { get; set; }
	public StockExpiry()
	{
        Ref_No = 0;
        Ref_Date = DateTime.Now;
        Code = string.Empty;
        IName = string.Empty;
        MRP = 0;
        ActualStock = 0;
        StockExpired = 0;
        UserNo = 0;
        Unit = string.Empty;
        Rate = 0;
        Amount = 0;
        Godown_ID = 0;
        master_code = string.Empty;
        qty_to_less = 0;
        Crstock =0;
        Godown = string.Empty;
        BranchId = 0;

	}
}