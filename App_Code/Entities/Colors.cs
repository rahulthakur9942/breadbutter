﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Colors
/// </summary>
public class Colors
{

    public int Color_ID { get; set; }
    public string Color_Name { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
	public Colors()
	{
        Color_ID = -1;
        Color_Name = "";
        UserId = 0;
        IsActive = false;
	}
}