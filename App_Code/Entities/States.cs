﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for States
/// </summary>
public class States
{
    public int State_ID { get; set; }
    public string State_Name { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }

	public States()
	{
        State_ID = -1;
        State_Name = "";
        IsActive = false;
        UserId = 0;
	}
}