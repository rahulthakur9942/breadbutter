﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseDetail
/// </summary>
public class PurchaseDetail
{
      
    public int PurchaseDetailId { get; set; }
    public int GrnNo { get; set; }
    public DateTime GrnDate { get; set; }
    public string OrderNo { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public string Unit { get; set; }
    public decimal CaseQty { get; set; }
    public decimal QtyInCase { get; set; }
    public decimal Qty { get; set; }
    public bool Scheme { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public decimal Dis1P { get; set; }
    public decimal Dis2P { get; set; }
    public decimal TaxP { get; set; }
    public decimal Dis1Amt { get; set; }
    public decimal Dis2Amt { get; set; }
    public decimal TaxAmt { get; set; }
    public decimal Dis3P { get; set; }
    public decimal Dis3Amt { get; set; }
    public int GodownId { get; set; }
    public decimal ExciseP { get; set; }
    public decimal ExciseAmt { get; set; }
    public int RowNum { get; set; }
    public int Free { get; set; }
    public decimal SaleRate { get; set; }
    public decimal Item_Margin { get; set; }
    public decimal ItemBasRateWTax { get; set; }
    public decimal surval { get; set; }
    public PurchaseDetail()
    {
        PurchaseDetailId = 0;
        GrnNo = 0; GrnDate = DateTime.Now; OrderNo = string.Empty; ItemCode = string.Empty; Unit = string.Empty;
        CaseQty = 0; QtyInCase = 0; Qty = 0; Scheme = false; MRP = 0; Rate = 0; Amount = 0; Dis1P = 0; Dis2P = 0; TaxP = 0;
        Dis1Amt = 0; Dis2Amt = 0; TaxAmt = 0; GodownId = 0; RowNum = 0; Free = 0; SaleRate = 0;
        ItemName = string.Empty;
        Dis3P = 0;
        Dis3Amt = 0;
        ExciseP = 0;
        ExciseAmt = 0;
        Item_Margin = 0;
        ItemBasRateWTax = 0;
        surval = 0;
    }
}