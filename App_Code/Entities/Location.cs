﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Location
/// </summary>
public class Location
{

    public int Location_ID { get; set; }
    public string Location_Name { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
	public Location()
	{
        Location_ID = -1;
        Location_Name = "";
        UserId = 0;
        IsActive = false;
	}
}