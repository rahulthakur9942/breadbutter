﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Processing
/// </summary>
public class Processing
{


    public Int64 ProcessingId { get; set; }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public string ProcessingTime { get; set; }
    public string Status { get; set; }
    public string TableNo { get;  set; }
    public string ImageUrl { get; set; }
    public bool IsStarted { get {return Status == "Started" ? true : false; } }
    public bool IsNew { get { return Status == "New" ? true : false; } }

    public bool IsActive { get; set; }


    public bool IsDelivered { get { return Status == "Delivered" ? true : false; } }
    public bool IsNewDelivery { get { return Status == "Processed" ? true : false; } }





    public bool IsNewOrStarted { get { return (Status == "New" || Status=="Started") ? true : false; } }

    public string ImagePath { get { return Status == "Delivered" ? "images/delivered.png" : (Status == "New" || Status == "Started") ? "images/processing.png" : "images/deliver.png"; } }

    public string DeliveryButtonText { get { return Status == "Delivered" ? "Delivered" :(Status=="New" || Status=="Started")? "Processing": "Deliver Now"; } }
    
    public string RelatedItems { get; set; }
    public string BillNowPrefix { get; set; }

    public string KotImagePath { get { return Status == "New" ? "images/startp.png" : "images/stop.png"; } }
    public bool ProcessedStatus { get { return (Status == "Processed" || Status == "Delivered") ? true : false; } }

    public string Remarks { get; set; }
    public string ButtonText { get { return Status == "New" ? "Start" : "Started"; } }
	public Processing()
	{
        Remarks = string.Empty;
        RelatedItems = string.Empty;
        TableNo = string.Empty;
        ProcessingId = 0;
        Item_Code = "";
        Item_Name = "";
        ProcessingTime = string.Empty;
        Status = "New";
        ImageUrl = string.Empty;
        BillNowPrefix = string.Empty;
        IsActive = false;
     
	}
}