﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OtherPaymentMode
/// </summary>
public class OtherPaymentMode
{


    public int OtherPayment_ID { get; set; }
    public int PaymetMode { get; set; }
    public string OtherPayment_Name { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }

	
	public OtherPaymentMode()
	{
        OtherPayment_ID = 0;
        OtherPayment_Name = "";
        PaymetMode = 0;
        UserId = 0;
        IsActive = false;
	}
}