﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KotDetail
/// </summary>
public class KotDetail
{
 
    public decimal KOTNo { get; set; }
    public int TableNo { get; set; }
    public string ProductCode { get; set; }
    public decimal SaleRate { get; set; }
    public decimal Qty { get; set; }
    public decimal Amount { get; set; }
    public decimal DisPercentage { get; set; }
    public decimal DisAmount { get; set; }
    public decimal TaxCode { get; set; }
    public decimal TaxAmount { get; set; }
    public DateTime ModifiedDate { get; set; }
    public bool Happy { get; set; }
    public int Rcode { get; set; }
    public int BranchId { get; set; }
    public int ItemID { get; set; }
    public string ItemName { get; set; }
    public decimal Tax_ID { get; set; }
    public string AddOn { get; set; }
    public int PaxNo { get; set; }
    public int EmpCode { get; set; }
    public bool TakeAway { get; set; }
	public KotDetail()
	{
        KOTNo = 0;
        ProductCode = string.Empty;
        SaleRate = 0;
        Qty = 0;
        Amount = 0;
        DisPercentage = 0;
        DisAmount = 0;
        TaxCode = 0;
        TaxAmount = 0;
        ModifiedDate = DateTime.Now;
        Happy = false;
        Rcode = 0;
        BranchId = 0;
        ItemID = 0;
        ItemName = string.Empty;
        Tax_ID = 0;
        AddOn = string.Empty;
        PaxNo = 0;
        EmpCode = 0;
        TableNo = 0;
        TakeAway = false;
	}
}