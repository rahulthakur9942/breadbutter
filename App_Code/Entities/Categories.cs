﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Headings
/// </summary>
public class Categories
{

    
    
    public int CategoryId{ get; set; }
    public string Title{ get; set; } 
    public bool IsActive{ get; set; }


    public Categories()
	{
        
        CategoryId = 0;
        Title = string.Empty;
         IsActive = true;
    }
}