﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Designations
/// </summary>
public class Designations
{
    public int DesgID { get; set; }
    public string DesgName { get; set; }
    public bool IsActive { get; set; }
    public int UserId { get; set; }
	public Designations()
	{
        DesgID = 0;
        DesgName = "";
        IsActive = false;
        UserId = 0;
        

	}
}