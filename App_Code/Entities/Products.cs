﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Products
/// </summary>
public class Product
{
    public int ItemID { get; set; }
    public string ItemImage { get; set; }
    public string Item_Name { get; set; }
    public string Item_Code { get; set; }
    public decimal Sale_Rate { get; set; }
    public decimal Sale_Rate_Excl { get; set; }
    public decimal Tax_Code { get; set; }
    public decimal Tax_ID{ get; set; }
    public decimal SurVal { get; set; }
    public int Likes { get; set; }
    public decimal Qty { get; set; }
    public decimal SubTotal { get; set; }
    public string Remarks { get; set; }
    public string Item_Remarks { get; set; }
    public decimal Discount { get; set; }
    public bool Edit_SaleRate { get; set; }
    public decimal DeliveryCharges { get; set; }
    public decimal comdata { get; set; }
    public decimal KKCPer { get; set; }
    public decimal KKCAmt { get; set; }
    public decimal SBCPer { get; set; }
    public decimal SBCAmt { get; set; }
    public string BillRemarks { get; set; }
    public int Tableno { get; set; }
    public string BillMode { get; set; }
    public int EmpCode { get; set; }
    public decimal Max_Retail_Price { get; set; }

    public string  CursorOn { get; set; }
	public Product()
	{
        Max_Retail_Price = 0;
        ItemID = 0;
        ItemImage = string.Empty;
        Item_Name = string.Empty;
        Item_Code = string.Empty;
        Sale_Rate = 0;
        Tax_Code = 0;
        Tax_ID = 0;
        SurVal = 0;
        Likes = 0;
        SubTotal = 0;
        Qty = 1;
        Remarks = string.Empty;
        Item_Remarks = string.Empty;
        Discount = 0;
        Edit_SaleRate = false;
        DeliveryCharges = 0;
        KKCAmt = 0;
        KKCPer = 0;
        SBCPer = 0;
        SBCAmt = 0;
        BillRemarks = "";
        Tableno = 0;
        EmpCode = 0;
        BillMode = "";
        CursorOn = "";
  


    }
}