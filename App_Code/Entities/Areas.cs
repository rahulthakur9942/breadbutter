﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Areas
/// </summary>
public class Areas
{

    public int Area_ID { get; set; }
    public int UserId { get; set; }
    public string Area_Name { get; set; }
    public bool IsActive { get; set; }
	public Areas()
	{
        Area_ID = -1;
        Area_Name = "";
        UserId = 0;
        IsActive = true;
	}
}