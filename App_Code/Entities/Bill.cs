﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Bill
/// </summary>
public class Bill
{
    public int ID { get; set; }
    public string Bill_Prefix { get; set; }
    public int Bill_No { get; set; }
    public string BillNowPrefix { get; set; }
    public DateTime Bill_Date { get; set; }
    public string Customer_ID { get; set; }
    public string Customer_Name { get; set; }
    public decimal Bill_Value { get; set; }
    public decimal DiscountPer { get; set; }
    public decimal Less_Dis_Amount { get; set; }
    public decimal Add_Tax_Amount { get; set; }
    public decimal Net_Amount { get; set; }
    public string BillMode { get; set; }
    public string CreditBank { get; set; }
    public int UserNO { get; set; }
    public int Bill_Type { get; set; }
    public decimal Cash_Amount { get; set; }
    public decimal Credit_Amount { get; set; }
    public decimal CrCard_Amount { get; set; }
    public decimal Round_Amount { get; set; }
    public bool Bill_Printed { get; set; }
    public bool Passing { get; set; }
    public int CashCust_Code { get; set; }
    public string CashCust_Name { get; set; }
    public decimal Tax_Per { get; set; }
    public int Godown_ID { get; set; }
    public DateTime ModifiedDate { get; set; }
    public decimal R_amount { get; set; }
    public int tokenno { get; set; }
    public int tableno { get; set; }
    public string remarks { get; set; }
    public decimal servalue { get; set; }
    public int ReceiviedGRNNo { get; set; }
    public int EmpCode { get; set; }
    public int OrderNo { get; set; }
    public int BranchId { get; set; }
    public bool Surval { get; set; }
    public string strBD { get { return Bill_Date.ToString("d"); } }
    public string strMD { get { return ModifiedDate.ToString("d"); } }

    public string CashCustAddress { get; set; }
    public string CreditCustAddress { get; set; }
    public string BillType { get; set; }
    public decimal OnlinePayment { get; set; }
    public decimal DeliveryCharges { get; set; }
    public decimal KKCPer { get; set; }
    public decimal KKCAmt { get; set; }
    public decimal SBCPer { get; set; }
    public decimal SBCAmt { get; set; }
    public string BillRemarks { get; set; }
	public string EmpName { get; set; }
    public string UserName { get; set; }
    public string Time { get; set; }
    public int OtherPayment_ID { get; set; }
    public string CoupanNo { get; set; }

    public decimal CouponAmt { get; set; }
    public string Mode { get; set; }
    public decimal CashAmt { get; set; }
    public decimal OnlineAmt { get; set; }
    public int cst_id { get; set; }
    public string Order_No { get; set; }
    public string cst_name { get; set; }
    public int posid { get; set; }
    public string OTP { get; set; }
    public string RdoPaymode { get; set; }



    public Bill()
    {
        ID = 0;
        CashCustAddress = string.Empty;
        CreditCustAddress = string.Empty;
        OtherPayment_ID = 0;
        CoupanNo = "";
        Bill_Prefix = "";
        Bill_No = 0;
        BillNowPrefix = "";
        Bill_Date = DateTime.Now;
        Customer_ID = "";
        Customer_Name = "";
        Bill_Value = 0;
        DiscountPer = 0;
        Less_Dis_Amount = 0;
        Add_Tax_Amount = 0;
        Net_Amount = 0;
        BillMode = "";
        CreditBank = "";
        UserNO = 0;
        Bill_Type = 0;
        Cash_Amount = 0;
        Credit_Amount = 0;
        CrCard_Amount = 0;
        Round_Amount = 0;
        Bill_Printed = false;
        Passing = false;
        CashCust_Code = 0;
        CashCust_Name = "";
        Tax_Per = 0;
        Godown_ID = 0;
        ModifiedDate = DateTime.Now;
        R_amount = 0;
        tokenno = 0;
        tableno = 0;
        remarks = "";
        servalue = 0;
        ReceiviedGRNNo = 0;
        EmpCode = 0;
        BranchId = 0;
        BillType = "";
        OnlinePayment = 0;
        DeliveryCharges = 0;
        KKCAmt = 0;
        KKCPer = 0;
        SBCPer = 0;
        SBCAmt = 0;
        BillRemarks = "";
		EmpName = "";
        UserName = "";
        Time = "";
        CouponAmt = 0;
        Mode = "";
        CashAmt = 0;
        OnlineAmt = 0;
        cst_id = 0;
        Order_No = "";
        cst_name = "";
        posid = 0;
        OTP = "";
        RdoPaymode = "";

    }
}