﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for StockAdjustmentBLL
/// </summary>
public class DailyTransferBLL
{


    public List<DailyTransfer> GetByRefNo(int RefNo,int BranchId)
    {
        List<DailyTransfer> TransferList = new List<DailyTransfer>();
        SqlDataReader dr = new DailyTransferDAL().GetByRefNo(RefNo,BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DailyTransfer objDailyTransfer = new DailyTransfer()
                    {
                        RefNo = Convert.ToInt32(dr["RefNo"]),
                        Date = Convert.ToString(dr["Date"]),
                        Amount=Convert.ToDecimal(dr["Amount"]),
                        MRP = Convert.ToDecimal(dr["MRP"]),
                        Rate = Convert.ToDecimal(dr["Rate"]),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        Stock = Convert.ToDecimal(dr["Stock"]),
                        Item_Code =dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),

                        
                    };
                    TransferList.Add(objDailyTransfer);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }

    public List<DailyTransfer> GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        List<DailyTransfer> TransferList = new List<DailyTransfer>();
        SqlDataReader dr = new DailyTransferDAL().GetByDate(DateFrom, DateTo, BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DailyTransfer objDailyTransfer = new DailyTransfer()
                    {
                        RefNo = Convert.ToInt32(dr["RefNo"]),
                        Date = Convert.ToString(dr["Date"]),
                      Godown_ID=Convert.ToInt32(dr["Godown_ID"]),

                    };
                    TransferList.Add(objDailyTransfer);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }


    public int Delete(int RefNo,int BranchId)
    {

        return new DailyTransferDAL().Delete(RefNo,BranchId);
    }

    public int InsertUpdate(DateTime Date, Int32 GodownId, int BranchId, int RefNo, DataTable dt, int UserNo)
    {

        return new DailyTransferDAL().InsertUpdate(Date,GodownId,BranchId,RefNo,dt,UserNo);
    }
}