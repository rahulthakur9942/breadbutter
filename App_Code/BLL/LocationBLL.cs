﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for LocationBLL
/// </summary>
public class LocationBLL
{
    public Int32 DeleteLocation(Location objlocation)
    {
        return new LocationDAL().Delete(objlocation);
    }
    public void GetById(Location objLocation)
    {
       
        SqlDataReader dr = null;
        try
        {
            dr = new LocationDAL().GetById(objLocation);
            if (dr.HasRows)
            {
                dr.Read();
                
                     
                 objLocation.Location_Name = dr["Location_Name"].ToString();
                 objLocation.Location_ID = Convert.ToInt16(dr["Location_ID"]);
                 objLocation.UserId = Convert.ToInt16(dr["UserId"]);
                 objLocation.IsActive = Convert.ToBoolean(dr["IsActive"]);
                 
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
         

    }






    public List<Location> GetAll()
    {
        List<Location> LocationList = new List<Location>();

        SqlDataReader dr = null;
        try
        {
            dr = new LocationDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Location objLocation = new Location()
                    {
                        Location_Name = dr["Location_Name"].ToString(),

                        Location_ID = Convert.ToInt16(dr["Location_ID"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    LocationList.Add(objLocation);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return LocationList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new LocationDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Location_ID"].ToString(), dr["Location_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Location objLocation)
    {

        return new LocationDAL().InsertUpdate(objLocation);
    }

}