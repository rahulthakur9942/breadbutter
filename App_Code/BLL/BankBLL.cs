﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for BankBLL
/// </summary>
public class BankBLL
{
    public Int32 DeleteBank(Banks objBank)
    {
        return new BankDAL().Delete(objBank);
    }



    public void GetById(Banks objBank)
    {
     
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new BankDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                     
                       objBank.Bank_Name = dr["Bank_Name"].ToString();
                        objBank.CCODE = dr["CCODE"].ToString();
                        objBank.Bank_ID = Convert.ToInt16(dr["Bank_ID"]);
                        objBank.UserId = Convert.ToInt16(dr["UserId"]);
                        objBank.IsActive = Convert.ToBoolean(dr["IsActive"]);
              
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
       

    }




    public List<Banks> GetAll()
    {
        List<Banks> BankList = new List<Banks>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new BankDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Banks objBanks = new Banks()
                    {
                        Bank_Name = dr["Bank_Name"].ToString(),
                        CCODE = dr["CCODE"].ToString(),
                        Bank_ID = Convert.ToInt16(dr["Bank_ID"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                    };
                    BankList.Add(objBanks);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return BankList;

    }




    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new BankDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Bank_ID"].ToString(), dr["Bank_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public Int16 InsertUpdate(Banks objBank)
    {

        return new BankDAL().InsertUpdate(objBank);
    }
}