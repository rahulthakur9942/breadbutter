﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for KotPrintBLL
/// </summary>
public class KotPrintBLL
{
    public List<KotPrint> GetDataByUserNo(int UserNo)
    {
        List<KotPrint> KotList = new List<KotPrint>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new KotPrintDAL().KOTprint(UserNo);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KotPrint objKot = new KotPrint()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                        DepartmentName = Convert.ToString(dr["DepartmentName"]),

                      
                    };


                    KotList.Add(objKot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return KotList;

    }
}