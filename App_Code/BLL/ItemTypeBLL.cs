﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for ItemTypeBLL
/// </summary>
public class ItemTypeBLL
{


    public Int32 DeleteItemType(ItemTypes objItemType)
    {
        return new ItemTypeDAL().Delete(objItemType);
    }

    public void GetById(ItemTypes objItemType)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new ItemTypeDAL().GetById(objItemType);
            if (dr.HasRows)
            {
                dr.Read();


                objItemType.ItemType_Name = dr["ItemType_Name"].ToString();
                objItemType.ItemType_Id = Convert.ToInt16(dr["ItemType_Id"]);



            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }






    public List<ItemTypes> GetAll()
    {
        List<ItemTypes> ItemTypesList = new List<ItemTypes>();

        SqlDataReader dr = null;
        try
        {
            dr = new ItemTypeDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ItemTypes objItemTypes = new ItemTypes()
                    {
                        ItemType_Name = dr["ItemType_Name"].ToString(),

                        ItemType_Id = Convert.ToInt16(dr["ItemType_Id"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    ItemTypesList.Add(objItemTypes);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return ItemTypesList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ItemTypeDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["ItemType_Id"].ToString(), dr["ItemType_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(ItemTypes objItemTypes)
    {

        return new ItemTypeDAL().InsertUpdate(objItemTypes);
    }
}