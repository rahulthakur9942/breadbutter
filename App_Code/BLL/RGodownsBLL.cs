﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for RRGodownsBLL
/// </summary>
/// 

public class ItemType
{
    public string Item_TypeID { get; set; }
    public string Item_Type { get; set; }
}

//public class Company
//{
//    public Int32 Company_ID { get; set; }
//    public string Company_Name { get; set; }
//}

public class Branch
{
    public Int32 ID { get; set; }
    public string Name { get; set; }
}


public class Products
{
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
}

public class Department
{
    public Int32 Prop_Id { get; set; }
    public string Prop_Name { get; set; }
}

//public class Group
//{
//    public Int32 Group_Id { get; set; }
//    public string Group_Name { get; set; }
//}

//public class SubGroup
//{
//    public Int32 SGroup_Id { get; set; }
//    public string SGroup_Name { get; set; }
//}
public class RGodownsBLL
{


    public List<RGodowns> GetAll()
    {
        List<RGodowns> GodownList = new List<RGodowns>();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    RGodowns objRGodowns = new RGodowns()
                    {
                        Godown_Name = dr["Godown_Name"].ToString(),
                        Divison = Convert.ToString(dr["Divison"]),
                        FinacialYear = Convert.ToString(dr["FinacialYear"]),
                        Godown_Id = Convert.ToInt16(dr["Godown_Id"]),
                        Caption = Convert.ToString(dr["Caption"]),

                    };
                    GodownList.Add(objRGodowns);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return GodownList;

    }


    public List<Department> GetAllDepartment()
    {
        List<Department> ItemTypeList = new List<Department>();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllDepartments();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Department objItemType = new Department()
                    {
                        Prop_Id = Convert.ToInt32(dr["Prop_Id"]),
                        Prop_Name = Convert.ToString(dr["Prop_Name"]),

                    };
                    ItemTypeList.Add(objItemType);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return ItemTypeList;

    }




    public List<Group> GetAllGroup()
    {
        List<Group> ItemTypeList = new List<Group>();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllGroup();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Group objItemType = new Group()
                    {
                        Group_Id = Convert.ToInt32(dr["Group_Id"]),
                        Group_Name = dr["Group_Name"].ToString(),

                    };
                    ItemTypeList.Add(objItemType);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return ItemTypeList;

    }



    public List<SubGroup> GetAllSubGroup()
    {
        List<SubGroup> ItemTypeList = new List<SubGroup>();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllSubGroup();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    SubGroup objItemType = new SubGroup()
                    {
                        SGroup_Id = Convert.ToInt32(dr["SGroup_Id"]),
                        SGroup_Name = dr["SGroup_Name"].ToString(),

                    };
                    ItemTypeList.Add(objItemType);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return ItemTypeList;

    }


    public string GetGroupHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllGroup();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["Group_ID"] + "' name='group' id='chkS_" + dr["Group_ID"] + "'/> <label for='chkS_" + dr["Group_ID"] + "'>" + dr["Group_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No Groups Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }


    public string GetSubGroupHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllSubGroup();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["SGroup_ID"] + "' name='subgroup' id='chkS_" + dr["SGroup_ID"] + "'/> <label for='chkS_" + dr["SGroup_ID"] + "'>" + dr["SGroup_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No SubGroups Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }


    public List<ItemType> GetAllItemType()
    {
        List<ItemType> ItemTypeList = new List<ItemType>();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllItemType();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ItemType objItemType = new ItemType()
                    {
                        Item_Type = dr["ItemType_Name"].ToString(),
                        Item_TypeID = dr["ItemType_ID"].ToString(),

                    };
                    ItemTypeList.Add(objItemType);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return ItemTypeList;

    }
    

    public List<Company> GetAllCompany()
    {
        List<Company> ItemTypeList = new List<Company>();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllCompany();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Company objItemType = new Company()
                    {
                        Company_ID = Convert.ToInt32(dr["Company_ID"]),
                        Company_Name = Convert.ToString(dr["Company_Name"]),

                    };
                    ItemTypeList.Add(objItemType);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return ItemTypeList;

    }



    public List<Products> GetAllProducts(string ItemType)
    {
        List<Products> ProductList = new List<Products>();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllProducts(ItemType);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Products objProducts = new Products()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = Convert.ToString(dr["Item_Name"]),


                    };
                    ProductList.Add(objProducts);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return ProductList;

    }



    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    //if (dr["DGID"].ToString() == dr["GodownId"].ToString())
                    //{
                    //    strBuilder.Append(string.Format("<option selected=selected value={0}>{1}</option>", dr["Godown_Id"].ToString(), dr["Godown_Name"].ToString()));
                    //}
                    //else
                    //{
                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Godown_Id"].ToString(), dr["Godown_Name"].ToString()));

                    // }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return strBuilder.ToString();

    }






    public string GetOptionsGodownFrom()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetGodownByStock();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Godown_Id"].ToString(), dr["Godown_Name"].ToString()));


                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return strBuilder.ToString();

    }


    // public Int16 InsertUpdate(RGodowns objRGodowns)
    //{

    //    return new RGodownsDAL().InsertUpdate(objRGodowns);
    //}


    public string GetCompanyHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllCompany();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["Company_ID"] + "' name='company' id='chkS_" + dr["Company_ID"] + "'/> <label for='chkS_" + dr["Company_ID"] + "' style='color: black;'>" + dr["Company_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No Companies Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }
    public string GetProductsHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllItems();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["Discontinued"]))
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["ItemID"] + "' name='item' id='chkS_" + dr["ItemID"] + "'/> <label for='chkS_" + dr["ItemID"] + "'>" + dr["Item_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No Items Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }

    public string GetItemsHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllItems();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["Discontinued"]) == false)
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["Item_Code"] + "' name='item' id='chkS_" + dr["Item_Code"] + "'/> <label for='chkS_" + dr["Item_Code"] + "'>" + dr["Item_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No Items Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return html;
    }

    public string GetDepartmentHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new RGodownsDAL().GetAllDepartments();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["Prop_ID"] + "' name='department' id='chkS_" + dr["Prop_ID"] + "'/> <label for='chkS_" + dr["Prop_ID"] + "'>" + dr["Prop_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No Department Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }
	
}