﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PayModeBLL
/// </summary>
public class PayModeBLL
{


    public Int32 ChangePaymode(string BillNowPrefix, string BillType, decimal CashAmount, decimal CrCardAmount, string Bank, string BankName, decimal CreditAmount, string CreditCustId, string CreditCustName, decimal OnlinePayment, int BranchId)
    {
        return new PayModeDAL().ChangePaymode(BillNowPrefix, BillType, CashAmount, CrCardAmount, Bank, BankName, CreditAmount, CreditCustId, CreditCustName, OnlinePayment, BranchId);
    }



    public void GetBillByBillNowPrefix(string BNF, Bill objBill)
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new PayModeDAL().GetBillByBNF(BNF);
            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    objBill.Net_Amount = Convert.ToDecimal(dr["Net_Amount"].ToString());
                    objBill.BillNowPrefix = dr["BillNowPrefix"].ToString();

                    objBill.BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString());

                    objBill.Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString());
                    objBill.Bill_No = Convert.ToInt32(dr["Bill_No"].ToString());
                    objBill.Bill_Prefix = Convert.ToString(dr["Bill_Prefix"]);

                    objBill.Customer_ID = Convert.ToString(dr["Customer_ID"]);
                    objBill.Customer_Name = Convert.ToString(dr["Customer_Name"].ToString());
                    objBill.Bill_Value = Convert.ToDecimal(dr["Bill_Value"]);
                    objBill.BillMode = Convert.ToString(dr["BillMode"]);
                    objBill.Less_Dis_Amount = Convert.ToDecimal(dr["Less_Dis_Amount"]);
                    objBill.Add_Tax_Amount = Convert.ToDecimal(dr["Add_Tax_Amount"]);
                    objBill.Net_Amount = Convert.ToDecimal(dr["Net_Amount"]);
                    objBill.CreditBank = Convert.ToString(dr["CreditBank"]);
                    objBill.UserNO = Convert.ToInt32(dr["UserNO"]);
                    objBill.Bill_Type = Convert.ToInt32(dr["Bill_Type"]);
                    objBill.Cash_Amount = Convert.ToDecimal(dr["Cash_Amount"]);
                    objBill.Credit_Amount = Convert.ToDecimal(dr["Credit_Amount"]);
                    objBill.CrCard_Amount = Convert.ToDecimal(dr["CrCard_Amount"]);
                    objBill.CashCust_Code = Convert.ToInt32(dr["CashCust_Code"]);
                    objBill.CashCust_Name = Convert.ToString(dr["CashCust_Name"]);
                    objBill.Round_Amount = Convert.ToDecimal(dr["Round_Amount"]);
                    objBill.Passing = Convert.ToBoolean(dr["Passing"]);
                    objBill.Bill_Printed = Convert.ToBoolean(dr["Bill_Printed"]);

                    objBill.Tax_Per = Convert.ToDecimal(dr["Tax_Per"]);
                    objBill.Godown_ID = Convert.ToInt32(dr["Godown_ID"]);
                    objBill.ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]);
                    objBill.R_amount = Convert.ToDecimal(dr["R_amount"]);
                    objBill.tokenno = Convert.ToInt32(dr["tokenno"]);
                    objBill.tableno = Convert.ToInt32(dr["tableno"]);
                    objBill.remarks = Convert.ToString(dr["remarks"]);
                    objBill.servalue = Convert.ToDecimal(dr["ServiceTaxPer"]);
                    objBill.ReceiviedGRNNo = Convert.ToInt32(dr["ReceiviedGRNNo"]);
                    objBill.DiscountPer = Convert.ToDecimal(dr["DisPer"]);
                    objBill.EmpCode = Convert.ToInt32(dr["EmpCode"]);
                    objBill.BranchId = Convert.ToInt32(dr["BranchId"]);
                    objBill.CashCustAddress = dr["CashCustAddress"].ToString();
                    objBill.CreditCustAddress = dr["CreditCustAddress"].ToString();
                    objBill.DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]);
                    objBill.KKCPer = Convert.ToDecimal(dr["KKCPer"]);
                    objBill.KKCAmt = Convert.ToDecimal(dr["KKCAmt"]);
                    objBill.SBCPer = Convert.ToDecimal(dr["SBCPer"]);
                    objBill.SBCAmt = Convert.ToDecimal(dr["SBCAmt"]);
                    objBill.BillRemarks = Convert.ToString(dr["BillRemarks"]);
                    objBill.OnlinePayment = Convert.ToDecimal(dr["OnlinePayment"]);




                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }


    }


}