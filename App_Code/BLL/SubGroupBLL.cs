﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for SubGroupBLL
/// </summary>
public class SubGroupBLL
{
    public List<SubGroup> GetByGroup(int GroupId)
    {
        List<SubGroup> SubGroupList = new List<SubGroup>();
     
        SqlDataReader dr = null;
        try
        {
            dr = new SubGroupDAL().GetByGroup(GroupId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    SubGroup objSubGroups = new SubGroup()
                    {
                        SubGroupId = Convert.ToInt16(dr["SubGroupId"]),
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"].ToString()),
                        

                    };
                    SubGroupList.Add(objSubGroups);
                }
            }

        }

        finally
        {
          
            dr.Close();
            dr.Dispose();
        }
        return SubGroupList;

    }



 
}