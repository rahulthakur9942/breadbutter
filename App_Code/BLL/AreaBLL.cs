﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for AreaBLL
/// </summary>
public class AreaBLL
{

    public Int32 DeleteArea(Areas objArea)
    {
        return new AreasDAL().Delete(objArea);
    }


    public void GetById(Areas objArea)
    {
       
        SqlDataReader dr = null;
        try
        {
            dr = new AreasDAL().GetById(objArea);
            if (dr.HasRows)
            {
                dr.Read();

                objArea.UserId = Convert.ToInt16(dr["UserId"]);  
                objArea.Area_Name = dr["Area_Name"].ToString();
                objArea.Area_ID = Convert.ToInt16(dr["Area_ID"]);
                objArea.IsActive = Convert.ToBoolean(dr["IsActive"]);
                
                 
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
         

    }



    public List<Areas> GetAll()
    {
        List<Areas> AreaList = new List<Areas>();

        SqlDataReader dr = null;
        try
        {
            dr = new AreasDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Areas objArea = new Areas()
                    {
                        Area_Name = dr["Area_Name"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        Area_ID = Convert.ToInt16(dr["Area_ID"]),
                        UserId = Convert.ToInt16(dr["UserId"])
                        
                    };
                    AreaList.Add(objArea);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return AreaList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new AreasDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Area_ID"].ToString(), dr["Area_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Areas objArea)
    {

        return new AreasDAL().InsertUpdate(objArea);
    }

}