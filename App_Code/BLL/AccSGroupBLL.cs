﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for AccSGroupBLL
/// </summary>
public class AccSGroupBLL
{
    public List<AccSGroup> GetAll()
    {
        List<AccSGroup> AccSGroupsList = new List<AccSGroup>();

        SqlDataReader dr = null;
        try
        {
            dr = new AccSGroupDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccSGroup objAccSGroup = new AccSGroup()
                    {
                        SS_NAME = dr["SS_NAME"].ToString(),
                        SS_CODE = Convert.ToInt16(dr["SS_CODE"]),
                        S_CODE = Convert.ToString(dr["S_CODE"]),
                        AMOUNT = Convert.ToDecimal(dr["AMOUNT"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    AccSGroupsList.Add(objAccSGroup);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return AccSGroupsList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new AccSGroupDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["SS_CODE"].ToString(), dr["SS_NAME"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(AccSGroup objAccSGroup)
    {

        return new AccSGroupDAL().InsertUpdate(objAccSGroup);
    }


    public List<AccSGroup> GetSgroupsByAccGroup(string SCODE)
    {
        List<AccSGroup> AccSGroupList = new List<AccSGroup>();

        SqlDataReader dr = null;
        try
        {
            dr = new AccSGroupDAL().GetByGroup(SCODE);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccSGroup objAccSGroup = new AccSGroup()
                    {
                        SS_NAME = dr["SS_NAME"].ToString(),
                        SS_CODE = Convert.ToInt16(dr["SS_CODE"]),
                        S_CODE = Convert.ToString(dr["S_CODE"]),
                        AMOUNT = Convert.ToDecimal(dr["AMOUNT"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    AccSGroupList.Add(objAccSGroup);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return AccSGroupList;

    }

    public Int32 DeleteAccGroup(AccSGroup objAccSGroup)
    {
        return new AccSGroupDAL().Delete(objAccSGroup);
    }
}