﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for CitiesBLL
/// </summary>
public class CitiesBLL
{
    public Int32 DeleteCity(Cities objCities)
    {
        return new CitiesDAL().Delete(objCities);
    }
    public void GetById(Cities objCities)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new CitiesDAL().GetById(objCities);
            if (dr.HasRows)
            {
                dr.Read();


                objCities.City_Name = dr["City_Name"].ToString();
                objCities.City_ID = Convert.ToInt16(dr["City_ID"]);
                objCities.UserId = Convert.ToInt16(dr["UserId"]);
                objCities.IsActive = Convert.ToBoolean(dr["IsActive"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }


    public List<Cities> GetAll()
    {
        List<Cities> CityList = new List<Cities>();

        SqlDataReader dr = null;
        try
        {
            dr = new CitiesDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Cities objCity = new Cities()
                    {
                        City_Name = dr["City_Name"].ToString(),

                        City_ID = Convert.ToInt16(dr["City_ID"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),          
                    };
                    CityList.Add(objCity);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return CityList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new CitiesDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["City_ID"].ToString(), dr["City_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Cities objCity)
    {

        return new CitiesDAL().InsertUpdate(objCity);
    }

}