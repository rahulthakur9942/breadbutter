﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebilltypesetting.aspx.cs" Inherits="ApplicationSettings_managebilltypesetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>



      <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Bill Type Settings</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                       
                        <div class="x_content">

                         <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                        <tr>
                          <td valign="top">
                          <table>
                              <tr>
                                  <td>
                                   <table cellpadding="10" cellspacing="5" border="0"   class="table" style="margin-bottom:20px;border-style:double" >
                                   <tr><td colspan ="100%" class="tableheadings"  >Local Billing</td></tr>

                                   <tr><td class="headings" align="left" style="text-align:left">Default Location:</td><td align="left" style="text-align:left;width:100px"><asp:DropDownList ClientIDMode="Static" runat="server" id="ddlBTLocation" style="width:200px" ></asp:DropDownList></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Local Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBTLocal" data-index="2"  name="chkBTLocal" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Retail Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBTRetail" data-index="2"  name="chkBTRetail" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">VAT Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBTVat" data-index="2"  name="chkBTVat" /></td></tr> 
                                        </table>
                                  </td>

                              </tr>
                              <tr>
                                  <td>
                                      <table cellpadding="10" cellspacing="5" border="0" class="table" style="margin-bottom:20px;">

                                      <tr><td colspan ="100%"  class="tableheadings" >OutStation Billing</td></tr>
                                          <tr><td class="headings" align="left" style="text-align:left">OutStation Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBTOutStation" data-index="2"  name="chkBTOutStation" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Against SaleTax:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBTSaleTax" data-index="2"  name="chkBTSaleTax" /></td></tr> 
                                  
                                      </table>
                                  </td>
                              </tr>

                              
                            
                          </table>
                          </td>
                            <td valign="top">
                                <table>

                                     <tr>
                                 <td>
                                 <table cellpadding="10" cellspacing="5" border="0" class="table"  style="margin-bottom:20px;border-style:double">
                                     <tr><td colspan ="100%"  class="tableheadings" >Default Series</td></tr>
                                    
                                     <tr><td class="headings" align="left" style="text-align:left">Default Series For Retail:</td><td align="left" style="text-align:left;width:100px"><asp:DropDownList ClientIDMode="Static" runat="server" id="ddlBTRetailSeries" style="width:200px" ></asp:DropDownList></td></tr> 
                                     <tr><td class="headings" align="left" style="text-align:left">Default Series For VAT:</td><td align="left" style="text-align:left;width:100px"><asp:DropDownList ClientIDMode="Static" runat="server" id="ddlBTVATSeries" style="width:200px" ></asp:DropDownList></td></tr> 
                                     <tr><td class="headings" align="left" style="text-align:left">Default Series For CST:</td><td align="left" style="text-align:left;width:100px"><asp:DropDownList ClientIDMode="Static" runat="server" id="ddlBTCSTSeries" style="width:200px" ></asp:DropDownList></td></tr> 
                                 </table>

                                 </td>
                             </tr>

                                </table>
                            </td>
                              <td valign="top">
                                <table>

                                     <tr>
                                 <td>
                                 <table cellpadding="10" cellspacing="5" border="0" class="table"  style="margin-bottom:20px;border-style:double">
                                    
                                 </table>

                                 </td>
                             </tr>

                                </table>
                            </td>
                            
                        </tr>
                     
                        <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnBTAdd"  class="btn btn-primary btn-small" >Apply Settings</div></td>
                                          
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>



      <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">


       function InsertUpdateBillType() {
           var objSettings = {};
         
           var LocalBilling = false;
           var RetailBilling = false;
           var VATBilling = false;
           var OutStation = false;
           var SaleTax = false;
           var LocationId = 0;
           var LocationName = "";
           var Retail = "";
           var Vat = "";
           var Cst = "";

           if ($('#chkBTLocal').is(":checked")) {
               LocalBilling = true;
             
               if ($('#chkBTRetail').is(":checked")) {
                   RetailBilling = true;
               }
               if ($('#chkBTVat').is(":checked")) {
                   VATBilling = true;
               }

           }

           
           if ($('#chkBTOutStation').is(":checked")) {
               OutStation = true;
               if ($('#chkBTSaleTax').is(":checked")) {
                   SaleTax = true;
               }
           }

           LocationId = $("#ddlBTLocation").val();
           LocationName = $("#ddlBTLocation option:selected").text();
           Retail = $("#ddlBTRetailSeries").val();
           Vat = $("#ddlBTVATSeries").val();
           Cst = $("#ddlBTCSTSeries").val();

           objSettings.LocalBillling = LocalBilling;
           objSettings.RetailBilling = RetailBilling;
           objSettings.VatBilling = VATBilling;
           objSettings.Outstation = OutStation;
           objSettings.SaleTax = SaleTax;
           objSettings.Ret_Def_Series = Retail;
           objSettings.Vat_Def_Series = Vat;
           objSettings.CST_Def_Series = Cst;
           objSettings.Default_location = LocationName;
           objSettings.Default_locationID = LocationId;

           alert(Vat);

           var DTO = { 'objSettings': objSettings };



           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managebilltypesetting.aspx/Insert",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {

                       alert("Setting Applied Successfully.");
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }




       $(document).ready(
    function () {
        $("#chkBTRetail").prop('disabled', true);
        $("#chkBTVat").prop('disabled', true);
        $("#chkBTSaleTax").prop('disabled', true);



        $("#ddlBTLocation").change(function () {


            var location = $("#ddlBTLocation").val();

            $.ajax({
                type: "POST",
                data: '{  "Default_LocationID":"' + location + '"}',
                url: "managebilltypesetting.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $('#chkBTSaleTax').prop('checked', false);
                    $("#chkBTRetail").prop('checked', false);
                    $("#chkBTVat").prop('checked', false);
                    var LocalBillling = obj.setttingData.LocalBillling;
                    if (LocalBillling == true) {

                        $('#chkBTLocal').prop('checked', true);
                        $("#chkBTRetail").prop('disabled', false);
                        $("#chkBTVat").prop('disabled', false);
                        
                        var RetailBillling = obj.setttingData.RetailBilling;
                        var VatBillling = obj.setttingData.VatBilling;
                        if (RetailBillling == true) {
                            $('#chkBTRetail').prop('checked', true);
                        }


                        if (VatBillling == true) {
                            $('#chkBTVat').prop('checked', true);
                        }

                    }
                    else {
                        $('#chkBTLocal').prop('checked', false);
                        $("#chkBTRetail").prop('disabled', true);
                        $("#chkBTVat").prop('disabled', true);
                    }



                    var Outstationbilling = obj.setttingData.Outstation;
                    if (Outstationbilling == true) {
                        $('#chkBTOutStation').prop('checked', true);
                        $("#chkBTSaleTax").prop('disabled', false);
                       
                        var saletax = obj.setttingData.SaleTax;
                        if (saletax == true) {
                            $('#chkBTSaleTax').prop('checked', true);
                        }

                    }
                    else {
                        $('#chkBTOutStation').prop('checked', false);
                        $("#chkBTSaleTax").prop('disabled', true);
                    }

                    var retail = obj.setttingData.Ret_Def_Series;
                    if (retail == "") {
                       retail = "0"
                    }
                    $("#ddlBTRetailSeries option[value='" + retail + "']").prop("selected", true);

                   
                    var vat = obj.setttingData.Vat_Def_Series;
                    if (vat == "") {
                        vat = "0"
                    }
                    $("#ddlBTVATSeries option[value='" + vat + "']").prop("selected", true);

                    var cst = obj.setttingData.CST_Def_Series;
                    if (cst == "") {
                        cst = "0"
                    }
                    $("#ddlBTCSTSeries option[value='" + cst + "']").prop("selected", true);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });
        });






        $("#btnBTAdd").click(
        function () {
            
            InsertUpdateBillType();
        }
        );

        $("#chkBTLocal").change(function () {
             
           
            if ($('#chkBTLocal').prop('checked') == true) {
                $("#chkBTRetail").prop('disabled', false);
                $("#chkBTVat").prop('disabled', false);
            }
            else {
                $("#chkBTRetail").prop('disabled', true);
                $("#chkBTVat").prop('disabled', true);
                $("#chkBTRetail").prop('checked', false);
                $("#chkBTVat").prop('checked', false);
            }
        });

        $("#chkBTOutStation").change(function () {

            if ($('#chkBTOutStation').prop('checked') == true) {
                
                $("#chkBTSaleTax").prop('disabled', false);
               
            }
            else {
                $("#chkBTSaleTax").prop('disabled', true);
                $("#chkBTSaleTax").prop('checked', false);
            }
        });
    });


   </script>
</asp:Content>

