﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApplicationSettings_managebilltypesetting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLocation();
            BindRetailSeries();
            BindVATSeries();
            BindCSTSeries();
        }
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BILLSETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    void BindLocation()
    {
        ddlBTLocation.DataSource = new LocationBLL().GetAll();
        ddlBTLocation.DataValueField = "Location_ID";
        ddlBTLocation.DataTextField = "Location_Name";
        ddlBTLocation.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Location--";
        li1.Value = "0";
        ddlBTLocation.Items.Insert(0, li1);

    }

    void BindRetailSeries()
    {
        ddlBTRetailSeries.DataSource = new BillSeriesSettingBLL().GetByBillType("Retail",2);
        ddlBTRetailSeries.DataValueField = "Series_Name";
        ddlBTRetailSeries.DataTextField = "Series_Name";
        ddlBTRetailSeries.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Series--";
        li1.Value = "0";
        ddlBTRetailSeries.Items.Insert(0, li1);

    }
    void BindVATSeries()
    {
        ddlBTVATSeries.DataSource = new BillSeriesSettingBLL().GetByBillType("VAT",1);
        ddlBTVATSeries.DataValueField = "Series_Name";
        ddlBTVATSeries.DataTextField = "Series_Name";
        ddlBTVATSeries.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Series--";
        li1.Value = "0";
        ddlBTVATSeries.Items.Insert(0, li1);

    }
    void BindCSTSeries()
    {
        ddlBTCSTSeries.DataSource = new BillSeriesSettingBLL().GetByBillType("CST",1);
        ddlBTCSTSeries.DataValueField = "Series_Name";
        ddlBTCSTSeries.DataTextField = "Series_Name";
        ddlBTCSTSeries.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Series--";
        li1.Value = "0";
        ddlBTCSTSeries.Items.Insert(0, li1);

    }

    

    [WebMethod]
    public static string Insert(BillTypeSettings objSettings)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objSettings.UserId = Id;
        int status = new BillTypeSettingBLL().UpdateBasicSettings(objSettings);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string FillSettings()
    {
        BillTypeSettings ObjSettings = new BillTypeSettings();
        new BillTypeSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string FillSettings(int Default_LocationID)
    {
        BillTypeSettings ObjSettings = new BillTypeSettings() { Default_BranchId = Default_LocationID };

        new BillTypeSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}