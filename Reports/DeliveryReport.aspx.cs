﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class Reports_DeliveryReport : System.Web.UI.Page
{
  DeliveryReport dtReport = new DeliveryReport();
  protected void Page_Load(object sender, EventArgs e)
  {
    BindGrid();
  }
  public void btnSubmit_Click(object Sender, EventArgs e)
  {
    DataSet dt = dtReport.GetByDateReport(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtToDate.Text));
    gvDeliveryReport.DataSource = dt.Tables[0];
    gvDeliveryReport.DataBind();
  }
  public void BindGrid()
  {
    DataSet dt = dtReport.GetDeliveryReport();
    gvDeliveryReport.DataSource = dt.Tables[0];
    gvDeliveryReport.DataBind();
  }
}