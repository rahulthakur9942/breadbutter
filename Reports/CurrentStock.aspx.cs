﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_CurrentStock : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
        if (!IsPostBack)
        {
            ddlItemType.DataSource = new RGodownsBLL().GetAllItemType();
            ddlItemType.DataTextField = "Item_Type";
            ddlItemType.DataValueField = "Item_TypeID";
            ddlItemType.DataBind();

            ddlcompany.DataSource = new RGodownsBLL().GetAllCompany();
            ddlcompany.DataTextField = "Company_Name";
            ddlcompany.DataValueField = "Company_ID";
            ddlcompany.DataBind();

            ddlgodown.DataSource = new RGodownsBLL().GetAll();
            ddlgodown.DataTextField = "Godown_Name";
            ddlgodown.DataValueField = "Godown_id";
            ddlgodown.DataBind();
            
            BindDepartents();
            BindBranches();
            BindGroups();
           
        }

        CheckRole();

        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        string deptqry = "";

        if (rdbAll.Checked == true)
        {
            deptqry = "All";
        }
        else
        {
            deptqry = hdnDept.Value;
        }

        string groupqry = "";

        if (rdbAllGroup.Checked == true)
        {
            groupqry = "All";
        }
        else
        {
            groupqry = hdnGroup.Value;
        }

        Int32 CompanyId = Convert.ToInt32(ddlcompany.SelectedValue);
        Int32 GodownId = Convert.ToInt32(ddlgodown.SelectedValue);
        String ItemType = ddlItemType.SelectedValue;
        rptcurrentstock objBreakageExpiry = new rptcurrentstock(deptqry,groupqry,CompanyId, GodownId, ItemType, BranchId);
        ReportViewer1.Report = objBreakageExpiry;



    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    void BindDepartents()
    {
        ltDepartments.Text = new RGodownsBLL().GetDepartmentHtml();
    }

    void BindGroups()
    {
        ltGroups.Text = new RGodownsBLL().GetGroupHtml();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCURRENTSTOCK));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

      
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}