﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReportStockLedger : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();

              ddlgodown.DataSource = new RGodownsBLL().GetAll();
            ddlgodown.DataTextField = "Godown_Name";
            ddlgodown.DataValueField = "Godown_id";
            ddlgodown.DataBind();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "--Select--";
            ddlgodown.Items.Insert(0, li);
            Bind();
          
        }

        CheckRole();

       

        Int32 GodownId = Convert.ToInt32(ddlgodown.SelectedValue);

        //if (GodownId == 0)
        //{
        //    Response.Write("<script>alert('Please Choose Godown');</script>");
        //}

        // string Products1 = Convert.ToString(ddlProducts.SelectedValue.ToString());
        string Products1 = txtCode.Text;

        Boolean FOC = true;
        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        if (txtDateFrom.Text != "" && Products1 != "0")
        {
            rptStockLedger objBreakageExpiry = new rptStockLedger(Products1, Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), GodownId, FOC, BranchId);
            ReportViewer1.Report = objBreakageExpiry;
        }
       


    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPSTOCKLEDGER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    public void Bind()
    {
        ListItem li = new ListItem();
        li.Value = "0";
        li.Text = "--Select--";
        string ItemType = "";
        if (rdbFinished.Checked == true)
        {
            ItemType = "3";
        }
        else if (rdbSemiFinished.Checked == true)
        {
            ItemType = "5";
        }
        else if (rdbRaw.Checked == true)
        {
            ItemType = "1";
        }



        ddlProducts.DataSource = new RGodownsBLL().GetAllProducts(ItemType);
        ddlProducts.DataTextField = "Item_Name";
        ddlProducts.DataValueField = "Item_Code";
        ddlProducts.DataBind();
        ddlProducts.Items.Insert(0, li);

    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

       



    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }

    protected void rdbFinished_CheckedChanged(object sender, EventArgs e)
    {
        Bind();
    }
    protected void rdbSemiFinished_CheckedChanged(object sender, EventArgs e)
    {
        Bind();
    }
    protected void rdbRaw_CheckedChanged(object sender, EventArgs e)
    {
        Bind();
    }
}