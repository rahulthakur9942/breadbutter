﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true"
    CodeFile="manageemployees.aspx.cs" Inherits="manageemployees" %>

<%@ Register Src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script language="javascript" type="text/javascript">
        var m_EmployeeId = 0;
        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }

        function GetVMResponse(Id, Title, IsActive, Status, Type) {
            $("#dvVMDialog").dialog("close");

            var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
            if (Type == "Area") {

                $("#<%=ddlArea.ClientID %>").append(opt);

            }

            if (Type == "City") {

                $("#<%=ddlCity.ClientID %>").append(opt);

            }

            if (Type == "State") {

                $("#<%=ddlState.ClientID %>").append(opt);

            }
        }

        function OpenVMDialog(Type) {
            $.ajax({
                type: "POST",
                data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
                url: "managearea.aspx/LoadUserControl",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    $("#dvVMDialog").remove();
                    $("body").append("<div id='dvVMDialog'/>");
                    $("#dvVMDialog").html(msg.d).dialog({ modal: true });
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });
        }


        function ResetControls() {

            m_EmployeeId = 0;
            var txtName = $("#txtName");
            var btnAdd = $("#btnAdd");
            var btnUpdate = $("#btnUpdate");
            txtName.focus().val("");
            $("#chkIsActive").prop("checked", "checked");

            $("#txtAddress1").val("");

            $("#txtAddress2").val("");
            $("#txtPwd").val("");
            $("#txtCompValue").val("0");

            $("#txtDis").val("0");

            $("#txtContactNumber").val("");

            $("#<%=ddlCity.ClientID%> option").removeAttr("selected");
            $("#<%=ddlArea.ClientID%> option").removeAttr("selected");
            $("#<%=ddlState.ClientID%> option").removeAttr("selected");
            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


            for (var i = 0; i < arrRole.length; i++) {

                if (arrRole[i] == "1") {

                    $("#btnAdd").css({ "display": "block" });
                }
            }


            btnUpdate.css({ "display": "none" });

            $("#btnReset").css({ "display": "none" });
            $("#hdnId").val("0");
            validateForm("detach");
        }



        function TakeMeTop() {
            $("html, body").animate({ scrollTop: 0 }, 500);
        }

        function RefreshGrid() {
            $('#jQGridDemo').trigger('reloadGrid');
        }

        function InsertUpdate() {

            if (!validateForm("frmCity")) {
                return;
            }

            var objEmployee = {};
            var Id = m_EmployeeId;

            var Name = $("#txtName").val();
            if ($.trim(Name) == "") {
                $("#txtName").focus();
                return;
            }
            var Address1 = $("#txtAddress1").val();
            var Address2 = $("#txtAddress2").val();
            var City = $("#<%=ddlCity.ClientID%>").val();
            var State = $("#<%=ddlState.ClientID%>").val();
            var Area = $("#<%=ddlArea.ClientID%>").val();
            var CntctNo = $("#txtContactNumber").val();
            var IsActive = false;
            var IsDeliveryBoy = false;
            var Comp = $("#txtCompValue").val();
           
            var Discount = $("#txtDis").val(); 
           
           var Designation = $('#<%=ddldesignation.ClientID %> option:selected').val();
            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }


            if ($('#chkIsDeliveryBoy').is(":checked")) {
                IsDeliveryBoy = true;
            }

            var Pwd = $("#txtPwd").val();

            objEmployee.Code = Id;
            objEmployee.Name = Name;
            objEmployee.Address1 = Address1;
            objEmployee.Address2 = Address2;
            objEmployee.AreaId = Area;
            objEmployee.CityId = City;
            objEmployee.StateId = State;
            objEmployee.ContactNo = CntctNo;
            objEmployee.IsActive = IsActive;
            objEmployee.KotPswrd = Pwd;
            objEmployee.ComplimentryValue = Comp;
            objEmployee.Discount = Discount;
            objEmployee.Designation = Designation;
            objEmployee.IsDeliveryBoy = IsDeliveryBoy;
            
      
       



            var DTO = { 'objEmployee': objEmployee };
            $.uiLock('');

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "manageemployees.aspx/Insert",
                data: JSON.stringify(DTO),
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -1) {
                        alert("Insertion Failed.Employee with duplicate name already exists.");
                        return;
                    }
                    if (obj.Status == -11) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }
                    if (Id == "0") {
                        ResetControls();
                        jQuery("#jQGridDemo").jqGrid('addRowData', obj.employee.Code, obj.employee, "last");
                        alert("Employee is added successfully.");
                    }
                    else {
                        ResetControls();

                        var myGrid = $("#jQGridDemo");
                        var selRowId = myGrid.jqGrid('getGridParam', 'selrow');
                        myGrid.jqGrid('setRowData', selRowId, obj.employee);
                        alert("Employee is Updated successfully.");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });
        }


        $(document).ready(
      function () {


          BindGrid();
          ValidateRoles();

          function ValidateRoles() {

              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

              for (var i = 0; i < arrRole.length; i++) {
                  if (arrRole[i] == "1") {
                      $("#btnAdd").show();
                      $("#btnAdd").click(
                    function () {
                        m_EmployeeId = 0;
                        InsertUpdate();
                    }
                    );
                  }
                  else if (arrRole[i] == "3") {
                      $("#btnUpdate").click(function () {

                          var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                          if ($.trim(SelectedRow) == "") {
                              alert("No Employee is selected to Edit");
                              return;
                          }
                          InsertUpdate();
                      }
        );
                  }
                  else if (arrRole[i] == "2") {
                      $("#btnDelete").show();
                      $("#btnDelete").click(
     function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No Employee is selected to Delete");
             return;
         }

         var employeeId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Code')
         if (confirm("Are You sure to delete this record")) {
             $.uiLock('');

             $.ajax({
                 type: "POST",
                 data: '{"EmployeeId":"' + employeeId + '"}',
                 url: "manageemployees.aspx/Delete",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.status == -10) {
                         alert("You don't have permission to perform this action..Consult Admin Department.");
                         return;
                     }
                     if (obj.status == -1) {
                         alert("Deletion Failed. Employee is in Use.");
                         return
                     }
                     BindGrid();
                     ResetControls();
                     alert("Employee is Deleted successfully.");
                 },
                 error: function (xhr, ajaxOptions, thrownError) {
                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {
                     $.uiUnlock();
                 }
             });
         }
     }
     );
                  }
              }
          }


          $('#txtName').focus();
          $("#btnReset").click(
        function () {
            ResetControls();
        }
        );
      });

    </script>
    <style type="text/css">
        #tblist tr
        {
            border-bottom: solid 1px silver;
            background: #EDEDED;
        }
        #tblist tr td
        {
            text-align: left;
            padding: 2px;
        }
        #tblist tr:nth-child(even)
        {
            background: #F7F7F7;
        }
    </style>
    <form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnRoles" runat="server" />
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Employees</h3>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    Go!</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Add/Edit Employee</h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed">
                        <tr>
                            <td class="headings">
                                Name
                            </td>
                            <td colspan="3">
                                <input type="text" name="txtName" class="form-control validate required alphanumeric"
                                    data-index="1" id="txtName" style="width: 544px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">
                                Address1:
                            </td>
                            <td>
                                <textarea class="form-control validate required alphanumeric" id="txtAddress1" style="width: 213px"></textarea>
                            </td>
                            <td class="headings">
                                Address2:
                            </td>
                            <td>
                                <textarea class="form-control" id="txtAddress2" required></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">
                                Area:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Style="width: 190px; height: 35px" class="form-control validate ddlrequired"
                                                ID="ddlArea" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <span class="fa fa-plus" onclick="javascript:OpenVMDialog('Area')" style="font-size: 25px;
                                                padding: 10px 0px 0px 3px; cursor: pointer"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="headings">
                                City:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Style="width: 190px; height: 35px" class="form-control validate ddlrequired"
                                                ID="ddlCity" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <span class="fa fa-plus" onclick="javascript:OpenVMDialog('City')" style="font-size: 25px;
                                                padding: 10px 0px 0px 3px; cursor: pointer"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                        <tr>
                            <td class="headings">
                                State:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList Style="width: 190px; height: 35px" ID="ddlState" class="form-control validate ddlrequired"
                                                runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <span class="fa fa-plus" onclick="javascript:OpenVMDialog('State')" style="font-size: 25px;
                                                padding: 10px 0px 0px 3px; cursor: pointer"></span>
                                        </td>

                                      
                                    </tr>
                                </table>
                            </td>
                            <td class="headings">
                                Designation:
                            </td>
                               <td>
                                <table>
                                    <tr>
                                        <td>
                                           <asp:DropDownList Style="width: 190px; height: 35px" ID="ddldesignation" class="form-control validate ddlrequired"
                                                runat="server">
                                            </asp:DropDownList>
                                        </td>
                                 <%--       <td>
                                            <span class="fa fa-plus" onclick="javascript:OpenVMDialog('State')" style="font-size: 25px;
                                                padding: 10px 0px 0px 3px; cursor: pointer"></span>
                                        </td>--%>

                                      
                                    </tr>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">
                                Contact Number:
                            </td>
                            <td>
                                <input type="text" name="txtContactNumber" class="form-control validate valNumber"
                                    data-index="1" id="txtContactNumber" style="width: 213px" />
                            </td>

                             <td class="headings">
                                KOT Password:
                            </td>
                            <td>
                                <input type="text" name="txtPwd" class="form-control"
                                    data-index="1" id="txtPwd" style="width: 213px" />
                            </td>
                        </tr>

                          <tr>
                            <td class="headings">
                                Comp Value:
                            </td>
                            <td>
                                <input type="text" name="txtCompValue" value="0" class="form-control validate valNumber"
                                    data-index="1" id="txtCompValue" style="width: 213px" />
                            </td>

                             <td class="headings">
                                Discount:
                            </td>
                            <td>
                                <input type="text" name="txtDis" value="0"  class="form-control"
                                    data-index="1" id="txtDis" style="width: 213px" />
                            </td>
                        </tr>

                        <tr>
                            <td class="headings">
                                IsActive:
                            </td>
                            <td align="left" style="text-align: left">
                                <input type="checkbox" id="chkIsActive" checked="checked" data-index="2" name="chkIsActive" />
                            </td>
                           <td class="headings">
                                Is Dlivery Boy:
                            </td>
                            <td align="right" style="text-align: left">
                                <input type="checkbox" id="chkIsDeliveryBoy"  data-index="3" name="chkIsDeliveryBoy" />
                            </td>
                        </tr>




                        <tr>
                            <td>
                            </td>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <div id="btnAdd" style="display: none;" class="btn btn-primary btn-small">
                                                Add <i class="fa fa-external-link"></i>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="btnUpdate" class="btn btn-success btn-small" style="display: none;">
                                                <i class="fa fa-edit m-right-xs"></i>Update
                                            </div>
                                        </td>
                                        <td>
                                            <div id="btnReset" class="btn btn-danger btn-small" style="display: none;">
                                                <i class="fa fa-mail-reply-all"></i>Cancel</div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Manage Employees</h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <div class="youhave">
                        <table id="jQGridDemo">
                        </table>
                        <div id="btnDelete" style="margin-top: 10px; display: none;" class="btn btn-danger btn-small">
                            <i class="fa fa-trash m-right-xs"></i>Delete</div>
                        <div id="jQGridDemoPager">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
        <!-- /footer content -->
    </div>
    </form>



      <script type="text/javascript">
                function BindGrid() {
              
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageEmployees.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Code', 'Name', 'Address1', 'Address2', 'Address3', 'Area', 'City', 'State', 'ContactNo', 'IsActive', 'UserId', 'BranchId', 'ComplementaryValue', 'Discount', 'Designation'],
                        colModel: [
                                    { name: 'Code', key: true, index: 'Code', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'Name', index: 'Name', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Address1', index: 'Address1', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Address2', index: 'Address2', width: 200, stype: 'text', sortable: true, editable: true,hidden: false , editrules: { required: true } },
                                    { name: 'Address3', index: 'Address3', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'AreaId', index: 'AreaId', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'CityId', index: 'CityId', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'StateId', index: 'StateId', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'ContactNo', index: 'ContactNo', width: 200, stype: 'text', sortable: true, editable: true,hidden: false , editrules: { required: true } },                                    
                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox",hidden: true , editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'KotPswrd', index: 'KotPswrd', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'UserId',hidden:true, index: 'UserId', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },                        
                                    { name: 'ComplimentryValue', index: 'ComplimentryValue', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Discount', hidden: true, index: 'Discount', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Designation', index: 'Designation', width: 200, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },

                        ],
                        rowNum: 10,
                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Code',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Employees List",
                        editurl: 'handlers/ManageEmployees.ashx',
                         ignoreCase: true,
                         toolbar: [true, "top"],                         
                    });



           var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });


                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_EmployeeId = 0;
                    validateForm("detach");
             
                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                       $("#btnUpdate").css({ "display": "none" });
                       $("#btnReset").css({ "display": "none" });
                       $("#btnAdd").css({ "display": "none" });
                  

                       for (var i = 0; i < arrRole.length; i++) {

                           if (arrRole[i] == 1) {

                               $("#btnAdd").css({ "display": "block" });
                           }

                           if (arrRole[i] == 3) {


                               m_EmployeeId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Code');


                             
                               $("#txtName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Name'));
                               $("#txtPwd").val($('#jQGridDemo').jqGrid('getCell', rowid, 'KotPswrd'));
                               $("#txtAddress1").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Address1'));
                               $("#txtAddress2").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Address2'));

                               $("#txtCompValue").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ComplimentryValue'));
                               $("#txtDis").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Discount'));
                             
                               var City = $('#jQGridDemo').jqGrid('getCell', rowid, 'CityId');
                               $("#<%=ddlCity.ClientID%> option[value='" + City + "']").prop("selected", true);
                               var Area = $('#jQGridDemo').jqGrid('getCell', rowid, 'AreaId');
                               $("#<%=ddlArea.ClientID%> option[value='" + Area + "']").prop("selected", true);
                               var State = $('#jQGridDemo').jqGrid('getCell', rowid, 'StateId');
                               $("#<%=ddlState.ClientID%> option[value='" + State + "']").prop("selected", true);


                               var Designation = $('#jQGridDemo').jqGrid('getCell', rowid, 'Designation');
                               $("#<%=ddldesignation.ClientID%> option[value='" + Designation + "']").prop("selected", true);

                               $("#txtContactNumber").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ContactNo'));

                               if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                   $('#chkIsActive').prop('checked', true);
                               }
                               else {
                                   $('#chkIsActive').prop('checked', false);

                               }


                               $("#btnAdd").css({ "display": "none" });
                               $("#btnUpdate").css({ "display": "block" });
                               $("#btnReset").css({ "display": "block" });
                           }

                       }
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true
                             }
               );
        }
    </script>
  


</asp:Content>
