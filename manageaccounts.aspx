﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageaccounts.aspx.cs" Inherits="manageaccounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
 

 
     
   
<script language="javascript" type="text/javascript">

    var m_AccountId = 0;
    $(document).ready(
    function () {

        BindGrid();

        $("#ddlAccount").change(
        function () {

            var BalInc = $("#ddlAccount").val();
            $.ajax({
                type: "POST",
                data: '{ "BALINC": "' + BalInc + '"}',
                url: "manageaccounts.aspx/BindAccGroups",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    alert(obj.AccGroupOptions);

                    $("#ddlAccGroup").html(obj.AccGroupOptions);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );


        });
    });


</script>
    



<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Accounts</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Account</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                     <tr><td class="headings">Account Master:</td><td colspan = "3">    <select id="ddlAccount" style="width:300px" class="validate ddlrequired" >
                     <option value="B">BALANCE SHEET</option>
                     <option value="I">INCOME/EXPENDITURE</option></td></tr>                               
                    
                     <tr><td class="headings" >Account Name:</td><td colspan = "3">  <input type="text"  name="txtTitle" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 300px"/></td></tr>
                      <tr><td class="headings">Group:</td><td colspan = "3"> <asp:DropDownList style="width:215px;height:35px"  ID ="ddlAccGroup"  runat="server" ></asp:DropDownList>
                    </td></tr>   

                        <tr><td class="headings">Op. Balance</td><td><input type="text"  name="txtOpBal" class="form-control validate required valnumber"   data-index="1" id="Text1" style="width: 150px"/></td><td class="headings">Debit/Credit:</td><td>    <select id="ddlDrCr" style="width:300px" class="validate ddlrequired" >
                         <option value="DR">DR</option>
                     <option value="CR">CR</option></td></tr>      
                        </select>
                    </td></tr>
               
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Account</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Account</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;" >Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Accounts</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


 
</form>


  <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageAccounts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Account Id','CODE','CCODE','H_CODE','S_CODE','SS_CODE','CNAME','OP. BAL','DR/CR'],
  


                        colModel: [
                                    { name: 'AccountId', key: true, index: 'AccountId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'CODE', index: 'CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'CCODE', index: 'CCODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'H_CODE', index: 'H_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'S_CODE', index: 'S_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'SS_CODE', index: 'SS_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'CNAME', index: 'CNAME', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                   { name: 'OP_BAL', index: 'OP_BAL', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
 
                                    { name: 'DR_CR', index: 'DR_CR', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'AccountId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Accounts List",

                        editurl: 'handlers/ManageAccounts.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_AccountId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_AccountId = $('#jQGridDemo').jqGrid('getCell', rowid, 'AccountId');
                  
                    
                     txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'CNAME'));
                     txtTitle.focus();
                     alert($('#jQGridDemo').jqGrid('getCell', rowid, 'OP_BAL'));
                     $("#txtOpBal").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OP_BAL'));
                     var accgroup = $('#jQGridDemo').jqGrid('getCell', rowid, 'S_CODE');
                     $("#<%=ddlAccGroup.ClientID%> option[value='" + accgroup + "']").prop("selected", true);
                     var div = $('#jQGridDemo').jqGrid('getCell', rowid, 'DR_CR')
                     $('#ddlDrCr option[value=' + div + ']').prop('selected', 'selected');
                    
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


            </script>

</asp:Content>

