﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class ApplicationSettings_managediscountsetting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            BindBranches();
        }
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.DISCOUNTSETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }



    void BindBranches()
    {

        ddlBDBranch.DataSource = new BranchBLL().GetAll();
        ddlBDBranch.DataValueField = "BranchId";
        ddlBDBranch.DataTextField = "BranchName";
        ddlBDBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBDBranch.Items.Insert(0, li1);

    }


    [WebMethod]
    public static string FillGridSettings(string Type,int Branch)
    {
        DiscountSetting ObjSettings = new DiscountSetting() { Type = Type };
        

        ObjSettings.BranchId = Branch;
       List<DiscountDetail>lst= new DiscountSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,
            DiscountDetail=lst

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string InsertDiscountOptions(DiscountSetting objSettings, DiscountDetail[] objDisDetail)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
      
        objSettings.UserId = Id;
      

        DataTable dt = new DataTable();
        dt.Columns.Add("StartValue");
        dt.Columns.Add("EndValue");
        dt.Columns.Add("DisPer");
    
        DataRow dr;



        foreach (var item in objDisDetail)
        {
            dr = dt.NewRow();
            dr["StartValue"] = item.StartValue;
            dr["EndValue"] = item.EndValue;
            dr["DisPer"] = item.DisPer;
   
            dt.Rows.Add(dr);


        }




        int status = new DiscountSettingBLL().UpdateBasicSettings(objSettings,dt);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}