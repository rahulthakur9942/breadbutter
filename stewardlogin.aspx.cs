﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class stewardlogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    protected void itemSelected(object sender, EventArgs e)
    {
        var locid = ddlLocation.SelectedValue;
        if (locid == "1")
        {
            Response.Cookies[Constants.DataBase].Value = "1";
            BindBranches();
        }
        else if (locid == "2")
        {
            Response.Cookies[Constants.DataBase].Value = "6";
            BindBranches();
        }
        else
        {

            Response.Write("<script>alert('Choose Location First')</script>");
            return;
        }


    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {

        if (ddlBranch.SelectedValue == "0")
        {

            Response.Write("<script>alert('Choose Branch First')</script>");
            return;
        }

        Employees objEmployee = new Employees()
        {
            Name = txtUserName.Value,
            KotPswrd = txtPassword.Value

        };

        int Branchvalue = Convert.ToInt32(ddlBranch.SelectedValue);

        Int32 status = new kotBLL().EmployeeLoginCheck(objEmployee, Branchvalue);
        


        if (status.ToString() == "-1")
        {
            Response.Write("<script>alert('Invalid User Name');</script>");
        }
        else if (status.ToString() == "-2")
        {
            Response.Write("<script>alert('Invalid Password');</script>");
        }
        else if (status.ToString() == "-3")
        {
            Response.Write("<script>alert('Invalid Branch');</script>");
        }
        else
        {


          Response.Redirect("http://49.50.124.155:8091/stewardwelcome.aspx?bid=" + ddlBranch.SelectedValue + "&bname=" + ddlBranch.SelectedItem.Text + "&UserId=" + status + "&UserName=" + txtUserName.Value.Trim());
            //Response.Redirect("http://localhost:51000/RPro/stewardwelcome.aspx?bid=" + ddlBranch.SelectedValue + "&bname=" + ddlBranch.SelectedItem.Text + "&UserId=" + status + "&UserName=" + txtUserName.Value.Trim());
        }


    }
}