﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managegodown : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.GODOWNS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }
    [WebMethod]
    public static string Insert(int GodownId, string GodownName, string Divison, string FinancialYr, string Caption,bool IsActive)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.GODOWNS));

        string[] arrRoles = sesRoles.Split(',');


        if (GodownId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }


        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Godowns objGodown = new Godowns()
        {
            Godown_Id = GodownId,
            Godown_Name = GodownName.Trim().ToUpper(),
            Divison = Divison,
            FinacialYear = FinancialYr,
            Caption =Caption,
            UserId = Id,
            IsActive = IsActive,

        };
        status = new GodownsBLL().InsertUpdate(objGodown);
        var JsonData = new
        {
            godown = objGodown,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 GodownId)
    {

        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.GODOWNS));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Godowns objGodown = new Godowns()
        {
            Godown_Id = GodownId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new GodownsBLL().DeleteGodown(objGodown);
        var JsonData = new
        {
            godown = objGodown,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}