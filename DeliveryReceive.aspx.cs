﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class DeliveryReceive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdntodaydate.Value = DateTime.Now.ToShortDateString();
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }

    }
    [WebMethod]
    public static string ReceiveDelivery(string BillNo, int BranchId, int GodownId)
    {

        int BranchIdNew = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        int Status = new DeliveryNoteBLL().ReceiveDelivery(BillNo, BranchIdNew, GodownId);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetById(string BillNo)
    {

        Int32 BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string[] Bill = BillNo.Split('-');
        JavaScriptSerializer ser = new JavaScriptSerializer();
        DeliveryMaster objDelivery = new DeliveryMaster();
        objDelivery.Bill_No = Convert.ToInt32(Bill[1].ToString());
        objDelivery.Prefix = Bill[0].ToString();
        List<DeliveryDetail> DeliveryDetail = new DeliveryNoteBLL().GetDeliveryByIdByBranch(objDelivery, BranchId);
        var JsonData = new
        {
            Delivery = objDelivery,
            DeliveryDetail = DeliveryDetail
        };
        return ser.Serialize(JsonData);


    }

    //[WebMethod]
    //public static string GetDefaultGodown(int BranchId)
    //{
    //    JavaScriptSerializer ser = new JavaScriptSerializer();
    //    int Status = new BillSeriesSettingBLL().GetByBillType(BillNo, BranchId, GodownId);
    //    var JsonData = new
    //    {

    //        status = Status
    //    };
    //    return ser.Serialize(JsonData);
    //}
}